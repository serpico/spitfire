# Spartion

c++ implementation of the Sparse deconvolution algorithm.
  

## Compile


```
cd spartion
mkdir build
cmake ..
make
```

## Library usage

Here is an example how to run a process on an image

```
#include <spartion>

```

## Command line tools

```
...
```

# Docker

This repository contains a Dockerfile to run Spartion in a container. We highly recommend using a `docker-compose.yml` to run your containers. Here is an example of docker-compose file content:

```yaml
version: "3"
    services:
        spartion:
            container_name: spartion
            build:
            context: ./spartion
            dockerfile: Dockerfile
            volumes:
                - './data:/app/data/' 
```
