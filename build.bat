mkdir build
cd build

git clone https://github.com/sylvainprigent/score.git
cd score
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
cd ..\..

git clone https://github.com/sylvainprigent/sdata.git
cd sdata
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
cd ..\..

git clone https://github.com/sylvainprigent/simage.git
cd simage
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64 -Dscore_DIR=%cd%\..\..\score\build\ -Dsdata_DIR=%cd%\..\..\sdata\build\
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
cd ..\..

git clone https://github.com/sylvainprigent/sdataio.git
cd sdataio
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64 -Dscore_DIR=%cd%\..\..\score\build\ -Dsdata_DIR=%cd%\..\..\sdata\build\
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
cd ..\..

git clone https://github.com/sylvainprigent/sroi.git
cd sroi
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64 -Dscore_DIR=%cd%\..\..\score\build\ -Dsdata_DIR=%cd%\..\..\sdata\build\ -Dsdataio_DIR=%cd%\..\..\sdataio\build\ -Dsimage_DIR=%cd%\..\..\simage\build\
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
cd ..\..

git clone https://github.com/sylvainprigent/smanipulate.git
cd smanipulate
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64 -Dscore_DIR=%cd%\..\..\score\build\ -Dsimage_DIR=%cd%\..\..\simage\build\ -Dsdata_DIR=%cd%\..\..\sdata\build\ -Dsroi_DIR=%cd%\..\..\sroi\build\
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
cd ..\..

git clone https://github.com/sylvainprigent/sfiltering.git
cd sfiltering
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64 -Dscore_DIR=%cd%\..\..\score\build\ -Dsimage_DIR=%cd%\..\..\simage\build\
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
cd ..\..

git clone https://github.com/sylvainprigent/sfft.git
cd sfft
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64 -Dscore_DIR=%cd%\..\..\score\build\ -Dsimage_DIR=%cd%\..\..\simage\build\
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1 
cd ..\..

git clone https://github.com/sylvainprigent/simageio.git
cd simageio
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64 -Dscore_DIR=%cd%\..\..\score\build\ -Dsimage_DIR=%cd%\..\..\simage\build\
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
cd ..\..

git clone https://github.com/sylvainprigent/scli.git
cd scli
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -A x64 -Dscore_DIR=%cd%\..\..\score\build\
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
cd ..\..

# Spartion
cmake .. -G "Visual Studio 16 2019" -A x64 -Dspartion_BUILD_TOOLS=ON -Dscore_DIR=%cd%\score\build\ -Dsimage_DIR=%cd%\simage\build\ -Dsmanipulate_DIR=%cd%\smanipulate\build\ -Dsroi_DIR=%cd%\sroi\build\ -Dsfiltering_DIR=%cd%\sfiltering\build\ -Dsdata_DIR=%cd%\sdata\build\ -Dsfft_DIR=%cd%\sfft\build\ -Dsimageio_DIR=%cd%\simageio\build\ -Dsdataio_DIR=%cd%\sdataio\build\ -Dscli_DIR=%cd%\scli\build\
cmake ..
if errorlevel 1 exit 1
cmake --build . --config Release
if errorlevel 1 exit 1
