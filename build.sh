#!/bin/sh

mkdir build/
cd build

git clone https://github.com/sylvainprigent/score.git
cd score
mkdir build/
cd build/
cmake ..
make
cd ../..

git clone https://github.com/sylvainprigent/sdata.git
cd sdata
mkdir build/
cd build/
cmake ..
make
cd ../..

git clone https://github.com/sylvainprigent/simage.git
cd simage
mkdir build/
cd build/
cmake .. -Dscore_DIR=${PWD}/../../score/build/ -Dsdata_DIR=${PWD}/../../sdata/build/
make
cd ../..

git clone https://github.com/sylvainprigent/sdataio.git
cd sdataio
mkdir build/
cd build/
cmake .. -Dscore_DIR=${PWD}/../../score/build/ -Dsdata_DIR=${PWD}/../../sdata/build/
make
cd ../..

git clone https://github.com/sylvainprigent/sroi.git
cd sroi
mkdir build/
cd build/
cmake .. -Dscore_DIR=${PWD}/../../score/build/ -Dsdata_DIR=${PWD}/../../sdata/build/ -Dsdataio_DIR=${PWD}/../../sdataio/build/ -Dsimage_DIR=${PWD}/../../simage/build/
make
cd ../..

git clone https://github.com/sylvainprigent/smanipulate.git
cd smanipulate
mkdir build/
cd build/
cmake .. -Dscore_DIR=${PWD}/../../score/build/ -Dsimage_DIR=${PWD}/../../simage/build/ -Dsdata_DIR=${PWD}/../../sdata/build/ -Dsroi_DIR=${PWD}/../../sroi/build/
make
cd ../..

git clone https://github.com/sylvainprigent/sfiltering.git
cd sfiltering
mkdir build/
cd build/
cmake .. -Dscore_DIR=${PWD}/../../score/build/ -Dsimage_DIR=${PWD}/../../simage/build/
make
cd ../..

git clone https://github.com/sylvainprigent/sfft.git
cd sfft
mkdir build/
cd build/
cmake .. -Dscore_DIR=${PWD}/../../score/build/ -Dsimage_DIR=${PWD}/../../simage/build/
make 
cd ../..

git clone https://github.com/sylvainprigent/simageio.git
cd simageio
mkdir build/
cd build/
cmake .. -Dscore_DIR=${PWD}/../../score/build/ -Dsimage_DIR=${PWD}/../../simage/build/
make
cd ../..

git clone https://github.com/sylvainprigent/scli.git
cd scli
mkdir build/
cd build/
cmake .. -Dscore_DIR=${PWD}/../../score/build/
make
cd ../..

# Spartion
cmake .. -Dspitfire_BUILD_TOOLS=ON -Dscore_DIR=${PWD}/score/build/ -Dsimage_DIR=${PWD}/simage/build/ -Dsmanipulate_DIR=${PWD}/smanipulate/build/ -Dsroi_DIR=${PWD}/sroi/build/ -Dsfiltering_DIR=${PWD}/sfiltering/build/ -Dsdata_DIR=${PWD}/sdata/build/ -Dsfft_DIR=${PWD}/sfft/build/ -Dsimageio_DIR=${PWD}/simageio/build/ -Dsdataio_DIR=${PWD}/sdataio/build/ -Dscli_DIR=${PWD}/scli/build/
cmake ..
make
