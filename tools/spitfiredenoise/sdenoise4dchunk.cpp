#include <score>
#include <scli>
#include <simageio>
#include <spitfiredenoise>


int main(int argc, char *argv[])
{
    SObserverConsole* observer = new SObserverConsole();
    try
    {
        // Parse inputs
        SCliParser cmdParser(argc, argv);
        cmdParser.addInputData("-i", "Input image file (txt file with list of tif frames)");
        cmdParser.addOutputData("-o", "Output image file (txt file with list of tif frames)");
        cmdParser.addParameterInt("-chunk", "Size of a time chunk ", 5);

        cmdParser.addParameterSelect("-method", "Deconvolution method 'SV' or 'HSV", "HSV");
        cmdParser.addParameterFloat("-regularization", "Regularization parameter as pow(2,-x)", 2);
        cmdParser.addParameterFloat("-weighting", "Weighting parameter", 0.6);
        cmdParser.addParameterFloat("-deltaz", "Delta resolution between xy and z", 1.0);
        cmdParser.addParameterFloat("-deltat", "Delta resolution between xyz and t", 1.0);
        cmdParser.addParameterInt("-niter", "Nb iterations", 200);

        cmdParser.addParameterBoolean("-verbose", "Print iterations to console", true);
        cmdParser.setMan("Denoise a 4D image with Spitfire method");
        cmdParser.parse(2);

        std::string inputImageFile = cmdParser.getDataURI("-i");
        std::string outputImageFile = cmdParser.getDataURI("-o");
        const int chunk = cmdParser.getParameterInt("-chunk");

        const std::string method = cmdParser.getParameterString("-method");
        const float regularization = cmdParser.getParameterFloat("-regularization");
        const float weighting = cmdParser.getParameterFloat("-weighting");
        const float deltaZ = cmdParser.getParameterFloat("-deltaz");
        const float deltaT = cmdParser.getParameterFloat("-deltat");
        const int niter = cmdParser.getParameterInt("-niter");
        const bool verbose = cmdParser.getParameterBool("-verbose");

        if (inputImageFile == ""){
            observer->message("SDenoise4d: Input image path is empty");
            return 1;
        }

        if (verbose){
            observer->message("SDenoise4d: input image: " + inputImageFile);
            observer->message("SDenoise4d: input image: " + outputImageFile);
            observer->message("SDenoise4d: method: " + method);
            observer->message("SDenoise4d: chunk: " + std::to_string(chunk));
            observer->message("SDenoise4d: regularization parameter: " + std::to_string(regularization));
            observer->message("SDenoise4d: weighting parameter: " + std::to_string(weighting));
            observer->message("SDenoise4d: deltaZ parameter: " + std::to_string(deltaZ));
            observer->message("SDenoise4d: deltaT parameter: " + std::to_string(deltaT));
            observer->message("SDenoise4d: nb iterations: " + std::to_string(niter));
        }

        // run process using time chunk
        STxtIOChunk io(inputImageFile, chunk, 1);
        SImg::tic();
        while(io.hasNext())
        {
            SImage* inputImage = io.getNext();
            std::cout << "input chunk size: " << inputImage->getSizeX() << ", " << inputImage->getSizeY()  << ", " << inputImage->getSizeZ() << ", " << inputImage->getSizeT() << ", " << inputImage->getSizeC() << std::endl;
            SDenoise4d process; 
            process.addObserver(observer);
            process.setInput( inputImage );
            process.setMethod(method);
            process.setRegularization(regularization);
            process.setWeighting(weighting);
            process.setIterationsNumber(niter);
            process.setVerbose(verbose);
            process.setDeltaZ(deltaZ);
            process.setDeltaT(deltaT);
            process.run();
            SImage* outputImage = process.getOutput();
            io.writeCurrent(outputImage, outputImageFile);

            delete inputImage;
            delete outputImage;
        }
        io.writeTxt(outputImageFile);
        SImg::toc();
    }
    catch (SException &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }
    catch (std::exception &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }

    delete observer;
    return 0;
}
