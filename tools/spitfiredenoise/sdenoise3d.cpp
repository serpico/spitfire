#include <score>
#include <scli>
#include <simageio>
#include <spitfiredenoise>


int main(int argc, char *argv[])
{
    SObserverConsole* observer = new SObserverConsole();
    try
    {
        // Parse inputs
        SCliParser cmdParser(argc, argv);
        cmdParser.addInputData("-i", "Input image file");
        cmdParser.addOutputData("-o", "Output image file");

        cmdParser.addParameterSelect("-method", "Deconvolution method 'SV' or 'HSV", "HSV");
        cmdParser.addParameterFloat("-regularization", "Regularization parameter as pow(2,-x)", 2);
        cmdParser.addParameterFloat("-weighting", "Weighting parameter", 0.6);
        cmdParser.addParameterFloat("-delta", "Delta resolution between xy and z", 1.0);
        cmdParser.addParameterInt("-niter", "Nb iterations", 200);

        cmdParser.addParameterBoolean("-verbose", "Print iterations to console", true);
        cmdParser.setMan("Denoise a 3D image with SPARTION method");
        cmdParser.parse(2);

        std::string inputImageFile = cmdParser.getDataURI("-i");
        std::string outputImageFile = cmdParser.getDataURI("-o");

        const std::string method = cmdParser.getParameterString("-method");
        const float regularization = cmdParser.getParameterFloat("-regularization");
        const float weighting = cmdParser.getParameterFloat("-weighting");
        const float delta = cmdParser.getParameterFloat("-delta");
        const int niter = cmdParser.getParameterInt("-niter");
        const bool verbose = cmdParser.getParameterBool("-verbose");

        if (inputImageFile == ""){
            observer->message("SDenoise3d: Input image path is empty");
            return 1;
        }

        if (verbose){
            observer->message("SDenoise3d: input image: " + inputImageFile);
            observer->message("SDenoise3d: method: " + method);
            observer->message("SDenoise3d: regularization parameter: " + std::to_string(regularization));
            observer->message("SDenoise3d: weighting parameter: " + std::to_string(weighting));
            observer->message("SDenoise3d: delta parameter: " + std::to_string(delta));
            observer->message("SDenoise3d: nb iterations: " + std::to_string(niter));
        }

        // Run process
        SImage* inputImage = SImageReader::read(inputImageFile, 32);
        std::cout << "input image size: " << inputImage->getSizeX() << ", " << inputImage->getSizeY()  << ", " << inputImage->getSizeZ()  << std::endl;
        SDenoise3d process;
        process.addObserver(observer);
        process.setInput( inputImage );
        process.setMethod(method);
        process.setRegularization(regularization);
        process.setWeighting(weighting);
        process.setIterationsNumber(niter);
        process.setVerbose(verbose);
        process.setDelta(delta);
        SImg::tic();
        process.run();
        SImg::toc();
        SImage* outputImage = process.getOutput();
        SImageReader::write(outputImage, outputImageFile);

        delete inputImage;
        delete outputImage;

    }
    catch (SException &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }
    catch (std::exception &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }

    delete observer;
    return 0;
}
