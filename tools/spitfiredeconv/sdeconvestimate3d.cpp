#include <iostream>
#include <score>
#include <scli>
#include <sdataio>
#include <simageio>
#include <spitfiredeconv>
#include <sroi>

int main(int argc, char *argv[])
{
    SObserverConsole* observer = new SObserverConsole();
    try
    {
        SCliParser cmdParser(argc, argv);
        cmdParser.addInputData("-i", "Input image file");
        cmdParser.addInputData("-roi", "Input roi file");
        cmdParser.addInputData("-psf", "Input PSF file");
        cmdParser.addOutputData("-ocurve", "Output curve file");
        cmdParser.addOutputData("-ovalue", "Output value file");

        cmdParser.addParameterSelect("-method", "Deconvolution method 'SV' or 'HSV", "HSV");
        cmdParser.addParameterFloat("-delta", "Depth delta resolution", 1.0);
        cmdParser.addParameterSelect("-normalization", "Intansity normalization ['max', 'L2', 'sum', '8bits', '12bits', '16bits']", "L2");

        cmdParser.addParameterFloat("-weighting", "Weighting parameter", 0.6);
        cmdParser.addParameterFloat("-rmin", "Regularization parameter minimum pow(2,-x)", 5.0);
        cmdParser.addParameterFloat("-rmax", "Regularization parameter maximum pow(2,-x)", 20.0);
        cmdParser.addParameterFloat("-rstep", "Regularization step", 1.0);

        cmdParser.addParameterBoolean("-verbose", "Print iterations to console", true);
        cmdParser.setMan("Regularization parameter estimaition for 3D SPARTION deconvolution");
        cmdParser.parse(2);


        std::string inputImageFile = cmdParser.getDataURI("-i");
        std::string inputRoiFile = cmdParser.getDataURI("-roi");
        std::string inputPSFFile = cmdParser.getDataURI("-psf");
        std::string outputCurveFile = cmdParser.getDataURI("-ocurve");
        std::string outputValueFile = cmdParser.getDataURI("-ovalue");

        std::cout << "read method " << std::endl;
        const std::string method = cmdParser.getParameterString("-method");
        std::cout << "read delta " << std::endl;
        const float delta = cmdParser.getParameterFloat("-delta");
        std::cout << "read normalization " << std::endl;
        const std::string normalisation = cmdParser.getParameterString("-normalization");

        std::cout << "read weighting " << std::endl;
        const float weighting = cmdParser.getParameterFloat("-weighting");
        const float rmin = cmdParser.getParameterFloat("-rmin");
        const float rmax = cmdParser.getParameterFloat("-rmax");
        const float rstep = cmdParser.getParameterFloat("-rstep");

        std::cout << "read verbose " << std::endl;
        const bool verbose = cmdParser.getParameterBool("-verbose");

        if (inputImageFile == ""){
            observer->message("SDeconvEstimate3d: Input image path is empty");
            return 1;
        }

        if (verbose){
            observer->message("SDeconvEstimate3d: input image: " + inputImageFile);
            observer->message("SDeconvEstimate3d: psf: " + inputPSFFile);
            observer->message("SDeconvEstimate3d: method: " + method);
            observer->message("SDeconvEstimate3d: regularization min: " + std::to_string(rmin));
            observer->message("SDeconvEstimate3d: regularization max: " + std::to_string(rmax));
            observer->message("SDeconvEstimate3d: regularization step: " + std::to_string(rstep));
            observer->message("SDeconvEstimate3d: weighting parameter: " + std::to_string(weighting));
        }

        // Run process
        SImage* inputImage = SImageReader::read(inputImageFile, 32);
        SImage* psfImage = SImageReader::read(inputPSFFile, 32);

        SDeconvEstimate3d process;
        process.addObserver(observer);
        process.setInput( inputImage );
        if (inputRoiFile != ""){
            SRoiReader reader;
            reader.setFile(inputRoiFile);
            reader.run();
            std::vector<SRoi*> rois = reader.getRois();
            std::cout << "rois size = " << rois.size() << std::endl;
            if (rois.size() > 0){
                process.setROI(rois[0]);
            }
        }
        process.setMethod(method);
        process.setPSF(psfImage);
        process.setDelta(delta);
        process.setRegularizationRange(rmin, rmax, rstep);
        process.setWeighting(weighting);
        process.setIntensityNormalization(normalisation);
        process.setVerbose(verbose);
        SImg::tic();
        process.run();
        SImg::toc();

        // save outputs
        if (outputCurveFile != ""){
            SArray* curve = process.getEstimationCurve();
            SCSV writer;
            writer.set(curve);
            writer.write(outputCurveFile);
            delete curve;
        }
        if (outputValueFile != ""){
            SFloat *value = new SFloat(process.getEstimatedRegularization());
            SCSV writer;
            writer.set(value);
            writer.write(outputValueFile);
            delete value;
        }
        delete inputImage;
    }
    catch (SException &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }
    catch (std::exception &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }

    delete observer;
    return 0;
}
