#include <iostream>
#include <score>
#include <scli>
#include <simageio>
#include <spitfiredeconv>

int main(int argc, char *argv[])
{
    SObserverConsole* observer = new SObserverConsole();
    try
    {
        // Parse inputs
        SCliParser cmdParser(argc, argv);
        cmdParser.addParameterFloat("-sigmaxy", "PSF width and height", 1.0);
        cmdParser.addParameterFloat("-sigmaz" ,"PSF depth", 1.0);
        cmdParser.addParameterInt("-width" ,"image width", 256);
        cmdParser.addParameterInt("-height" ,"image height", 256);
        cmdParser.addParameterInt("-depth" ,"image depth", 256);
        cmdParser.addOutputData("-o", "Output image file");

        cmdParser.setMan("Generate a Gaussian PSF");
        cmdParser.parse(4);

        float sigmaxy = cmdParser.getParameterFloat("-sigmaxy");
        float sigmaz = cmdParser.getParameterFloat("-sigmaz");
        unsigned int width = cmdParser.getParameterInt("-width");
        unsigned int height = cmdParser.getParameterInt("-height");
        unsigned int depth = cmdParser.getParameterInt("-depth");
        std::string outputImageFile = cmdParser.getDataURI("-o");

        if (sigmaxy < 0 || sigmaz < 0){
            observer->message("sigma must be positive");
            return 1;
        }

        if (outputImageFile == ""){
            observer->message("Output image file path is empty");
            return 1;
        }

        SGaussianPSF psfMaker;
        psfMaker.setSigma(sigmaxy, sigmaxy, sigmaz);
        psfMaker.setImageSize(width, height, depth);
        SImage* image = psfMaker.getPSF();

        SImageReader::write(image, outputImageFile);

    }
    catch (SException &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }
    catch (std::exception &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }

    delete observer;
    return 0;
}
