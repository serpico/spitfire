#include <iostream>
#include <score>
#include <scli>
#include <simageio>
#include <spitfiredeconv>
#include <sroi>
#include <sdataio>

int main(int argc, char *argv[])
{
    SObserverConsole* observer = new SObserverConsole();
    try
    {
        SCliParser cmdParser(argc, argv);
        cmdParser.addInputData("-i", "Input image file");
        cmdParser.addInputData("-roi", "Input roi file");
        cmdParser.addOutputData("-ocurve", "Output curve file");
        cmdParser.addOutputData("-ovalue", "Output value file");

        cmdParser.addParameterSelect("-method", "Deconvolution method 'SV' or 'HSV", "HSV");
        cmdParser.addParameterFloat("-sigma", "PSF width", 1.5);
        cmdParser.addParameterFloat("-regularization", "Regularization parameter as pow(2,-x)", 12);
        cmdParser.addParameterFloat("-weighting", "Weighting parameter", 0.6);
        cmdParser.addParameterFloat("-rmin", "Regularization parameter minimum pow(2,-x)", 5.0);
        cmdParser.addParameterFloat("-rmax", "Regularization parameter maximum pow(2,-x)", 20.0);
        cmdParser.addParameterFloat("-rstep", "Regularization step", 1.0);
        cmdParser.addParameterString("-normalization", "Intensity normalization step", "L2");
        cmdParser.addParameterBoolean("-verbose", "Print iterations to console", true);
        cmdParser.setMan("Estimate regularization for SPARTION2D deconvolution");
        cmdParser.parse(2);


        std::string inputImageFile = cmdParser.getDataURI("-i");
        std::string inputRoiFile = cmdParser.getDataURI("-roi");
        std::string outputCurveFile = cmdParser.getDataURI("-ocurve");
        std::string outputValueFile = cmdParser.getDataURI("-ovalue");

        const std::string method = cmdParser.getParameterString("-method");
        const float sigma = cmdParser.getParameterFloat("-sigma");
        const float rmin = cmdParser.getParameterFloat("-rmin");
        const float rmax = cmdParser.getParameterFloat("-rmax");
        const float rstep = cmdParser.getParameterFloat("-rstep");
        const float weighting = cmdParser.getParameterFloat("-weighting");
        const std::string normalisation = cmdParser.getParameterString("-normalization");
        const bool verbose = cmdParser.getParameterBool("-verbose");

        if (inputImageFile == ""){
            observer->message("SDeconvEstimate2d: Input image path is empty");
            return 1;
        }

        if (verbose){
            observer->message("SDeconvEstimate2d: input image: " + inputImageFile);
            observer->message("SDeconvEstimate2d: sigma psf: " + std::to_string(sigma));
            observer->message("SDeconvEstimate2d: method: " + method);
            observer->message("SDeconvEstimate2d: regularization min: " + std::to_string(rmin));
            observer->message("SDeconvEstimate2d: regularization max: " + std::to_string(rmax));
            observer->message("SDeconvEstimate2d: regularization step: " + std::to_string(rstep));
            observer->message("SDeconvEstimate2d: weighting parameter: " + std::to_string(weighting));
        }

        // Run process
        SImage* inputImage = SImageReader::read(inputImageFile, 32);

        SDeconvEstimate2d process;
        process.addObserver(observer);
        if (inputRoiFile != ""){
            SRoiReader reader;
            reader.setFile(inputRoiFile);
            reader.run();
            std::vector<SRoi*> rois = reader.getRois();
            if (rois.size() > 0){
                process.setROI(rois[0]);
            }
        }
        process.setInput( inputImage );
        process.setMethod(method);
        process.setSigma(sigma);
        process.setRegularizationRange(rmin, rmax, rstep);
        process.setWeighting(weighting);
        process.setIntensityNormalization(normalisation);
        process.setVerbose(verbose);
        SImg::tic();
        process.run();
        SImg::toc();

        // save outputs
        if (outputCurveFile != ""){
            SArray* curve = process.getEstimationCurve();
            SCSV writer;
            writer.set(curve);
            writer.write(outputCurveFile);
            delete curve;
        }
        if (outputValueFile != ""){
            SFloat *value = new SFloat(process.getEstimatedRegularization());
            SCSV writer;
            writer.set(value);
            writer.write(outputValueFile);
            delete value;
        }
        delete inputImage;
    }
    catch (SException &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }
    catch (std::exception &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }

    delete observer;
    return 0;
}
