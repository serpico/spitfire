#include <iostream>
#include <score>
#include <scli>
#include <sdataio>
#include <simageio>
#include <spitfiredeconv>
#include <spitfireairyscan>

int main(int argc, char *argv[])
{
    SObserverConsole* observer = new SObserverConsole();
    try
    {
        SCliParser cmdParser(argc, argv);
        cmdParser.addInputData("-i", "Input image file");
        cmdParser.addOutputData("-o", "Output image file");

        cmdParser.addParameterSelect("-method", "Deconvolution method 'SV' or 'HSV", "HSV");
        cmdParser.addParameterFloat("-sigma", "PSF width", 1.5);
        cmdParser.addParameterFloat("-regularization", "Regularization parameter as pow(2,-x)", 12);
        cmdParser.addParameterFloat("-weighting", "Weighting parameter", 0.6);
        cmdParser.addParameterInt("-niter", "Nb iterations", 200);
        cmdParser.addParameterBoolean("-stack", "Process 3D data as a 2D stack (for airyscan)", false);
        cmdParser.addParameterString("-scoeff", "Stack coeff ['mean', 'airyscand2c', 'airyscanid2c', 'airyscanidx', 'airyscaniidx', 'airyscanstep']", "");
        cmdParser.addParameterInt("-scoeffstepidx", "step index for the airyscanstep method. [7,19]", 7);
        cmdParser.addParameterFloat("-scoeffstepcoeff", "Coefficient of the step depth forairyscanstep method (in [0,1]) ", 0.5);
        cmdParser.addParameterFloat("-scoeffslope", "Slope of the coefficent curve", 3.0414);
        cmdParser.addParameterSelect("-normalization", "Intensity normalization ['max', 'L2', 'sum', '8bits', '12bits', '16bits']", "L2");
        cmdParser.addParameter("-energyfile", "Energy file", "");
        cmdParser.addParameterBoolean("-verbose", "Print iterations to console", true);
        cmdParser.setMan("Deconvolute a 2D image with SPARTION method");
        cmdParser.parse(2);


        std::string inputImageFile = cmdParser.getDataURI("-i");
        std::string outputImageFile = cmdParser.getDataURI("-o");

        const std::string method = cmdParser.getParameterString("-method");
        const float sigma = cmdParser.getParameterFloat("-sigma");
        const float regularization = cmdParser.getParameterFloat("-regularization");
        const float weighting = cmdParser.getParameterFloat("-weighting");
        const int niter = cmdParser.getParameterInt("-niter");
        const bool stack = cmdParser.getParameterBool("-stack");
        const std::string scoeff = cmdParser.getParameterString("-scoeff");
        const int scoeffstepidx = cmdParser.getParameterInt("-scoeffstepidx");
        const float scoeffstepcoeff = cmdParser.getParameterFloat("-scoeffstepcoeff");
        const float scoeffslope = cmdParser.getParameterFloat("-scoeffslope");
        const std::string normalisation = cmdParser.getParameterString("-normalization");
        const std::string energyFile = cmdParser.getParameterString("-energyfile");
        const bool verbose = cmdParser.getParameterBool("-verbose");

        if (inputImageFile == ""){
            observer->message("SDeconv2d: Input image path is empty");
            return 1;
        }

        if (verbose){
            observer->message("SDeconv2d: input image: " + inputImageFile);
            observer->message("SDeconv2d: output image: " + outputImageFile);
            observer->message("SDeconv2d: sigma psf: " + std::to_string(sigma));
            observer->message("SDeconv2d: method: " + method);
            observer->message("SDeconv2d: regularization parameter: " + std::to_string(regularization));
            observer->message("SDeconv2d: weighting parameter: " + std::to_string(weighting));
            observer->message("SDeconv2d: nb iterations: " + std::to_string(niter));
            observer->message("SDeconv2d: normalisation: " + normalisation);

        }

        // Run process
        SImage* inputImage = SImageReader::read(inputImageFile, 32);

        SDeconv2d process;
        process.addObserver(observer);
        process.setInput( inputImage );
        process.setMethod(method);
        process.setSigmaPSF(sigma);
        process.setRegularization(regularization);
        process.setWeighting(weighting);
        process.setIterationsNumber(niter);
        process.setUseStack(stack);
        if (scoeff != ""){
            SAiryscanWeights weightsObj;
            if (std::string(scoeff) == "mean"){
                process.setStackCoefficients(weightsObj.mean());
            }
            else if (std::string(scoeff) == "airyscand2c"){
                process.setStackCoefficients(weightsObj.distanceToCenter(scoeffslope));
            }
            else if (std::string(scoeff) == "airyscanid2c"){
                process.setStackCoefficients(weightsObj.invertedDistanceToCenter(scoeffslope));
            }
            else if (std::string(scoeff) == "airyscanidx"){
                process.setStackCoefficients(weightsObj.indexDistance(scoeffslope));
            }
            else if (std::string(scoeff) == "airyscaniidx"){
                process.setStackCoefficients(weightsObj.indexDistanceInv(scoeffslope));
            }
            else if (std::string(scoeff) == "airyscanstep"){
                process.setStackCoefficients(weightsObj.stepFunction(scoeffstepidx, scoeffstepcoeff));
            }
        }

        process.setIntensityNormalization(normalisation);
        if (energyFile != ""){
            process.setCalculateFinalEnergy(true);
        }
        process.setVerbose(verbose);
        SImg::tic();
        process.run();
        SImg::toc();

        // save outputs
        SImage* outputImage = process.getOutput();
        SImageReader::write(outputImage, outputImageFile);

        if (energyFile != ""){
            SArray* energies = process.getEnergyArray();
            SCSV csvReader;
            csvReader.set(energies);
            csvReader.write(energyFile);
            delete energies;
        }

        delete inputImage;
        delete outputImage;
    }
    catch (SException &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }
    catch (std::exception &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }
    delete observer;
    return 0;
}
