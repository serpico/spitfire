/// \file SDenoise4d.h
/// \brief SDenoise4d class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2020

#pragma once

#include <string>
#include "spitfiredenoiseExport.h"

#include "simage/SImageFloat.h"
#include "simage/SImageFilter.h"

/// \class SDenoise4d
/// \brief 4D (3D+t) image denoising with the spation algorithm
class SPITFIREDENOISE_EXPORT SDenoise4d : public SImageFilter{

public:
    SDenoise4d();

public:
    void setDeltaZ(float deltaZ);
    void setDeltaT(float deltaT);
    void setMethod(std::string method);
    void setRegularization(float regularization);
    void setWeighting(float weighting);
    void setIterationsNumber(int iterationsNumber);
    void setVerbose(bool verbose);

public:
    void setInput(SImage* image);
    SImage* getOutput();

public:
    void checkInputs();
    void run();

protected:
    SImageFloat* runSV(SImageFloat* image);
    SImageFloat* runHSV(SImageFloat* image);

protected:
    float m_regularization;
    float m_weighting;
    int m_iterationsNumber;
    bool m_verbose;
    float m_deltaZ;
    float m_deltaT;
    std::string m_method;

protected:
    SImage* m_input;
    SImage* m_output;

protected:
    int m_processPrecision;
    bool m_processZ;
    bool m_processT;
    bool m_processC;
};
