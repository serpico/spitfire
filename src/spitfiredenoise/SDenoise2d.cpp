/// \file SDenoise2d.cpp
/// \brief SDenoise2d class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2018

#include <iostream>
#include "math.h"

#include "SDenoise2d.h"

#include "SpitfireException.h"
#include "score/SMath.h"
#include "score/SCoreShortcuts.h"
#include "simage/SImageMath.h"
#include "smanipulate/SNormalize.h"

#ifdef SL_USE_OPENMP
#include "omp.h"
#endif

SDenoise2d::SDenoise2d() : SImageFilter(){
    m_processPrecision = 32;
    m_processZ = true;
    m_processT = false;
    m_processC = false;

    m_verbose = true;
    m_input = nullptr;
    m_output = nullptr;
    m_useStack = false;
}

void SDenoise2d::setInput(SImage* image){
    m_input = image;
}

SImage* SDenoise2d::getOutput(){
    return m_output;
}

void SDenoise2d::setMethod(std::string method){
    m_method = method;
}

void SDenoise2d::setRegularization(float regularization){
    m_regularization = pow(2,-regularization);
}

void SDenoise2d::setWeighting(float weighting){
    m_weighting = weighting;
}

void SDenoise2d::setIterationsNumber(int iterationsNumber){
    m_iterationsNumber = iterationsNumber;
}

void SDenoise2d::setVerbose(bool verbose){
    m_verbose = verbose;
}

void SDenoise2d::setUseStack(bool useStack){
    m_useStack = useStack;
}

void SDenoise2d::setStackCoefficients(std::vector<float> coefficients){
    m_stackCoefficients = coefficients;

    if (m_verbose){
        std::cout << "Stack coefficients:" << std::endl;
        for (unsigned int i = 0 ; i < coefficients.size() ; i++){
            std::cout << coefficients[i] << std::endl;
        }
        std::cout << std::endl;
    }
}

void SDenoise2d::run(){

    if (!m_input)
    {
        throw(SpitfireException("Error: Input image not set"));
    }

    if (m_useStack){
        this->runStack();
    }
    else{
        if (m_input->getSizeZ() == 1){
            this->runOnce();
        }
        else{
            this->runSlices();
        }
    }
}

void SDenoise2d::runStack(){

    if (m_verbose){
        this->notify("Run stack mode");
    }

    if (m_stackCoefficients.size() == 0){
        this->notify("WARNING: Stack coefficients empty - use mean");
        m_stackCoefficients.resize(m_input->getSizeZ());
        unsigned int sz = m_input->getSizeZ();
        for (unsigned int z = 0 ; z < sz ; z++){
            m_stackCoefficients[z] = 1/float(sz);
        }
    }

    if (m_stackCoefficients.size() != m_input->getSizeZ()){
        throw SpitfireException(std::string("Error: stack coefficients size (" + SImg::int2string(m_stackCoefficients.size()) + ") different of stack size (" + SImg::int2string(m_input->getSizeZ())+ ")").c_str());
    }

    SImageFloat* inputFloat = dynamic_cast<SImageFloat*>(m_input);

    //float maxInput = inputFloat->getMax();
    //float minInput = inputFloat->getMin();
    SNormalize normInputFilter;
    normInputFilter.setMethod(SNormalize::Max);
    normInputFilter.setInput(inputFloat);
    normInputFilter.run();
    SImageFloat* normInput = dynamic_cast<SImageFloat*>(normInputFilter.getOutput());

    if (m_method == "SV"){
        if (m_verbose){
            this->notify("use SV");
        }
        m_output = this->runSVStack(normInput);
    }
    else if (m_method == "HSV"){
        if (m_verbose){
            this->notify("use HSV");
        }
        m_output = this->runHSVStack(normInput);
    }
    else{
        throw SpitfireException("method must be SV or HSV");
    }
}

void SDenoise2d::runOnce()
{
    // normalize input
    SImageFloat* inputFloat = dynamic_cast<SImageFloat*>(m_input);
    float maxInput = inputFloat->getMax();
    float minInput = inputFloat->getMin();
    SNormalize normInputFilter;
    normInputFilter.setMethod(SNormalize::Max);
    normInputFilter.setInput(inputFloat);
    normInputFilter.run();
    SImageFloat* normInput = dynamic_cast<SImageFloat*>(normInputFilter.getOutput());

    SImageFloat* outputImage = nullptr;
    if (m_method == "SV"){
        if (m_verbose){
            this->notify("use SV");
        }
        outputImage = this->runSV(normInput);
    }
    else if (m_method == "HSV"){
        if (m_verbose){
            this->notify("use HSV");
        }
        outputImage = this->runHSV(normInput);
    }
    else{
        throw SpitfireException("method must be SV or HSV");
    }

    SImageMath::multiply(outputImage, maxInput-minInput);
    SImageMath::add(outputImage, minInput);
    m_output = outputImage;
}

void SDenoise2d::runSlices()
{
    unsigned int sx = m_input->getSizeX();
    unsigned int sy = m_input->getSizeY();
    unsigned int sz = m_input->getSizeZ();
    SImageFloat* outputImage = new SImageFloat(sx, sy, sz);
    float* outputImageBuffer = outputImage->getBuffer();
    SImageFloat* inputImage = dynamic_cast<SImageFloat*>(m_input);
    for (unsigned int z = 0; z < inputImage->getSizeZ(); ++z)
    {
        SImageFloat* input = inputImage->getSlice(z);

        float maxInput = input->getMax();
        float minInput = input->getMin();
        SNormalize normInputFilter;
        normInputFilter.setMethod(SNormalize::Max);
        normInputFilter.setInput(input);
        normInputFilter.run();
        SImageFloat* normInput = dynamic_cast<SImageFloat*>(normInputFilter.getOutput());

        SImageFloat* outputSlice = nullptr;
        if (m_method == "SV"){
            if (m_verbose){
                this->notify("use SV");
            }
            outputSlice = this->runSV(normInput);
        }
        else if (m_method == "HSV"){
            if (m_verbose){
                this->notify("use HSV");
            }
            outputSlice = this->runHSV(normInput);
        }
        else{
            throw SpitfireException("method must be SV or HSV");
        }

        SImageMath::multiply(outputImage, maxInput-minInput);
        SImageMath::add(outputImage, minInput);

        // copy slice to m_output

        float * outputSliceBuffer = outputSlice->getBuffer();
        for (unsigned int x = 0 ; x < sx ; x++){
            for (unsigned int y = 0 ; y < sy ; y++){
                outputImageBuffer[z + sz*(y + sy*x)] = outputSliceBuffer[sy*x+y];
            }
        }
    }
    m_output = outputImage;
}

SImageFloat* SDenoise2d::runSV(SImageFloat* image){

    unsigned int img_width = image->getSizeX();
    unsigned int img_height = image->getSizeY();
    unsigned int N = img_width*img_height;

    // Splitting parameters
    float dual_step = SMath::max(0.01, SMath::min(0.1, m_regularization));
    float primal_step = 0.99
            / (0.5
               + (8 * pow(m_weighting, 2.)
                  + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);

    // Initializations
    float* denoised_image = (float*) malloc(sizeof(float) * N);
    float* dual_images0 = (float*) malloc(sizeof(float) * N);
    float* dual_images1 = (float*) malloc(sizeof(float) * N);
    float* dual_images2 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* noisy_image = image->getBuffer();

    for (unsigned int i = 0 ; i < N ; i++){
        denoised_image[i] = noisy_image[i];
    }

    // Deconvolution process
    float tmp, dx, dy, min_val, max_val, dx_adj, dy_adj;
    int p, pxm, pym, pxp, pyp;
    min_val = 0.0;
    max_val = 1.0;
    for (int iter = 0; iter < m_iterationsNumber; iter++) {
        // Primal optimization

        for (unsigned int i = 0 ; i < N ; i++){
            auxiliary_image[i] = denoised_image[i];
        }

        for(unsigned int x = 0 ; x < img_width ; x++){
            for(unsigned int y = 0 ; y < img_height ; y++){

                p = img_height*x + y;
                pxm = img_height*(x-1) + y;
                pym = img_height*x + y-1;

                tmp = denoised_image[p] - primal_step * (denoised_image[p] - noisy_image[p]);


                if (x > 0){
                    dx_adj = dual_images0[pxm] - dual_images0[p];
                }
                else{
                    dx_adj = dual_images0[p];
                }

                if (y > 0){
                    dy_adj = dual_images1[pym] - dual_images1[p];
                }
                else{
                    dy_adj = dual_images1[p];
                }

                tmp -= (primal_weight * (dx_adj + dy_adj)
                        + primal_weight_comp * dual_images2[p]);

                denoised_image[p] = SMath::max(min_val, SMath::min(max_val, tmp));
            }
        }

        // Stopping criterion
        if (m_verbose)
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0)
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));


        // Dual optimization
        for(unsigned int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * denoised_image[i] - auxiliary_image[i];
        }


        for(unsigned int x = 0 ; x < img_width ; x++){
            for(unsigned int y = 0 ; y < img_height ; y++){

                p = img_height*x + y;
                pxp = img_height*(x+1) + y;
                pyp = img_height*x + y+1;

                if (x < img_width - 1) {
                    dx = auxiliary_image[pxp]- auxiliary_image[p];
                    dual_images0[p] += dual_weight * dx;
                }
                if (y < img_height - 1) {
                    dy = auxiliary_image[pyp] - auxiliary_image[p];
                    dual_images1[p] += dual_weight * dy;
                }
                dual_images2[p] += dual_weight_comp * auxiliary_image[p];
            }
        }

        for(unsigned int i = 0 ; i < N ; i++){
            float tmp = SMath::max(1.,
                                    1. / m_regularization
                                    * sqrt(
                                        pow(dual_images0[i], 2.)
                                        + pow(dual_images1[i], 2.)
                                        + pow(dual_images2[i], 2.)));
            dual_images0[i] /= tmp;
            dual_images1[i] /= tmp;
            dual_images2[i] /= tmp;
        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    delete dual_images0;
    delete dual_images1;
    delete dual_images2;
    delete auxiliary_image;
    this->notifyProgress(100);
    return new SImageFloat(denoised_image, img_width, img_height);
}

SImageFloat* SDenoise2d::runHSV(SImageFloat* image){

#ifdef SL_USE_OPENMP
    omp_set_num_threads(omp_get_max_threads());
#endif

    unsigned int img_width = image->getSizeX();
    unsigned int img_height = image->getSizeY();
    unsigned int N = img_width*img_height;
    float sqrt2 = sqrt(2.);

    // Splitting parameters
    float dual_step = SMath::max(0.001, SMath::min(0.01, m_regularization));
    float primal_step = 0.99 / (0.5 + (64 * pow(m_weighting, 2.) + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);

    // Initializations
    float* denoised_image = (float*) malloc(sizeof(float) * N);
    float* dual_images0 = (float*) malloc(sizeof(float) * N);
    float* dual_images1 = (float*) malloc(sizeof(float) * N);
    float* dual_images2 = (float*) malloc(sizeof(float) * N);
    float* dual_images3 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* noisy_image = image->getBuffer();

    for (unsigned int i = 0 ; i < N ; ++i){
        denoised_image[i] = noisy_image[i];
        dual_images0[i] = 0.0;
        dual_images1[i] = 0.0;
        dual_images2[i] = 0.0;
        dual_images3[i] = 0.0;
    }

    // Deconvolution process
    //float dxx, dyy, dxy, min_val, max_val, dxx_adj, dyy_adj, dxy_adj;
    //int p, pxm, pxp, pym, pyp, pxym, pxyp;
    float inv_reg = 1.0 / m_regularization;
    //float tmp, inv_tmp;
    for (int iter = 0; iter < m_iterationsNumber; ++iter) {
        // Primal optimization

#pragma omp parallel for 
        for (unsigned int i = 0 ; i < N ; ++i){
            auxiliary_image[i] = denoised_image[i];
        }

#pragma omp parallel for 
        for (unsigned int x = 1 ; x < img_width-1 ; ++x){
            for (unsigned int y = 1 ; y < img_height-1 ; ++y){

                float tmp, dxx_adj, dyy_adj, dxy_adj;
                int p, pxm, pxp, pym, pyp, pxym;    

                p = img_height*x+y;
                pxm = p - img_height;
                pxp = p + img_height;
                pym = p-1;
                pyp = p+1;
                pxym = pxm-1;

                tmp = denoised_image[p] - primal_step * (denoised_image[p] - noisy_image[p]);
                dxx_adj = dual_images0[pxm] - 2 * dual_images0[p] + dual_images0[pxp];
                dyy_adj = dual_images1[pym] - 2 * dual_images1[p] + dual_images1[pyp];
                dxy_adj = dual_images2[p] - dual_images2[pxm] - dual_images2[pym] + dual_images2[pxym];
                tmp -= (primal_weight * (dxx_adj + dyy_adj + sqrt2 * dxy_adj) + primal_weight_comp * dual_images3[p]);

                if (tmp > 1.0){
                    denoised_image[p] = 1.0;
                }
                else if (tmp < 0.0 ){
                    denoised_image[p] = 0.0;
                }
                else{
                    denoised_image[p] = tmp;
                }
            }
        }

        // Stopping criterion
        if (m_verbose){
            int iter_n = m_iterationsNumber / 10;
            if (iter_n < 1) iter_n = 1;
            if (iter % iter_n == 0){
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
            }
        }

        // Dual optimization
        #pragma omp parallel for
        for (unsigned int i = 0 ; i < N ; ++i){
            auxiliary_image[i] = 2 * denoised_image[i] - auxiliary_image[i];
        }

#pragma omp parallel for
        for (unsigned int x = 1 ; x < img_width-1 ; ++x){
            for (unsigned int y = 1 ; y < img_height-1 ; ++y){

                float dxx, dyy, dxy;
                int p, pxm, pxp, pym, pyp, pxyp; 

                p = img_height*x+y;
                pxm = p - img_height;
                pxp = p + img_height;
                pym = p-1;
                pyp = p+1;
                pxyp = pxp+1;

                dxx = auxiliary_image[pxp] - 2 * auxiliary_image[p] + auxiliary_image[pxm];
                dual_images0[p] += dual_weight * dxx;
                
                dyy = auxiliary_image[pyp] - 2 * auxiliary_image[p] + auxiliary_image[pym];
                dual_images1[p] += dual_weight * dyy;
                
                dxy = auxiliary_image[pxyp] - auxiliary_image[pxp]  - auxiliary_image[pyp] + auxiliary_image[p];
                dual_images2[p] += sqrt2 * dual_weight * dxy;
                
                dual_images3[p] += dual_weight_comp * auxiliary_image[p];
            }
        }

#pragma omp parallel for
        for( unsigned int i = 0 ; i < N ; ++i) 
        {
            float tmp = inv_reg * sqrt( dual_images0[i]*dual_images0[i] + dual_images1[i]*dual_images1[i] + dual_images2[i]*dual_images2[i] + dual_images3[i]*dual_images3[i]);
            //float tmp = inv_reg*inv_reg * ( dual_images0[i]*dual_images0[i] + dual_images1[i]*dual_images1[i] + dual_images2[i]*dual_images2[i] + dual_images3[i]*dual_images3[i]);
            
            if (tmp > 1.0)
            {
                float inv_tmp = 1.0/tmp;
                dual_images0[i] *= inv_tmp;
                dual_images1[i] *= inv_tmp;
                dual_images2[i] *= inv_tmp;
                dual_images3[i] *= inv_tmp;
            }
        }
    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    delete dual_images0;
    delete dual_images1;
    delete dual_images2;
    delete dual_images3;
    delete auxiliary_image;
    this->notifyProgress(100);
    return new SImageFloat(denoised_image, img_width, img_height);
}

SImageFloat* SDenoise2d::runSVStack(SImageFloat* image){

    unsigned int img_width = image->getSizeX();
    unsigned int img_height = image->getSizeY();
    unsigned int img_depth = image->getSizeZ();
    unsigned int N = img_width*img_height;

    // Splitting parameters
    float dual_step = SMath::max(0.01, SMath::min(0.1, m_regularization));
    float primal_step = 0.99
            / (0.5
               + (8 * pow(m_weighting, 2.)
                  + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);

    // Initializations
    float* denoised_image = (float*) malloc(sizeof(float) * N);
    float* dual_images0 = (float*) malloc(sizeof(float) * N);
    float* dual_images1 = (float*) malloc(sizeof(float) * N);
    float* dual_images2 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* noisy_image = image->getBuffer();

    // init denoise to first slice
    for(unsigned int x = 0 ; x < img_width ; x++){
        for(unsigned int y = 0 ; y < img_height ; y++){
            denoised_image[img_height*x+y] = noisy_image[0 + img_depth*(img_height*x+y)];
        }
    }

    // Deconvolution process
    float tmp, dx, dy, min_val, max_val, dx_adj, dy_adj;
    int p, pxm, pym, pxp, pyp;
    min_val = 0.0;
    max_val = 1.0;
    float residu;

    for (int iter = 0; iter < m_iterationsNumber; iter++) {
        // Primal optimization
        for (unsigned int i = 0 ; i < N ; i++){
            auxiliary_image[i] = denoised_image[i];
        }

        for(unsigned int x = 0 ; x < img_width ; x++){
            for(unsigned int y = 0 ; y < img_height ; y++){

                p = img_height*x+y;
                pxm = img_height*(x-1)+y;
                pym = img_height*x+y-1;

                residu = 0;
                for(unsigned int z = 0 ; z < img_depth ; z++){
                    residu += m_stackCoefficients[z]*(denoised_image[img_height*x+y] - noisy_image[z + img_depth*p]);
                }

                tmp = denoised_image[p]
                        - primal_step * (residu);

                if (x > 0){
                    dx_adj = dual_images0[pxm] - dual_images0[p];
                }
                else{
                    dx_adj = dual_images0[p];
                }

                if (y > 0){
                    dy_adj = dual_images1[pym] - dual_images1[p];
                }
                else{
                    dy_adj = dual_images1[p];
                }

                tmp -= (primal_weight * (dx_adj + dy_adj)
                        + primal_weight_comp * dual_images2[p]);

                denoised_image[p] = SMath::max(min_val, SMath::min(max_val, tmp));
            }
        }

        // Stopping criterion
        if (m_verbose)
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0)
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));

        // Dual optimization
        for(unsigned int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * denoised_image[i] - auxiliary_image[i];
        }

        for(unsigned int x = 0 ; x < img_width ; x++){
            for(unsigned int y = 0 ; y < img_width ; y++){

                p = img_height*x+y;
                pxp = img_height*(x+1)+y;
                pyp = img_height*x+y+1;

                if (x < img_width - 1) {
                    dx = auxiliary_image[pxp] - auxiliary_image[p];
                    dual_images0[p] += dual_weight * dx;
                }
                if (y < img_height - 1) {
                    dy = auxiliary_image[pyp] - auxiliary_image[p];
                    dual_images1[p] += dual_weight * dy;
                }
                dual_images2[p] += dual_weight_comp * auxiliary_image[p];
            }
        }

        for(unsigned int i = 0 ; i < img_width ; i++){
            float tmp = SMath::max(1.,
                                    1. / m_regularization
                                    * sqrt(
                                        pow(dual_images0[i], 2.)
                                        + pow(dual_images1[i], 2.)
                                        + pow(dual_images2[i], 2.)));
            dual_images0[i] /= tmp;
            dual_images1[i] /= tmp;
            dual_images2[i] /= tmp;

        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    delete dual_images0;
    delete dual_images1;
    delete dual_images2;
    delete auxiliary_image;
    return new SImageFloat(denoised_image, img_width, img_height);
}

SImageFloat* SDenoise2d::runHSVStack(SImageFloat* image){

    unsigned int img_width = image->getSizeX();
    unsigned int img_height = image->getSizeY();
    unsigned int img_depth = image->getSizeZ();
    unsigned int N = img_width*img_height;
    float sqrt2 = sqrt(2.);

    // Splitting parameters
    float dual_step = SMath::max(0.001, SMath::min(0.01, m_regularization));
    float primal_step = 0.99
            / (0.5
               + (64 * pow(m_weighting, 2.)
                  + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);

    // Initializations
    float* denoised_image = (float*) malloc(sizeof(float) * N);
    float* dual_images0 = (float*) malloc(sizeof(float) * N);
    float* dual_images1 = (float*) malloc(sizeof(float) * N);
    float* dual_images2 = (float*) malloc(sizeof(float) * N);
    float* dual_images3 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* noisy_image = image->getBuffer();

    // init denoise to first slice
    for(unsigned int x = 0 ; x < img_width ; x++){
        for(unsigned int y = 0 ; y < img_height ; y++){
            denoised_image[img_height*x+y] = noisy_image[0 + img_depth*(img_height*x+y)];
        }
    }

    // Deconvolution process
    float tmp, dxx, dyy, dxy, min_val, max_val, dxx_adj, dyy_adj, dxy_adj;
    float residu;
    min_val = 0.0;
    max_val = 0.0;
    int p, pxm, pxp, pym, pyp;
    for (int iter = 0; iter < m_iterationsNumber; iter++) {
        // Primal optimization

        for (unsigned int i = 0 ; i < N ; i ++){
            auxiliary_image[i] = denoised_image[i];
        }

        for (unsigned int x = 0 ; x < img_width ; x++)
        {
            for (unsigned int y = 0 ; y < img_height ; y++)
            {
                residu = 0;
                p = img_height*x + y;
                pxm = img_height*(x-1) + y;
                pxp = img_height*(x+1) + y;
                pym = img_height*x + y-1;
                pyp = img_height*x + y+1;
                for (unsigned int z = 0 ; z < img_depth ; z++){
                    residu += m_stackCoefficients[z]*(denoised_image[p] - noisy_image[z + img_depth*p]);
                }

                tmp = denoised_image[p] - primal_step * (residu);

                dxx_adj = dyy_adj = dxy_adj = 0.;

                if ((x > 0) && (x < img_width - 1))
                    dxx_adj = dual_images0[pxm] - 2 * dual_images0[p]
                            + dual_images0[pxp];

                if ((y > 0) && (y < img_height - 1))
                    dyy_adj = dual_images1[pym] - 2 * dual_images1[p]
                            + dual_images1[pyp];

                if ((x == 0) && (y == 0))
                    dxy_adj = dual_images2[p];
                if ((x > 0) && (y == 0))
                    dxy_adj = dual_images2[p] - dual_images2[pxm];
                if ((x == 0) && (y > 0))
                    dxy_adj = dual_images2[p] - dual_images2[pym];
                if ((x > 0) && (y > 0))
                    dxy_adj = dual_images2[p] - dual_images2[pxm]
                            - dual_images2[pym]
                            + dual_images2[img_height*(x-1) + (y-1)];

                tmp -= (primal_weight * (dxx_adj + dyy_adj + sqrt2 * dxy_adj)
                        + primal_weight_comp * dual_images3[p]);
                denoised_image[p] = SMath::max(min_val, SMath::min(max_val, tmp));
            }
        }

        // Stopping criterion
        if(m_verbose){
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0){
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
            }
        }

        // Dual optimization
        for (unsigned int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * denoised_image[i] - auxiliary_image[i];
        }

        for (unsigned int x = 0 ; x < img_width ; x++)
        {
            for (unsigned int y = 0 ; y < img_height ; y++)
            {

                p = img_height*x + y;
                pxm = img_height*(x-1) + y;
                pxp = img_height*(x+1) + y;
                pym = img_height*x + y-1;
                pyp = img_height*x + y+1;

                if ((x > 0) && (x < img_width - 1)) {
                    dxx = auxiliary_image[pxp] - 2 * auxiliary_image[p]
                            + auxiliary_image[pxm];
                    dual_images0[p] += dual_weight * dxx;
                }
                if ((y > 0) && (y < img_height - 1)) {
                    dyy = auxiliary_image[pyp] - 2 * auxiliary_image[p]
                            + auxiliary_image[pym];
                    dual_images1[p] += dual_weight * dyy;
                }

                if ((x < img_width - 1) && (y < img_height - 1)) {
                    dxy = auxiliary_image[img_height*(x+1) + (y+1)] - auxiliary_image[pxp]
                            - auxiliary_image[pyp] + auxiliary_image[p];
                    dual_images2[p] += sqrt2 * dual_weight * dxy;
                }

                dual_images3[p] += dual_weight_comp * auxiliary_image[p];
            }
        }

        for (unsigned int i = 0 ; i < N ; i++){
            float tmp = SMath::max(1.,
                             1. / m_regularization
                             * sqrt(
                                 pow(dual_images0[i], 2.)
                             + pow(dual_images1[i], 2.)
                    + pow(dual_images2[i], 2.)
                    + pow(dual_images3[i], 2.)));
            dual_images0[i] /= tmp;
            dual_images1[i] /= tmp;
            dual_images2[i] /= tmp;
            dual_images3[i] /= tmp;
        }
    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    delete dual_images0;
    delete dual_images1;
    delete dual_images2;
    delete dual_images3;
    delete auxiliary_image;
    return new SImageFloat(denoised_image, img_width, img_height);
}

