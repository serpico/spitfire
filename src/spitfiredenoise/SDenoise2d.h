/// \file SDenoise2d.h
/// \brief SDenoise2d class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2020

#pragma once

#include <string>
#include <vector>

#include "simage/SImageFloat.h"
#include "spitfiredenoiseExport.h"
#include "simage/SImageFilter.h"
#include "SpitfireException.h"


/// \class SDenoise2d
/// \brief 2D image denoising using the SPARTION algorithm
class SPITFIREDENOISE_EXPORT SDenoise2d : public SImageFilter{

public:
    SDenoise2d();

public:
    void setMethod(std::string method);
    void setRegularization(float regularization);
    void setWeighting(float weighting);
    void setIterationsNumber(int iterationsNumber);
    void setVerbose(bool verbose);
    void setUseStack(bool useStack);
    void setStackCoefficients(std::vector<float> coefficients);

public:
    void setInput(SImage* image);
    SImage* getOutput();

public:
    void run();

protected:
    void runStack();
    void runOnce();
    void runSlices();

protected:
    SImageFloat* runSV(SImageFloat* image);
    SImageFloat* runHSV(SImageFloat* image);
    SImageFloat* runSVStack(SImageFloat* image);
    SImageFloat* runHSVStack(SImageFloat* image);

protected:
    float m_regularization;
    float m_weighting;
    int m_iterationsNumber;
    bool m_verbose;
    std::string m_method;

protected:
    bool m_useStack;
    std::vector<float> m_stackCoefficients;

protected:
    SImage* m_input;
    SImage* m_output;

protected:
    int m_processPrecision;
    bool m_processZ;
    bool m_processT;
    bool m_processC;
};
