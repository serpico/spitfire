/// \file svdeconvEstimate.cpp
/// \brief svdeconvEstimate class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2020

#include <iostream>
#include <fstream>

#include "SDeconvEstimate2d.h"
#include "SDeconv2d.h"

#include "score/SException.h"
#include "score/SMath.h"
#include "simage/SImageCast.h"
#include "smanipulate/SCrop.h"
#include "sfiltering/SGradient.h"

SDeconvEstimate2d::SDeconvEstimate2d() : SImageFilter(){
    m_verbose = true;
    m_method = "HSV";
    m_dimension = "2D";
    m_sigma = 0.0;
    m_input = nullptr;
    m_roi = nullptr;
}

void SDeconvEstimate2d::setInput(SImage* image){
    m_input = image;
}

void SDeconvEstimate2d::setROI(SRoi* roi){
    m_roi = roi;
}

void SDeconvEstimate2d::setMethod(std::string method){
    m_method = method;
}

void SDeconvEstimate2d::setSigma(float sigma){
    m_sigma = sigma;
}

void SDeconvEstimate2d::setRegularizationRange(float regularizationMin, float regularizationMax, float step){
    m_regularizationMin = regularizationMin;
    m_regularizationMax = regularizationMax;
    m_regularizationStep = step;

    if (m_regularizationMin > m_regularizationMax){
        throw SException("SDeconvEstimate2d: regularization min must be lower than regularization max !");
    }
    if (m_regularizationStep < 0){
        throw SException("SDeconvEstimate2d: regularization step must be positive !");
    }
}

void SDeconvEstimate2d::setWeighting(float weighting){
    m_weighting = weighting;
    if ( m_weighting < 0 || m_weighting > 1){
        throw SException("SDeconvEstimate2d: weighting must be in [0,1] !");
    }
}

void SDeconvEstimate2d::setVerbose(bool verbose){
    m_verbose = verbose;
}

void SDeconvEstimate2d::setIntensityNormalization(std::string normalization){
    m_normalization = normalization;
}

std::vector<float> SDeconvEstimate2d::getEstimationCurveVect(){
    return m_rCurve;
}

SArray* SDeconvEstimate2d::getEstimationCurve(){
    SArray* array = new SArray(m_rCurve.size());
    for (unsigned int i = 0 ; i < m_rCurve.size() ; i++){
        array->set(i, new SFloat(m_rCurve[i]));
    }
    return array;
}

float SDeconvEstimate2d::getEstimatedRegularization(){
    return m_rEstimated;
}

void SDeconvEstimate2d::checkInputs(){
    if (!m_input){
        throw SException("SDeconvEstimate2d: no input data");
    }
}

void SDeconvEstimate2d::run(){

    m_rCurve.clear();

    // crop input
    SImage* input = nullptr;
    if (m_roi){
        SCrop cropFilter;
        SImage* sliceInput = nullptr;
        if (m_input->getSizeZ() > 1){
            int z = int(float(m_input->getSizeZ())/2.0);
            sliceInput = SImageCast::toFloat(m_input)->getSlice(z);
            cropFilter.setInput(sliceInput);
        }
        else{
            cropFilter.setInput(m_input);
        }
        cropFilter.setRoi(m_roi);
        cropFilter.run();
        input = cropFilter.getOutput();

        if (sliceInput){
            delete sliceInput;
        }
    }
    else{
        int z = int(float(m_input->getSizeZ())/2.0);
        input = SImageCast::toFloat(m_input)->getSlice(z);

    }

    /*
    std::cout << "parameters : " << std::endl;
    std::cout << "sigma = " << m_sigma << std::endl;
    std::cout << "m_method = " << m_method << std::endl;
    std::cout << "m_weighting = " << m_weighting << std::endl;
    std::cout << "m_normalization = " << m_normalization << std::endl;
    std::cout << "m_verbose = " << m_verbose << std::endl;
    */

    // run calculation
    this->notifyProgress( 0 );
    for (float r = m_regularizationMin ; r <= m_regularizationMax ; r += m_regularizationStep){



        // deconvolute the image
        SDeconv2d process;
        process.setInput( input );
        process.setSigmaPSF(m_sigma);
        process.setMethod(m_method);
        process.setRegularization(r);
        process.setWeighting(m_weighting);
        process.setIterationsNumber(200);
        process.setIntensityNormalization(m_normalization);
        process.setVerbose(m_verbose);
        process.run();
        SImage* outputImage = process.getOutput();

        // calculate the gradient
        SGradient gradFiler;
        gradFiler.setInput(outputImage);
        gradFiler.run();
        float v = gradFiler.getNormL1();
        m_rCurve.push_back(v);

        delete outputImage;

        if ( r == m_regularizationMax ){
            this->notifyProgress( 100 );
        }
        else{
            this->notifyProgress( 100*(r/(m_regularizationMax-m_regularizationMin)/m_regularizationStep) );
        }
    }

    this->analyseRCurve();
}

void SDeconvEstimate2d::analyseRCurve(){
    float minVal = SMath::FMAX;
    m_rEstimated = m_regularizationMax;
    for (int i = 0 ; i < int(m_rCurve.size()) ; i++){
        if ( m_rCurve[i] < minVal ){
            minVal = m_rCurve[i];
            m_rEstimated = m_regularizationMin + i*m_regularizationStep;
        }
    }
}
