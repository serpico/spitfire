/// \file SDeconv2d.cpp
/// \brief SDeconv2d class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2020

#include <iostream>

#include "math.h"

#include "SDeconv2d.h"
#include "SGaussianPSF.h"
#include "score/SCoreShortcuts.h"
#include "score/SMath.h"
#include "smanipulate/SNormalize.h"
#include "smanipulate/SShift.h"
#include "sfft/SFFT.h"
#include "sfft/SFFTConvolutionFilter.h"
#include "../SpitfireException.h"
#include "simage/SImageMath.h"
#include "sdata/SFloat.h"

#include "fftw3.h"

#ifdef SL_USE_OPENMP
#include "omp.h"
#endif

SDeconv2d::SDeconv2d() : SImageFilter(){
    m_processPrecision = 32;
    m_processZ = true;
    m_processT = false;
    m_processC = false;

    m_verbose = true;
    m_method = "SV";
    m_useStack = false;
    m_calculateEnergy = false;
}

void SDeconv2d::setMethod(std::string method){
    m_method = method;
}

void SDeconv2d::setSigmaPSF(float sigma){
    m_sigma = sigma;
}

void SDeconv2d::setRegularization(float regularization){
    m_regularization = pow(2,-regularization);
}

void SDeconv2d::setWeighting(float weighting){
    m_weighting = weighting;
}

void SDeconv2d::setIterationsNumber(unsigned int iterationsNumber){
    m_iterationsNumber = iterationsNumber;
}

void SDeconv2d::setIntensityNormalization(std::string normalization){
    m_normalization = normalization;
}

void SDeconv2d::setVerbose(bool verbose){
    m_verbose = verbose;
}

void SDeconv2d::setUseStack(bool useStack){
    m_useStack = useStack;
}

void SDeconv2d::setStackCoefficients(std::vector<float> coefficients){
    m_stackCoefficients = coefficients;

    if (m_verbose && m_useStack){
        this->notify("Stack coefficients:");
        for (int i = 0 ; i < int(coefficients.size()) ; i++){
            this->notify(std::to_string(coefficients[i]));
        }
    }
}

void SDeconv2d::setCalculateFinalEnergy(bool calculateEnergy){
    m_calculateEnergy = calculateEnergy;
}

void SDeconv2d::run(){

    this->checkInputs();

    if (m_useStack){
        this->runStack();
    }
    else{
        if (m_input->getSizeZ() == 1){
            this->runOnce();
        }
        else{
            this->runSlices();
        }
    }
}

std::vector<float> SDeconv2d::getEnergy(){
    return m_energy;
}

SArray* SDeconv2d::getEnergyArray(){
    SArray* energyArray = new SArray(3);
    energyArray->set(0, new SFloat(m_energy[0]) );
    energyArray->set(1, new SFloat(m_energy[1]) );
    energyArray->set(2, new SFloat(m_energy[2]) );
    return energyArray;
}

void SDeconv2d::runStack(){

    if (m_verbose){
        this->notify("Run stack mode");
    }

    // Check stack coefficients
    if (m_stackCoefficients.size() == 0){
        this->notify("WARNING: Stack coefficients empty - use mean");
        m_stackCoefficients.resize(m_input->getSizeZ());
        for (unsigned int z = 0 ; z < m_input->getSizeZ() ; z++){
            m_stackCoefficients[z] = 1/float(m_input->getSizeZ());
        }
    }

    if (m_stackCoefficients.size() != m_input->getSizeZ()){
        this->notify("Error: stack coefficients size (" + std::to_string(m_stackCoefficients.size())+") different of stack size ("+std::to_string(m_input->getSizeZ()));
        return;
    }

    // calculete PSF
    SGaussianPSF psfGenerator;
    psfGenerator.setImageSize(m_input->getSizeX(), m_input->getSizeY());
    psfGenerator.setSigma(m_sigma, m_sigma);
    SImage* psf = psfGenerator.getPSF();

    // normalize inputs
    SImageFloat* inputFloat = dynamic_cast<SImageFloat*>(m_input);
    m_maxInput = inputFloat->getMax();
    SNormalize normInputFilter;
    normInputFilter.setMethod(m_normalization);
    normInputFilter.setInput(m_input);
    normInputFilter.run();
    SImage* normInput = normInputFilter.getOutput();

    SNormalize normPSFFilter;
    normPSFFilter.setMethod(SNormalize::Sum);
    normPSFFilter.setInput(psf);
    normPSFFilter.run();
    SImage* normPSF = normPSFFilter.getOutput();

    //SImageReader::write(psf, "psf_base.tif");
    //SImageReader::write(normPSF, "psf_stack.tif");


    delete psf;

//    SImage* normPSF = psf;

    // run deconv
    SImageFloat* outputImage = nullptr;
    if (m_method == "SV"){
        if (m_verbose){
            this->notify("use SV");
        }
        outputImage = this->runSV2dStack(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, m_verbose);
    }
    else if (m_method == "HSV") {
        if (m_verbose){
            this->notify("use HSV");
        }
        outputImage = this->runHSV2dStack(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, m_verbose);
    }
    else{
        this->notify("ERROR: Method must be SV or HSV");
    }

    SImageMath::multiply(outputImage, m_maxInput/ outputImage->getMax());
    m_output = outputImage;
}

void SDeconv2d::runOnce()
{
    this->notify("runOnce");
    // calculate PSF
    SGaussianPSF psfGenerator;
    psfGenerator.setImageSize(m_input->getSizeX(), m_input->getSizeY());
    psfGenerator.setSigma(m_sigma, m_sigma);
    SImage* psf = psfGenerator.getPSF();

    SImageFloat* inputFloat = dynamic_cast<SImageFloat*>(m_input);

    // normalize inputs
    m_maxInput = inputFloat->getMax();
    SNormalize normInputFilter;
    normInputFilter.setMethod(m_normalization);
    normInputFilter.setInput(m_input);
    normInputFilter.run();
    SImage* normInput = normInputFilter.getOutput();

    SNormalize normPSFFilter;
    normPSFFilter.setMethod(SNormalize::Sum);
    normPSFFilter.setInput(psf);
    normPSFFilter.run();
    SImage* normPSF = normPSFFilter.getOutput();

    delete psf;

    // run deconv
    SImageFloat* outputImage = nullptr;
    if (m_method == "SV"){
        if (m_verbose){
            this->notify("use SV");
        }
        outputImage = this->runSV2d(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, m_verbose);
        if (m_calculateEnergy){
            m_energy = this->SVEnergy(normInput, normPSF, m_output, m_weighting, m_regularization);
        }
    }
    else if (m_method == "HSV"){
        if (m_verbose){
            this->notify("use HSV");
        }
        outputImage = this->runHSV2d(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, m_verbose);
        if (m_calculateEnergy){
            m_energy = this->HSVEnergy(normInput, normPSF, m_output, m_weighting, m_regularization);
        }
    }
    else{
        this->notify("ERROR: Method must be SV or HSV");
    }

    SImageMath::multiply(outputImage, m_maxInput/ outputImage->getMax());
    m_output = outputImage;

}

void SDeconv2d::runSlices(){

    // calculate PSF
    SGaussianPSF psfGenerator;
    psfGenerator.setImageSize(m_input->getSizeX(), m_input->getSizeY());
    psfGenerator.setSigma(m_sigma, m_sigma);
    SImage* psf = psfGenerator.getPSF();

    SNormalize normPSFFilter;
    normPSFFilter.setMethod(SNormalize::Sum);
    normPSFFilter.setInput(psf);
    normPSFFilter.run();
    SImage* normPSF = normPSFFilter.getOutput();
    delete psf;

    SImageFloat* inputFloat = dynamic_cast<SImageFloat*>(m_input);
    unsigned int sx = inputFloat->getSizeX();
    unsigned int sy = inputFloat->getSizeY();
    unsigned int sz = inputFloat->getSizeZ();
    SImageFloat* outputImage = new SImageFloat(sx, sy, sz);
    float* outputImageBuffer = outputImage->getBuffer();

    for (unsigned int z = 0; z < inputFloat->getSizeZ(); ++z)
    {
        if (m_verbose){
            this->notifyProgress(100* float(z)/float(sz) );
            //this->notify("Deconv slice " + std::to_string( z+1 ) + "/" + std::to_string(inputFloat->getSizeZ()));
        }

        SImageFloat* inputSlice = inputFloat->getSlice(z);

        m_maxInput = inputSlice->getMax();
        SNormalize normInputFilter;
        normInputFilter.setMethod(m_normalization);
        normInputFilter.setInput(inputSlice);
        normInputFilter.run();
        SImage* normInput = normInputFilter.getOutput();

        SImageFloat* outputSlice = nullptr;
        if (m_method == "SV"){
            //if (m_verbose){
            //    this->notify("use SV");
            //}
            outputSlice = this->runSV2d(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, false);
        }
        else if (m_method == "HSV"){
            //if (m_verbose){
            //    this->notify("use HSV");
            //}
            outputSlice = this->runHSV2d(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, false);
        }
        else{
            throw SpitfireException("SDevonv2d: method must be SV or HSV");
        }

        if (z == unsigned(sz/2)){
            if (m_calculateEnergy && m_method == "SV"){
                m_energy = this->SVEnergy(normInput, normPSF, outputSlice, m_weighting, m_regularization);
            }
            else if (m_calculateEnergy && m_method == "HSV"){
                m_energy = this->HSVEnergy(normInput, normPSF, outputSlice, m_weighting, m_regularization);
            }
        }

        SImageMath::multiply(outputSlice, m_maxInput/ outputSlice->getMax());

        // copy slice to m_output
        float* outputSliceBuffer = outputSlice->getBuffer();
        for (unsigned int x = 0 ; x < unsigned(normInput->getSizeX()) ; x++){
            for (unsigned int y = 0 ; y < unsigned(normInput->getSizeY()) ; y++)
            {
                outputImageBuffer[ z + sz*(y + sy*x)] = outputSliceBuffer[y + sy*x];
            }
        }
    }
    m_output = outputImage;
    if (m_verbose){
        this->notifyProgress(100);
    }
}

std::vector<float> SDeconv2d::SVEnergy(SImage* blurry_image, SImage* PSF, SImage* deblured_image, float weighting, float reg){

    unsigned int sx = blurry_image->getSizeX();
    unsigned int sy = blurry_image->getSizeY();
    //unsigned int sz = blurry_image->getSizeZ();
    float grad = 0.0;
    float dx, dy;
    float Er = 0.0;
    float* deblured_image_buffer = dynamic_cast<SImageFloat*>(blurry_image)->getBuffer();
#pragma omp parallel for
    for (unsigned int x = 0 ; x < sx; x++){
        for (unsigned int y = 0 ; y < sy; y++){
            dx = dy = 0.;

            if (x > 0)
                dx = deblured_image_buffer[sy*(x-1)+y] - deblured_image_buffer[sy*x+y];
            else
                dx = deblured_image_buffer[sy*x+y];
            if (y > 0)
                dy = deblured_image_buffer[sy*x+(y-1)] - deblured_image_buffer[sy*x+y];
            else
                dy = deblured_image_buffer[sy*x+y];

            grad = dx*dx + dy*dy;
            Er += sqrt(pow(weighting,2)*grad + pow(1-weighting,2)*pow(deblured_image_buffer[sy*x+y],2));
        }
    }

    // Calculate Ed
    SFFTConvolutionFilter convFilter;
    convFilter.setImage1(deblured_image);
    convFilter.setImage2(PSF);
    convFilter.run();
    SImageFloat* conv_deblured = dynamic_cast<SImageFloat*>(convFilter.getOutput());

    float Ed = 0;
    float* blurry_image_buffer = dynamic_cast<SImageFloat*>(blurry_image)->getBuffer();
    float* conv_deblured_buffer = dynamic_cast<SImageFloat*>(conv_deblured)->getBuffer();
#pragma omp parallel for
    for (unsigned int i = 0 ; i < sx*sy ; i++){
        Ed += pow(blurry_image_buffer[i] - conv_deblured_buffer[i],2);
    }

    delete conv_deblured;

    this->notify("HSVEnergy: Er=" + SImg::float2string(Er));
    this->notify("HSVEnergy: Ed=" + SImg::float2string(Ed));
    this->notify("HSVEnergy: E=" + SImg::float2string(Ed + reg*Er));
    std::vector<float> out;
    out.resize(3);
    out[0] = Ed + reg*Er;
    out[1] = Ed;
    out[2] = Er;
    return out;
}

std::vector<float> SDeconv2d::HSVEnergy(SImage* blurry_image, SImage* PSF, SImage* deblured_image, float weighting, float reg)
{
    unsigned int sx = blurry_image->getSizeX();
    unsigned int sy = blurry_image->getSizeY();
    float hessian = 0.0;
    float dxx, dyy, dxy;
    float Er = 0.0;
    float* deblured_image_buffer = dynamic_cast<SImageFloat*>(deblured_image)->getBuffer();
    for (unsigned int x = 0 ; x < sx; x++){
        for (unsigned int y = 0 ; y < sy; y++){
            dxx = dyy = dxy = 0.;
            if ((x > 0) && (x < sx - 1))
                dxx = deblured_image_buffer[sy*(x-1)+y] - 2 * deblured_image_buffer[sy*x+y] + deblured_image_buffer[sy*(x+1)+y];

            if ((y > 0) && (y < sy - 1))
                dyy = deblured_image_buffer[sy*x+(y-1)] - 2 * deblured_image_buffer[sy*x+y] + deblured_image_buffer[sy*x+(y+1)];

            if ((x == 0) && (y == 0))
                dxy = deblured_image_buffer[sy*x+y];
            if ((x > 0) && (y == 0))
                dxy = deblured_image_buffer[sy*x+y] - deblured_image_buffer[sy*(x-1)+y];
            if ((x == 0) && (y > 0))
                dxy = deblured_image_buffer[sy*x+y] - deblured_image_buffer[sy*x+(y-1)];
            if ((x > 0) && (y > 0))
                dxy = deblured_image_buffer[sy*x+y] - deblured_image_buffer[sy*(x-1)+y] - deblured_image_buffer[sy*x+(y-1)] + deblured_image_buffer[sy*(x-1)+(y-1)];

            hessian = dxx*dxx + dyy*dyy + dxy*dxy;
            Er += sqrt(pow(weighting,2)*hessian + pow(1-weighting,2)*pow(deblured_image_buffer[sy*x+y],2));
        }
    }

    // Calculate Ed
    SFFTConvolutionFilter convFilter;
    convFilter.setImage1(deblured_image);
    convFilter.setImage2(PSF);
    convFilter.run();
    SImageFloat* conv_deblured = dynamic_cast<SImageFloat*>(convFilter.getOutput());

    float Ed = 0;
    float* blurry_image_buffer = dynamic_cast<SImageFloat*>(blurry_image)->getBuffer();
    float* conv_deblured_buffer = dynamic_cast<SImageFloat*>(conv_deblured)->getBuffer();

#pragma omp parallel for
    for (unsigned int i = 0 ; i < sx*sy ; i++){
        Ed += pow(blurry_image_buffer[i] - conv_deblured_buffer[i],2);
    }

    delete conv_deblured;

    //this->notify("HSVEnergy: Er=" + SImg::float2string(Er));
    //this->notify("HSVEnergy: Ed=" + SImg::float2string(Ed));
    //this->notify("HSVEnergy: E=" + SImg::float2string(Ed + reg*Er));
    std::vector<float> out;
    out.resize(3);
    out[0] = Ed + reg*Er;
    out[1] = Ed;
    out[2] = Er;
    return out;

}

SImageFloat* SDeconv2d::runSV2d(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose){

    int img_width = image->getSizeX();
    int img_height = image->getSizeY();
    int N = img_width*img_height;
    int Nfft = img_width*(img_height/2+1);

#ifdef SL_USE_OPENMP
    omp_set_num_threads(omp_get_max_threads());
    int fftThreads = fftwf_init_threads();
    if (fftThreads == 0){
        this->notify("Cannot initialize parallel fft: error ");
    }
#endif

    // Optical transfer function and its adjoint
    SShift shiftOTFFilter;
    shiftOTFFilter.setInput(psf);
    shiftOTFFilter.setShift(int(- float(img_width) / 2.0), int(-float(img_height) / 2.0));
    shiftOTFFilter.run();
    SImageFloat* OTFCImg = dynamic_cast<SImageFloat*>(shiftOTFFilter.getOutput());

    SImageFloat* adjoint_PSF = new SImageFloat(img_width, img_height);
    float* psfBuffer = psf->getBuffer();
    float* adjoint_PSFBuffer = adjoint_PSF->getBuffer();
#pragma omp parallel for
    for (int x = 0 ; x < img_width ; x++){
        for (int y = 0 ; y < img_height ; y++){
            adjoint_PSFBuffer[y + img_height*x] = psfBuffer[(img_height - 1 - y) + img_height*(img_width - 1 - x)];
        }
    }

    SShift shiftAdjPSFFilter;
    shiftAdjPSFFilter.setInput(adjoint_PSF);
    shiftAdjPSFFilter.setShift(-(int(img_width) - 1) % 2, -(int(img_height) - 1) % 2);
    shiftAdjPSFFilter.run();
    SImageFloat* adjoint_PSF_shift = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter.getOutput());

    SShift shiftAdjPSFFilter2;
    shiftAdjPSFFilter2.setInput(adjoint_PSF_shift);
    shiftAdjPSFFilter2.setShift(int(-float(img_width) / 2.0), int(-float(img_height) / 2.0));
    shiftAdjPSFFilter2.run();
    SImageFloat* adjoint_OTFCimg = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter2.getOutput());
    delete adjoint_PSF_shift;

    // Splitting parameters
    float dual_step = SMath::max(0.01, SMath::min(0.1, m_regularization));
    float primal_step = 0.99
            / (0.5
               + (8 * pow(m_weighting, 2.)
                  + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);

    // Initializations
    float* blurry_array = image->getBuffer(); //float*) malloc(sizeof(float) * N);
    float* deblurred_image = (float*) malloc(sizeof(float) * N);
    float* dual_image0 = (float*) malloc(sizeof(float) * N);
    float* dual_image1 = (float*) malloc(sizeof(float) * N);
    float* dual_image2 = (float*) malloc(sizeof(float) * N);
    float* dual_image3 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* OTFReal = OTFCImg->getBuffer();// (float*) malloc(sizeof(float) * unsigned(N));
    float* adjoint_OTFReal = adjoint_OTFCimg->getBuffer(); //float*) malloc(sizeof(float) * unsigned(N));
    float* residue_image = (float*) malloc(sizeof(float) * unsigned(N));

#pragma omp parallel for
    for (int i = 0 ; i < N ; i++){
        dual_image0[i] = 0.0;
        dual_image1[i] = 0.0;
        dual_image2[i] = 0.0;
        dual_image3[i] = 0.0;
    }

    // init deblurred_image as input image
#pragma omp parallel for
    for (int i = 0 ; i < N ; i++){
        deblurred_image[i] = blurry_array[i];
    }

    fftwf_complex* blurry_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* deblurred_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* residue_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* adjoint_OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));

    SFFT::fft2D(blurry_array, blurry_image_FT, img_width, img_height);
    SFFT::fft2D(OTFReal, OTF, img_width, img_height);
    SFFT::fft2D(adjoint_OTFReal, adjoint_OTF, img_width, img_height);

    //free(blurry_array);
    delete OTFCImg;
    delete adjoint_OTFCimg;

#pragma omp parallel for
    for (int i = 0 ; i < Nfft ; i++){
        deblurred_image_FT[i][0] = blurry_image_FT[i][0];
        deblurred_image_FT[i][1] = blurry_image_FT[i][1];
        residue_image_FT[i][0] = blurry_image_FT[i][0];
        residue_image_FT[i][1] = blurry_image_FT[i][1];
    }

    // Deconvolution process
    float tmp, dx, dy, dx_adj, dy_adj;
    float real_tmp, imag_tmp;
    float min_val = 0.0;
    float max_val = 1.0;
    int pxm, pym, pxp, pyp;
    int p;

    for (unsigned int iter = 0; iter < m_iterationsNumber; iter++) {
        // Primal optimization
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = deblurred_image[i];
        }
        SFFT::fft2D(deblurred_image, deblurred_image_FT, img_width, img_height);

#pragma omp parallel for
        for (int i = 0 ; i < Nfft ; i++){
            residue_image_FT[i][0] = deblurred_image_FT[i][0];
            residue_image_FT[i][1] = deblurred_image_FT[i][1];
        }

        // Data term
#pragma omp parallel for
        for (int i = 0 ; i < Nfft ; i++){
            real_tmp = OTF[i][0] * deblurred_image_FT[i][0]
                    - OTF[i][1] * deblurred_image_FT[i][1]
                    - blurry_image_FT[i][0];
            imag_tmp = OTF[i][0] * deblurred_image_FT[i][1]
                    + OTF[i][1] * deblurred_image_FT[i][0]
                    - blurry_image_FT[i][1];

            residue_image_FT[i][0] = adjoint_OTF[i][0] * real_tmp
                    - adjoint_OTF[i][1] * imag_tmp;
            residue_image_FT[i][1] = adjoint_OTF[i][0] * imag_tmp
                    + adjoint_OTF[i][1] * real_tmp;

        }

        SFFT::ifft2D(residue_image_FT, residue_image, img_width, img_height);

#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){

                p = y + img_height * x;
                tmp = deblurred_image[p] - primal_step * residue_image[p]/float(N);

                pxm = y + img_height * (x-1);
                if (x > 0){
                    dx_adj = dual_image0[pxm] - dual_image0[p];
                }
                else{
                    if (y > 0){
                        dx_adj = 0;
                    }
                    //dx_adj = dual_image0[p];
                }

                pym = (y-1) + img_height * (x);
                if (y > 0){
                    dy_adj = dual_image1[pym] - dual_image1[p];
                }
                else{
                    if (x > 0){
                        dy_adj = 0.0;        
                    }
                    //dy_adj = dual_image1[p];
                }
                if (x == 0 && y == 0){
				    dy_adj = 0.0;
			    }


                tmp -= (primal_weight * (dx_adj + dy_adj)
                        + primal_weight_comp * dual_image2[p]);

                deblurred_image[p] = SMath::max(min_val, SMath::min(max_val, tmp));
            }
        }

        // Stopping criterion
        if(verbose){
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0){
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
                //this->notify("Iteration #" + std::to_string(iter));
            }
        }

        // Dual optimization
#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * deblurred_image[i] - auxiliary_image[i];
        }

#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){

                p = y + img_height * x;

                if (x < img_width - 1) {
                    pxp = y + img_height * (x+1);
                    dx = auxiliary_image[pxp] - auxiliary_image[p];
                    dual_image0[p] += dual_weight * dx;
                }

                if (y < img_height - 1) {
                    pyp = (y+1) + img_height * (x);
                    dy = auxiliary_image[pyp] - auxiliary_image[p];
                    dual_image1[p] += dual_weight * dy;
                }

                dual_image2[p] += dual_weight_comp * auxiliary_image[p];
            }
        }

#pragma omp parallel for
        for(int p = 0 ; p < N ; p++){
            float tmp = SMath::max(1.,
                                   1. / m_regularization
                                   * sqrt(
                                       pow(dual_image0[p], 2.)
                                       + pow(dual_image1[p], 2.)
                                       + pow(dual_image2[p], 2.)));
            dual_image0[p] /= tmp;
            dual_image1[p] /= tmp;
            dual_image2[p] /= tmp;
        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    // copy output
    free(dual_image0);
    free(dual_image1);
    free(dual_image2);
    free(dual_image3);
    free(auxiliary_image);
    free(residue_image);

    fftwf_free(blurry_image_FT);
    fftwf_free(deblurred_image_FT);
    fftwf_free(residue_image_FT);
    fftwf_free(OTF);
    fftwf_free(adjoint_OTF);

    SImageFloat* outputImage = new SImageFloat(deblurred_image, img_width, img_height, 1);
    if (verbose){
        this->notifyProgress(100);
    }
    return outputImage;
}

SImageFloat* SDeconv2d::runHSV2d(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose){

    int img_width = image->getSizeX();
    int img_height = image->getSizeY();
    int N = img_width*img_height;
    int Nfft = img_width*(img_height/2+1);

    float sqrt2 = sqrt(2.);

#ifdef SL_USE_OPENMP
    omp_set_num_threads(omp_get_max_threads());
    int fftThreads = fftwf_init_threads();
    if (fftThreads == 0){
        std::cout << "Cannot initialize parrallel fft: error " << fftThreads << std::endl;
    }
#endif

    // Optical transfer function and its adjoint
    SShift shiftOTFFilter;
    shiftOTFFilter.setInput(psf);
    shiftOTFFilter.setShift(int(- float(img_width) / 2.0), int(-float(img_height) / 2.0));
    shiftOTFFilter.run();
    SImageFloat* OTFCImg = dynamic_cast<SImageFloat*>(shiftOTFFilter.getOutput());

    SImageFloat* adjoint_PSF = new SImageFloat(img_width, img_height);
    float* psfBuffer = psf->getBuffer();
    float* adjoint_PSFBuffer = adjoint_PSF->getBuffer();
    for (int x = 0 ; x < img_width ; x++){
        for (int y = 0 ; y < img_height ; y++){
            adjoint_PSFBuffer[y + img_height*x] = psfBuffer[(img_height - 1 - y) + img_height*(img_width - 1 - x)];
        }
    }

    SShift shiftAdjPSFFilter;
    shiftAdjPSFFilter.setInput(adjoint_PSF);
    shiftAdjPSFFilter.setShift(-(int(img_width) - 1) % 2, -(int(img_height) - 1) % 2);
    shiftAdjPSFFilter.run();
    SImageFloat* adjoint_PSF_shift = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter.getOutput());

    SShift shiftAdjPSFFilter2;
    shiftAdjPSFFilter2.setInput(adjoint_PSF_shift);
    shiftAdjPSFFilter2.setShift(int(-float(img_width) / 2.0), int(-float(img_height) / 2.0));
    shiftAdjPSFFilter2.run();
    SImageFloat* adjoint_OTFCimg = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter2.getOutput());
    delete adjoint_PSF_shift;

    // Splitting parameters
    float dual_step = SMath::max(0.001, SMath::min(0.01, m_regularization));
    float primal_step = 0.99
            / (0.5
               + (64 * pow(m_weighting, 2.)
                  + pow(1. - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);

    // Initializations
    // copy inputs to buffer
    float* blurry_array = image->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* OTFReal = OTFCImg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* adjoint_OTFReal = adjoint_OTFCimg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* deblurred_image = (float*) malloc(sizeof(float) * N);

    int p = 0;

    float* dual_image0 = (float*) malloc(sizeof(float) * N);
    float* dual_image1 = (float*) malloc(sizeof(float) * N);
    float* dual_image2 = (float*) malloc(sizeof(float) * N);
    float* dual_image3 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* residue_image = (float*) malloc(sizeof(float) * N);

//#pragma omp parallel for
    for (int i = 0 ; i < N ; i++){
        dual_image0[i] = 0.0;
        dual_image1[i] = 0.0;
        dual_image2[i] = 0.0;
        dual_image3[i] = 0.0;
        deblurred_image[i] = blurry_array[i];
    }

    fftwf_complex* blurry_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* deblurred_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* residue_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* adjoint_OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));

    SFFT::fft2D(blurry_array, blurry_image_FT, img_width, img_height);
    SFFT::fft2D(OTFReal, OTF, img_width, img_height);
    SFFT::fft2D(adjoint_OTFReal, adjoint_OTF, img_width, img_height);

    //free(blurry_array);
    free(OTFReal);
    free(adjoint_OTFReal);

//#pragma omp parallel for
    for (int i = 0 ; i < Nfft ; i++){
        deblurred_image_FT[i][0] = blurry_image_FT[i][0];
        deblurred_image_FT[i][1] = blurry_image_FT[i][1];
        residue_image_FT[i][0] = blurry_image_FT[i][0];
        residue_image_FT[i][1] = blurry_image_FT[i][1];
    }

    // Deconvolution process
    float tmp, dxx, dyy, dxy, min_val, max_val, dxx_adj, dyy_adj, dxy_adj;
    min_val = 0.0;
    max_val = 1.0;
    float real_tmp, imag_tmp;
    int pxp, pyp, pxm, pym, pxym, pxyp;


    for (unsigned int iter = 0; iter < m_iterationsNumber; iter++) {

        // Primal optimization
//#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = deblurred_image[i];
        }

        SFFT::fft2D(deblurred_image, deblurred_image_FT, img_width, img_height);

//#pragma omp parallel for
        for (int i = 0 ; i < Nfft ; i++){
            residue_image_FT[i][0] = deblurred_image_FT[i][0];
            residue_image_FT[i][1] = deblurred_image_FT[i][1];
        }

        // Data term
//#pragma omp parallel for
        for (int i = 0 ; i < Nfft ; i++){
            real_tmp = OTF[i][0] * deblurred_image_FT[i][0]
                    - OTF[i][1] * deblurred_image_FT[i][1]
                    - blurry_image_FT[i][0];
            imag_tmp = OTF[i][0] * deblurred_image_FT[i][1]
                    + OTF[i][1] * deblurred_image_FT[i][0]
                    - blurry_image_FT[i][1];

            residue_image_FT[i][0] = adjoint_OTF[i][0] * real_tmp
                    - adjoint_OTF[i][1] * imag_tmp;
            residue_image_FT[i][1] = adjoint_OTF[i][0] * imag_tmp
                    + adjoint_OTF[i][1] * real_tmp;

        }

        SFFT::ifft2D(residue_image_FT, residue_image, img_width, img_height);

        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){

                p = y + (img_height)* x;
                tmp = deblurred_image[p] - (primal_step * residue_image[p]/float(N));

                pxm = y + (img_height)* (x-1);
                pxp = y + (img_height)* (x+1);
                if ((x > 0) && (x < img_width - 1)){
                    dxx_adj = dual_image0[pxm] - 2 * dual_image0[p] + dual_image0[pxp];
                }
                else{
                    dxx_adj = 0.0;
                }

                pym = (y-1) + (img_height)* x;
                pyp = (y+1) + (img_height)* x;
                if ((y > 0) && (y < img_height - 1)){
                    dyy_adj = dual_image1[pym] - 2 * dual_image1[p] + dual_image1[pyp];
                }
                else{
                    dyy_adj = 0;
                }

                if ((x == 0) && (y == 0)){
                    dxy_adj = 0.0; //dual_image2[p];
                }
                if ((x > 0) && (y == 0)){
                    dxy_adj = 0.0; //dual_image2[p] - dual_image2[pxm];
                }
                if ((x == 0) && (y > 0)){
                    dxy_adj = 0.0; // dual_image2[p] - dual_image2[pym];
                }

                pxym = (y-1) + (img_height)* (x-1);
                if ((x > 0) && (y > 0)){
                    dxy_adj = dual_image2[p] - dual_image2[pxm]
                            - dual_image2[pym] + dual_image2[pxym];
                }
                else{
                    dxy_adj = 0.0;
                }

                tmp -= (primal_weight * (dxx_adj + dyy_adj + sqrt2 * dxy_adj)
                        + primal_weight_comp * dual_image3[p]);

                deblurred_image[p] = SMath::max(min_val, SMath::min(max_val, tmp));

            }
        }

        // Stopping criterion
        if (verbose){
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0){
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
            }
        }

        // Dual optimization
//#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * deblurred_image[i] - auxiliary_image[i];
        }

//#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){

                p = y + img_height * x;

                pxp = y + img_height * (x+1);
                pxm = y + img_height * (x-1);
                if ((x > 0) && (x < img_width - 1)) {
                    dxx = auxiliary_image[pxp] - 2 * auxiliary_image[p]
                            + auxiliary_image[pxm];
                    dual_image0[p] += dual_weight * dxx;
                }

                pyp = (y+1) + img_height * x;
                pym = (y-1) + img_height * x;
                if ((y > 0) && (y < img_height - 1)) {
                    dyy = auxiliary_image[pyp] - 2 * auxiliary_image[p]
                            + auxiliary_image[pym];
                    dual_image1[p] += dual_weight * dyy;
                }

                pxyp = (y+1) + img_height * (x+1);
                if ((x < img_width - 1) && (y < img_height - 1)) {
                    dxy = auxiliary_image[pxyp] - auxiliary_image[pxp]
                            - auxiliary_image[pyp] + auxiliary_image[p];
                    dual_image2[p] += sqrt2 * dual_weight * dxy;
                }
                dual_image3[p] += dual_weight_comp * auxiliary_image[p];
            }
        }

//#pragma omp parallel for
        for (int p = 0 ; p < N ; p++){
            float tmp = SMath::max(1.,
                                   1. / m_regularization
                                   * sqrt(
                                       pow(dual_image0[p], 2.)
                                       + pow(dual_image1[p], 2.)
                                       + pow(dual_image2[p], 2.)
                                       + pow(dual_image3[p], 2.)));
            dual_image0[p] /= tmp;
            dual_image1[p] /= tmp;
            dual_image2[p] /= tmp;
            dual_image3[p] /= tmp;
        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    // copy output
    free(dual_image0);
    free(dual_image1);
    free(dual_image2);
    free(dual_image3);
    free(auxiliary_image);
    free(residue_image);

    fftwf_free(blurry_image_FT);
    fftwf_free(deblurred_image_FT);
    fftwf_free(residue_image_FT);


    SImageFloat* outputImage = new SImageFloat(deblurred_image, img_width, img_height, 1);
    if (verbose){
        this->notifyProgress(100);
    }
    return outputImage;

}

SImageFloat* SDeconv2d::runSV2dStack(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose){

    int img_width = image->getSizeX();
    int img_height = image->getSizeY();
    int img_depth = image->getSizeZ();
    int N = img_width*img_height;
    int Nfft = img_width*(img_height/2+1);

#ifdef SL_USE_OPENMP
    omp_set_num_threads(omp_get_max_threads());
    int fftThreads = fftwf_init_threads();
    if (fftThreads == 0){
        this->notify("Cannot initialize parallel fft: error ");
    }
#endif

    // Optical transfer function and its adjoint
    SShift shiftOTFFilter;
    shiftOTFFilter.setInput(psf);
    shiftOTFFilter.setShift(int(- float(img_width) / 2.0), int(-float(img_height) / 2.0));
    shiftOTFFilter.run();
    SImageFloat* OTFCImg = dynamic_cast<SImageFloat*>(shiftOTFFilter.getOutput());

    SImageFloat* adjoint_PSF = new SImageFloat(img_width, img_height);
    float* psfBuffer = psf->getBuffer();
    float* adjoint_PSFBuffer = adjoint_PSF->getBuffer();
#pragma omp parallel for
    for (int x = 0 ; x < img_width ; x++){
        for (int y = 0 ; y < img_height ; y++){
            adjoint_PSFBuffer[y + img_height*x] = psfBuffer[(img_height - 1 - y) + img_height*(img_width - 1 - x)];
        }
    }

    SShift shiftAdjPSFFilter;
    shiftAdjPSFFilter.setInput(adjoint_PSF);
    shiftAdjPSFFilter.setShift(-(int(img_width) - 1) % 2, -(int(img_height) - 1) % 2);
    shiftAdjPSFFilter.run();
    SImageFloat* adjoint_PSF_shift = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter.getOutput());

    SShift shiftAdjPSFFilter2;
    shiftAdjPSFFilter2.setInput(adjoint_PSF_shift);
    shiftAdjPSFFilter2.setShift(int(-float(img_width) / 2.0), int(-float(img_height) / 2.0));
    shiftAdjPSFFilter2.run();
    SImageFloat* adjoint_OTFCimg = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter2.getOutput());
    delete adjoint_PSF_shift;

    // Splitting parameters
    float dual_step = SMath::max(0.01, SMath::min(0.1, m_regularization));
    float primal_step = 0.99
            / (0.5
               + (8 * pow(m_weighting, 2.)
                  + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);

    // Initializations
    float* blurry_array = image->getBuffer(); //float*) malloc(sizeof(float) * N);
    float* deblurred_image = (float*) malloc(sizeof(float) * N);
    float* dual_image0 = (float*) malloc(sizeof(float) * N);
    float* dual_image1 = (float*) malloc(sizeof(float) * N);
    float* dual_image2 = (float*) malloc(sizeof(float) * N);
    float* dual_image3 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* OTFReal = OTFCImg->getBuffer();// (float*) malloc(sizeof(float) * unsigned(N));
    float* adjoint_OTFReal = adjoint_OTFCimg->getBuffer(); //float*) malloc(sizeof(float) * unsigned(N));
    float* residue_image = (float*) malloc(sizeof(float) * unsigned(N));

#pragma omp parallel for
    for (int i = 0 ; i < N ; i++){
        dual_image0[i] = 0.0;
        dual_image1[i] = 0.0;
        dual_image2[i] = 0.0;
        dual_image3[i] = 0.0;
        deblurred_image[i] = 0.0;
    }

    // init deblurred_image as input image //  z + m_sz*(y + m_sy*x)
    for (int z = 0 ; z < img_depth ; z++){
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                deblurred_image[y + img_height*x] += m_stackCoefficients[z]*blurry_array[ z + img_depth*(y + img_height*x)];
            }
        }
    }

    std::vector<fftwf_complex*> blurry_images_FT;
    for (int z = 0 ; z < img_depth ; z++){
        blurry_images_FT.push_back((fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft)));
    }
    fftwf_complex* deblurred_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* residue_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* adjoint_OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));

    for (int z = 0 ; z < img_depth ; z++){
        SFFT::fft2D(image->getSlice(z)->getBuffer(), blurry_images_FT[z], img_width, img_height);
    }

    SFFT::fft2D(OTFReal, OTF, img_width, img_height);
    SFFT::fft2D(adjoint_OTFReal, adjoint_OTF, img_width, img_height);
    SFFT::fft2D(deblurred_image, deblurred_image_FT, img_width, img_height);

    free(blurry_array);
    delete OTFCImg;
    delete adjoint_OTFCimg;

#pragma omp parallel for
    for (int i = 0 ; i < Nfft ; i++){
        residue_image_FT[i][0] = 0.0; //blurry_images_FT[0][i][0];
        residue_image_FT[i][1] = 0.0; //blurry_images_FT[0][i][1];
    }

    // Deconvolution process
    float tmp, dx, dy, dx_adj, dy_adj;
    float real_tmp, imag_tmp, real_residu, imag_residu;
    float min_val = 0.0;
    float max_val = 1.0;
    int pxm, pym, pxp, pyp;
    int p;

    for (unsigned int iter = 0; iter < m_iterationsNumber; iter++) {
        // Primal optimization
#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = deblurred_image[i];
        }
        SFFT::fft2D(deblurred_image, deblurred_image_FT, img_width, img_height);

#pragma omp parallel for
        for (int i = 0 ; i < Nfft ; i++){
            residue_image_FT[i][0] = 0.0; //deblurred_image_FT[i][0];
            residue_image_FT[i][1] = 0.0; //deblurred_image_FT[i][1];
        }

        // Data term
        for (int z = 0 ; z < img_depth ; z++){
#pragma omp parallel for
            for (int i = 0 ; i < Nfft ; i++){
                real_tmp = OTF[i][0] * deblurred_image_FT[i][0]
                        - OTF[i][1] * deblurred_image_FT[i][1]
                        - blurry_images_FT[z][i][0];
                imag_tmp = OTF[i][0] * deblurred_image_FT[i][1]
                        + OTF[i][1] * deblurred_image_FT[i][0]
                        - blurry_images_FT[z][i][1];

                real_residu = adjoint_OTF[i][0] * real_tmp
                        - adjoint_OTF[i][1] * imag_tmp;
                imag_residu = adjoint_OTF[i][0] * imag_tmp
                        + adjoint_OTF[i][1] * real_tmp;

                residue_image_FT[i][0] += m_stackCoefficients[z]*real_residu;
                residue_image_FT[i][1] += m_stackCoefficients[z]*imag_residu;
            }
        }

        SFFT::ifft2D(residue_image_FT, residue_image, img_width, img_height);

#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){

                p = y + img_height * x;
                tmp = deblurred_image[p] - primal_step * residue_image[p]/float(N);

                pxm = y + img_height * (x-1);
                if (x > 0){
                    dx_adj = dual_image0[pxm] - dual_image0[p];
                }
                else{
                    dx_adj = dual_image0[p];
                }

                pym = (y-1) + img_height * (x);
                if (y > 0){
                    dy_adj = dual_image1[pym] - dual_image1[p];
                }
                else{
                    dy_adj = dual_image1[p];
                }

                tmp -= (primal_weight * (dx_adj + dy_adj)
                        + primal_weight_comp * dual_image2[p]);

                deblurred_image[p] = SMath::max(min_val, SMath::min(max_val, tmp));
            }
        }

        // Stopping criterion
        if(verbose){
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0){
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
                //this->notify("Iteration #" + std::to_string(iter));
            }
        }

        // Dual optimization
#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * deblurred_image[i] - auxiliary_image[i];
        }

#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){

                p = y + img_height * x;

                if (x < img_width - 1) {
                    pxp = y + img_height * (x+1);
                    dx = auxiliary_image[pxp] - auxiliary_image[p];
                    dual_image0[p] += dual_weight * dx;
                }

                if (y < img_height - 1) {
                    pyp = (y+1) + img_height * (x);
                    dy = auxiliary_image[pyp] - auxiliary_image[p];
                    dual_image1[p] += dual_weight * dy;
                }

                dual_image2[p] += dual_weight_comp * auxiliary_image[p];
            }
        }

#pragma omp parallel for
        for(int p = 0 ; p < N ; p++){
            float tmp = SMath::max(1.,
                                   1. / m_regularization
                                   * sqrt(
                                       pow(dual_image0[p], 2.)
                                       + pow(dual_image1[p], 2.)
                                       + pow(dual_image2[p], 2.)));
            dual_image0[p] /= tmp;
            dual_image1[p] /= tmp;
            dual_image2[p] /= tmp;
        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    // copy output
    free(dual_image0);
    free(dual_image1);
    free(dual_image2);
    free(dual_image3);
    free(auxiliary_image);
    free(residue_image);

    for (int z = 0 ; z < img_depth ; z++){
        fftwf_free(blurry_images_FT[z]);
    }
    fftwf_free(deblurred_image_FT);
    fftwf_free(residue_image_FT);
    fftwf_free(OTF);
    fftwf_free(adjoint_OTF);

    SImageFloat* outputImage = new SImageFloat(deblurred_image, img_width, img_height, 1);
    if (verbose){
        this->notifyProgress(100);
    }
    return outputImage;

}

SImageFloat* SDeconv2d::runHSV2dStack(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose){

    int img_width = image->getSizeX();
    int img_height = image->getSizeY();
    int img_depth = image->getSizeZ();
    int N = img_width*img_height;
    int Nfft = img_width*(img_height/2+1);

    float sqrt2 = sqrt(2.);

#ifdef SL_USE_OPENMP
    omp_set_num_threads(omp_get_max_threads());
    int fftThreads = fftwf_init_threads();
    if (fftThreads == 0){
        this->notify("Cannot initialize parallel fft: error ");
    }
#endif

    // Optical transfer function and its adjoint
    SShift shiftOTFFilter;
    shiftOTFFilter.setInput(psf);
    shiftOTFFilter.setShift(int(- float(img_width) / 2.0), int(-float(img_height) / 2.0));
    shiftOTFFilter.run();
    SImageFloat* OTFCImg = dynamic_cast<SImageFloat*>(shiftOTFFilter.getOutput());

    SImageFloat* adjoint_PSF = new SImageFloat(img_width, img_height);
    float* psfBuffer = psf->getBuffer();
    float* adjoint_PSFBuffer = adjoint_PSF->getBuffer();
#pragma omp parallel for
    for (int x = 0 ; x < img_width ; x++){
        for (int y = 0 ; y < img_height ; y++){
            adjoint_PSFBuffer[y + img_height*x] = psfBuffer[(img_height - 1 - y) + img_height*(img_width - 1 - x)];
        }
    }

    SShift shiftAdjPSFFilter;
    shiftAdjPSFFilter.setInput(adjoint_PSF);
    shiftAdjPSFFilter.setShift(-(int(img_width) - 1) % 2, -(int(img_height) - 1) % 2);
    shiftAdjPSFFilter.run();
    SImageFloat* adjoint_PSF_shift = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter.getOutput());

    SShift shiftAdjPSFFilter2;
    shiftAdjPSFFilter2.setInput(adjoint_PSF_shift);
    shiftAdjPSFFilter2.setShift(int(-float(img_width) / 2.0), int(-float(img_height) / 2.0));
    shiftAdjPSFFilter2.run();
    SImageFloat* adjoint_OTFCimg = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter2.getOutput());
    delete adjoint_PSF_shift;

    // Splitting parameters
    float dual_step = SMath::max(0.01, SMath::min(0.1, m_regularization));
    float primal_step = 0.99
            / (0.5
               + (8 * pow(m_weighting, 2.)
                  + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);

    // Initializations
    float* blurry_array = image->getBuffer(); //float*) malloc(sizeof(float) * N);
    float* deblurred_image = (float*) malloc(sizeof(float) * N);
    float* dual_image0 = (float*) malloc(sizeof(float) * N);
    float* dual_image1 = (float*) malloc(sizeof(float) * N);
    float* dual_image2 = (float*) malloc(sizeof(float) * N);
    float* dual_image3 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* OTFReal = OTFCImg->getBuffer();// (float*) malloc(sizeof(float) * unsigned(N));
    float* adjoint_OTFReal = adjoint_OTFCimg->getBuffer(); //float*) malloc(sizeof(float) * unsigned(N));
    float* residue_image = (float*) malloc(sizeof(float) * unsigned(N));

#pragma omp parallel for
    for (int i = 0 ; i < N ; i++){
        dual_image0[i] = 0.0;
        dual_image1[i] = 0.0;
        dual_image2[i] = 0.0;
        dual_image3[i] = 0.0;
        deblurred_image[i] = 0.0;
    }

    // init deblurred_image as input image //  z + m_sz*(y + m_sy*x)
    for (int z = 0 ; z < img_depth ; z++){
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                deblurred_image[y + img_height*x] += m_stackCoefficients[z]*blurry_array[ z + img_depth*(y + img_height*x)];
            }
        }
    }

    std::vector<fftwf_complex*> blurry_images_FT;
    for (int z = 0 ; z < img_depth ; z++){
        blurry_images_FT.push_back((fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft)));
    }
    fftwf_complex* deblurred_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* residue_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* adjoint_OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));

    for (int z = 0 ; z < img_depth ; z++){
        SFFT::fft2D(image->getSlice(z)->getBuffer(), blurry_images_FT[z], img_width, img_height);
    }

    SFFT::fft2D(OTFReal, OTF, img_width, img_height);
    SFFT::fft2D(adjoint_OTFReal, adjoint_OTF, img_width, img_height);
    SFFT::fft2D(deblurred_image, deblurred_image_FT, img_width, img_height);

    free(blurry_array);
    delete OTFCImg;
    delete adjoint_OTFCimg;

#pragma omp parallel for
    for (int i = 0 ; i < Nfft ; i++){
        residue_image_FT[i][0] = 0.0; //blurry_images_FT[0][i][0];
        residue_image_FT[i][1] = 0.0; //blurry_images_FT[0][i][1];
    }

    // Deconvolution process
    float tmp, dxx, dyy, dxy, min_val, max_val, dxx_adj, dyy_adj, dxy_adj;
    min_val = 0.0;
    max_val = 1.0;
    float real_tmp, imag_tmp, real_residu, imag_residu;
    int p, pxp, pyp, pxm, pym, pxym, pxyp;


    for (unsigned int iter = 0; iter < m_iterationsNumber; iter++) {

        // Primal optimization
#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = deblurred_image[i];
        }

        SFFT::fft2D(deblurred_image, deblurred_image_FT, img_width, img_height);

#pragma omp parallel for
        for (int i = 0 ; i < Nfft ; i++){
            residue_image_FT[i][0] = 0.0; //deblurred_image_FT[i][0];
            residue_image_FT[i][1] = 0.0; //deblurred_image_FT[i][1];
        }

        // Data term
        for (int z = 0 ; z < img_depth ; z++){
#pragma omp parallel for
            for (int i = 0 ; i < Nfft ; i++){
                real_tmp = OTF[i][0] * deblurred_image_FT[i][0]
                        - OTF[i][1] * deblurred_image_FT[i][1]
                        - blurry_images_FT[z][i][0];
                imag_tmp = OTF[i][0] * deblurred_image_FT[i][1]
                        + OTF[i][1] * deblurred_image_FT[i][0]
                        - blurry_images_FT[z][i][1];

                real_residu = adjoint_OTF[i][0] * real_tmp
                        - adjoint_OTF[i][1] * imag_tmp;
                imag_residu = adjoint_OTF[i][0] * imag_tmp
                        + adjoint_OTF[i][1] * real_tmp;

                residue_image_FT[i][0] += m_stackCoefficients[z]*real_residu;
                residue_image_FT[i][1] += m_stackCoefficients[z]*imag_residu;
            }
        }

        SFFT::ifft2D(residue_image_FT, residue_image, img_width, img_height);

        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){

                p = y + (img_height)* x;
                tmp = deblurred_image[p] - (primal_step * residue_image[p]/float(N));

                pxm = y + (img_height)* (x-1);
                pxp = y + (img_height)* (x+1);
                if ((x > 0) && (x < img_width - 1)){
                    dxx_adj = dual_image0[pxm] - 2 * dual_image0[p] + dual_image0[pxp];
                }

                pym = (y-1) + (img_height)* x;
                pyp = (y+1) + (img_height)* x;
                if ((y > 0) && (y < img_height - 1)){
                    dyy_adj = dual_image1[pym] - 2 * dual_image1[p] + dual_image1[pyp];
                }

                if ((x == 0) && (y == 0)){
                    dxy_adj = dual_image2[p];
                }
                if ((x > 0) && (y == 0)){
                    dxy_adj = dual_image2[p] - dual_image2[pxm];
                }
                if ((x == 0) && (y > 0)){
                    dxy_adj = dual_image2[p] - dual_image2[pym];
                }

                pxym = (y-1) + (img_height)* (x-1);
                if ((x > 0) && (y > 0)){
                    dxy_adj = dual_image2[p] - dual_image2[pxm]
                            - dual_image2[pym] + dual_image2[pxym];
                }

                tmp -= (primal_weight * (dxx_adj + dyy_adj + sqrt2 * dxy_adj)
                        + primal_weight_comp * dual_image3[p]);

                deblurred_image[p] = SMath::max(min_val, SMath::min(max_val, tmp));

            }
        }

        // Stopping criterion
        if (verbose){
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0){
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
            }
        }

        // Dual optimization
#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * deblurred_image[i] - auxiliary_image[i];
        }

#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){

                p = y + img_height * x;

                pxp = y + img_height * (x+1);
                pxm = y + img_height * (x-1);
                if ((x > 0) && (x < img_width - 1)) {
                    dxx = auxiliary_image[pxp] - 2 * auxiliary_image[p]
                            + auxiliary_image[pxm];
                    dual_image0[p] += dual_weight * dxx;
                }

                pyp = (y+1) + img_height * x;
                pym = (y-1) + img_height * x;
                if ((y > 0) && (y < img_height - 1)) {
                    dyy = auxiliary_image[pyp] - 2 * auxiliary_image[p]
                            + auxiliary_image[pym];
                    dual_image1[p] += dual_weight * dyy;
                }

                pxyp = (y+1) + img_height * (x+1);
                if ((x < img_width - 1) && (y < img_height - 1)) {
                    dxy = auxiliary_image[pxyp] - auxiliary_image[pxp]
                            - auxiliary_image[pyp] + auxiliary_image[p];
                    dual_image2[p] += sqrt2 * dual_weight * dxy;
                }
                dual_image3[p] += dual_weight_comp * auxiliary_image[p];
            }
        }

#pragma omp parallel for
        for (int p = 0 ; p < N ; p++){
            float tmp = SMath::max(1.,
                                   1. / m_regularization
                                   * sqrt(
                                       pow(dual_image0[p], 2.)
                                       + pow(dual_image1[p], 2.)
                                       + pow(dual_image2[p], 2.)
                                       + pow(dual_image3[p], 2.)));
            dual_image0[p] /= tmp;
            dual_image1[p] /= tmp;
            dual_image2[p] /= tmp;
            dual_image3[p] /= tmp;
        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    // copy output
    free(dual_image0);
    free(dual_image1);
    free(dual_image2);
    free(dual_image3);
    free(auxiliary_image);
    free(residue_image);

    for (int z = 0 ; z < img_depth ; z++){
        fftwf_free(blurry_images_FT[z]);
    }
    fftwf_free(deblurred_image_FT);
    fftwf_free(residue_image_FT);


    SImageFloat* outputImage = new SImageFloat(deblurred_image, img_width, img_height, 1);
    if (verbose){
        this->notifyProgress(100);
    }
    return outputImage;

}
