/// \file SDeconvEstimate3d.h
/// \brief SDeconvEstimate3d class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2018

#pragma once

#include <string>
#include <vector>

#include "spitfiredeconvExport.h"
#include "simage/SImageFilter.h"
#include "simage/SImage.h"
#include "sroi/SRoi.h"
#include "sdata/SArray.h"

/// \class SDeconvEstimate3d
/// \brief Estimate the r parameter for SDeconv3d
class SPITFIREDECONV_EXPORT SDeconvEstimate3d : public SImageFilter{

public:
    SDeconvEstimate3d();

public:
    void setInput(SImage* image);

public:
    void setROI(SRoi* roi);
    void setMethod(std::string method);
    void setPSF(SImage* psf);
    void setDelta(float delta);
    void setRegularizationRange(float regularizationMin, float regularizationMax, float step);
    void setWeighting(float weighting);
    void setVerbose(bool verbose);
    void setIntensityNormalization(std::string normalization); // max, sum, L2, 8bits, 12bits, 16bits

public:
    std::vector<float> getEstimationCurveVect();
    SArray* getEstimationCurve();
    float getEstimatedRegularization();

public:
    void checkInputs();
    void run();

protected:
    void analyseRCurve();

protected:
    // inputs
    SImage* m_input;
    SRoi* m_roi;
    SImage* m_psf;
    float m_delta;
    float m_regularizationMin;
    float m_regularizationMax;
    float m_regularizationStep;
    float m_weighting;
    bool m_verbose;
    std::string m_method;
    std::string m_dimension;
    std::string m_normalization;

protected:
    // outputs
    std::vector<float> m_rCurve;
    float m_rEstimated;

};
