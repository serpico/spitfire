/// \file SGaussianPSF.cpp
/// \brief SGaussianPSF class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2020

#include "math.h"

#include "SGaussianPSF.h"
#include "simage/SImage.h"
#include "simage/SImageFloat.h"


SGaussianPSF::SGaussianPSF(){
    m_sigmaX = 1.0;
    m_sigmaY = 1.0;
    m_sigmaZ = 1.0;
    m_sx = 100;
    m_sy = 100;
    m_sz = 1;
}

void SGaussianPSF::setImageSize(unsigned int sx, unsigned int sy, unsigned int sz){
    m_sx = sx;
    m_sy = sy;
    m_sz = sz;
}

void SGaussianPSF::setSigma(float sigmaX, float sigmaY, float sigmaZ){
    m_sigmaX = sigmaX;
    m_sigmaY = sigmaY;
    m_sigmaZ = sigmaZ;
}

void SGaussianPSF::setSigmaX(float sigma){
    m_sigmaX = sigma;
}

void SGaussianPSF::setSigmaY(float sigma){
    m_sigmaY = sigma;
}

void SGaussianPSF::setSigmaZ(float sigma){
    m_sigmaZ = sigma;
}

SImage* SGaussianPSF::getPSF(){
    if (m_sz == 1){
        return this->getPSF2D();
    }
    else{
        return this->getPSF3D();
    }
}

SImage* SGaussianPSF::getPSF2D(){

    SImageFloat* outIm = new SImageFloat(m_sx, m_sy);
    float* buffer = outIm->getBuffer();
    int x0=m_sx/2;
    int y0=m_sy/2;
    for (int x = 0 ; x < int(m_sx) ; x++){
        for (int y = 0 ; y < int(m_sy) ; y++){
            buffer[y + m_sy*x] = exp( - pow(x-x0,2)/(2*m_sigmaX*m_sigmaX) - pow(y-y0,2)/(2*m_sigmaY*m_sigmaY) );
        }
    }
    return outIm;
}

SImage* SGaussianPSF::getPSF3D(){

    SImageFloat* outIm = new SImageFloat(m_sx, m_sy, m_sz);
    float* buffer = outIm->getBuffer();
    int x0=m_sx/2;
    int y0=m_sy/2;
    int z0=m_sz/2;
    for (int x = 0 ; x < int(m_sx) ; x++){
        for (int y = 0 ; y < int(m_sy) ; y++){
            for (int z = 0 ; z < int(m_sz) ; z++){
                buffer[z + m_sz*(y + m_sy*x)] = exp( - pow(x-x0,2)/(2*m_sigmaX*m_sigmaX) - pow(y-y0,2)/(2*m_sigmaY*m_sigmaY) - pow(z-z0,2)/(2*m_sigmaZ*m_sigmaZ) );
            }
        }
    }
    return outIm;
}
