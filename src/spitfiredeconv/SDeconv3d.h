/// \file SDeconv3d.h
/// \brief SDeconv3d class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2018

#pragma once

#include <string>
#include "spitfiredeconvExport.h"

#include "simage/SImageFloat.h"
#include "simage/SImageFilter.h"
#include "sdata/SArray.h"
#include <vector>

/// \class SDeconv3d
/// \brief class that contains the implementation of svdeconv3d
class SPITFIREDECONV_EXPORT SDeconv3d : public SImageFilter{

public:
    SDeconv3d();

public:
    void setMethod(std::string method);
    void setPSF(SImage* psf);
    void setRegularization(float regularization);
    void setWeighting(float weighting);
    void setIterationsNumber(unsigned int iterationsNumber);
    void setVerbose(bool verbose);
    void setPSFNoise(float mean, float sigma);
    void setDelta(float delta);
    void setUseStack(bool useStack);
    void setStackCoefficients(std::vector<float> coefficients);
    void setIntensityNormalization(std::string normalization); // max, sum, L2, 8bits, 12bits, 16bits
    void setCalculateFinalEnergy(bool calculateEnergy);

public:
    void run();

public:
    std::vector<float> getEnergy();
    SArray* getEnergyArray();

protected:
    void runStack();
    void runOnce();

protected:
    SImageFloat* runSV3d(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool m_verbose);
    SImageFloat* runHSV3d(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool m_verbose);
    SImageFloat* runSV3dStack(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose);
    SImageFloat* runHSV3dStack(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose);

protected:
    std::vector<float> HSVEnergy(SImage* blurryImage, SImage* PSF, SImage* debluredImage, float weighting, float reg, float delta);
    std::vector<float> SVEnergy(SImage* blurryImage, SImage* PSF, SImage* debluredImage, float weighting, float reg, float delta);

protected:
    SImage* m_psf;
    float m_regularization;
    float m_weighting;
    unsigned int m_iterationsNumber;
    bool m_verbose;
    std::string m_method;
    float m_psfNoiseMean;
    float m_psfNoiseSigma;
    float m_delta;
    std::string m_normalization;
    bool m_useStack;
    std::vector<float> m_stackCoefficients;
    float m_calculateEnergy;
    std::vector<float> m_energy;
};
