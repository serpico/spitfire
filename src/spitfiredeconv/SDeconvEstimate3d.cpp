/// \file SDeconvEstimate3d.cpp
/// \brief SDeconvEstimate3d class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2020

#include <iostream>
#include <fstream>

#include "SDeconvEstimate3d.h"
#include "SDeconv3d.h"

#include "score/SException.h"
#include "score/SMath.h"
#include "simage/SImageCast.h"
#include "smanipulate/SCrop.h"
#include "sfiltering/SGradient.h"

SDeconvEstimate3d::SDeconvEstimate3d() : SImageFilter(){
    m_verbose = true;
    m_method = "HSV";
    m_dimension = "2D";
    m_input = nullptr;
    m_psf = nullptr;
}

void SDeconvEstimate3d::setInput(SImage* image){
    m_input = image;
}

void SDeconvEstimate3d::setROI(SRoi* roi){
    m_roi = roi;
}

void SDeconvEstimate3d::setMethod(std::string method){
    m_method = method;
}

void SDeconvEstimate3d::setPSF(SImage *psf){
    m_psf = psf;
}

void SDeconvEstimate3d::setDelta(float delta){
    m_delta = delta;
}

void SDeconvEstimate3d::setRegularizationRange(float regularizationMin, float regularizationMax, float step){
    m_regularizationMin = regularizationMin;
    m_regularizationMax = regularizationMax;
    m_regularizationStep = step;

    if (m_regularizationMin > m_regularizationMax){
        throw SException("SDeconvEstimate3d: regularization min must be lower than regularization max !");
    }
    if (m_regularizationStep < 0){
        throw SException("SDeconvEstimate3d: regularization step must be positive !");
    }
}

void SDeconvEstimate3d::setWeighting(float weighting){
    m_weighting = weighting;
    if ( m_weighting < 0 || m_weighting > 1){
        throw SException("SDeconvEstimate3d: weighting must be in [0,1] !");
    }
}

void SDeconvEstimate3d::setVerbose(bool verbose){
    m_verbose = verbose;
}

void SDeconvEstimate3d::setIntensityNormalization(std::string normalization){
    m_normalization = normalization;
}

std::vector<float> SDeconvEstimate3d::getEstimationCurveVect(){
    return m_rCurve;
}

SArray* SDeconvEstimate3d::getEstimationCurve(){
    SArray* array = new SArray(m_rCurve.size());
    for (unsigned int i = 0 ; i < m_rCurve.size() ; i++){
        array->set(i, new SFloat(m_rCurve[i]));
    }
    return array;
}

float SDeconvEstimate3d::getEstimatedRegularization(){
    return m_rEstimated;
}

void SDeconvEstimate3d::checkInputs(){
    if (!m_input){
        throw SException("SDeconvEstimate2d: no input data");
    }
}

void SDeconvEstimate3d::run(){

    m_rCurve.clear();

    // crop input
    SImage* input = nullptr;
    if (m_roi){
        SCrop cropFilter;
        cropFilter.setInput(m_input);
        cropFilter.setRoi(m_roi);
        cropFilter.run();
        input = cropFilter.getOutput();
    }
    else{
        input = m_input;
    }

    if (!input){
        throw SException("SDeconvEstimate3d: Input image in NULL");
    }
    if (!m_psf){
        throw SException("SDeconvEstimate3d: PSF image in NULL");
    }

    // run calculation
    for (float r = m_regularizationMin ; r <= m_regularizationMax ; r += m_regularizationStep){

        std::cout << "run regularization " << r << std::endl;
        // deconvolute the image
        SDeconv3d process;
        process.setInput( input );
        process.setPSF(m_psf);
        process.setDelta(m_delta);
        process.setMethod(m_method);
        process.setRegularization(r);
        process.setWeighting(m_weighting);
        process.setIterationsNumber(200);
        std::cout << "normalization " << m_normalization << std::endl;
        process.setIntensityNormalization(m_normalization);
        process.setVerbose(m_verbose);
        std::cout << "call run " << r << std::endl;
        process.run();
        std::cout << "get output " << r << std::endl;
        SImage* outputImage = process.getOutput();

        // calculate the gradient
        SGradient gradFiler;
        gradFiler.setInput(outputImage);
        gradFiler.run();
        float v = gradFiler.getNormL1();
        m_rCurve.push_back(v);

        delete outputImage;
    }

    this->analyseRCurve();
}

void SDeconvEstimate3d::analyseRCurve(){
    float minVal = SMath::FMAX;
    m_rEstimated = m_regularizationMax;
    for (int i = 0 ; i < int(m_rCurve.size()) ; i++){
        if ( m_rCurve[i] < minVal ){
            minVal = m_rCurve[i];
            m_rEstimated = m_regularizationMin + i*m_regularizationStep;
        }
    }
}
