/// \file SDeconv3d.cpp
/// \brief SDeconv3d class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2018

#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"

#ifdef SL_USE_OPENMP
    #include "omp.h"
#endif

#include "SDeconv3d.h"

#include "score/SException.h"
#include "score/SCoreShortcuts.h"
#include "smanipulate/SNormalize.h"
#include "simage/SImageMath.h"
#include "smanipulate/SResizeCanvas.h"
#include "smanipulate/SShift.h"
#include "sfft/SFFT.h"
#include "sfft/SFFTConvolutionFilter.h"
#include "score/SMath.h"
#include "sdata/SFloat.h"

SDeconv3d::SDeconv3d() : SImageFilter(){
    m_processPrecision = 32;
    m_processZ = true;
    m_processT = false;
    m_processC = false;

    m_verbose = true;
    m_method = "SV";
    m_psfNoiseMean = 0;
    m_psfNoiseSigma = 0;
    m_delta = 1.0;
    m_calculateEnergy = false;
}

void SDeconv3d::setMethod(std::string method){
    m_method = method;
}

void SDeconv3d::setPSF(SImage* psf){
    m_psf = psf;
}

void SDeconv3d::setRegularization(float regularization){
    m_regularization = pow(2,-regularization);
}

void SDeconv3d::setWeighting(float weighting){
    m_weighting = weighting;
}

void SDeconv3d::setIterationsNumber(unsigned int iterationsNumber){
    m_iterationsNumber = iterationsNumber;
}

void SDeconv3d::setVerbose(bool verbose){
    m_verbose = verbose;
}

void SDeconv3d::setPSFNoise(float mean, float sigma){
    m_psfNoiseMean = mean;
    m_psfNoiseSigma = sigma;
}

void SDeconv3d::setDelta(float delta){

    if (delta < 0 || delta > 1){
        throw SException("ERROR: delta must be in [0, 1.0]");
    }
    m_delta = delta;
}

void SDeconv3d::setUseStack(bool useStack){
    m_useStack = useStack;
}

void SDeconv3d::setStackCoefficients(std::vector<float> coefficients){
    m_stackCoefficients = coefficients;

    if (m_verbose && m_useStack){
        this->notify("Stack coefficients:");
        for (int i = 0 ; i < int(coefficients.size()) ; i++){
            this->notify(std::to_string(coefficients[i]));
        }
    }
}

void SDeconv3d::setIntensityNormalization(std::string normalization){
    m_normalization = normalization;
}

void SDeconv3d::setCalculateFinalEnergy(bool calculateEnergy){
    m_calculateEnergy = calculateEnergy;
}

std::vector<float> SDeconv3d::getEnergy(){
    return m_energy;
}

SArray* SDeconv3d::getEnergyArray(){
    SArray* energyArray = new SArray(3);
    energyArray->set(0, new SFloat(m_energy[0]) );


    energyArray->set(1, new SFloat(m_energy[1]) );
    energyArray->set(2, new SFloat(m_energy[2]) );
    return energyArray;
}

void SDeconv3d::run(){

    this->checkInputs();

    if (m_useStack){
        this->runStack();
    }
    else{
        this->runOnce();
    }
}

void SDeconv3d::runOnce(){

    SImageFloat* inputFloat = dynamic_cast<SImageFloat*>(m_input);

    // resize PSF
    SResizeCanvas canvasFilter;
    canvasFilter.setInput(m_psf);
    canvasFilter.setSize(m_input->getSizeX(), m_input->getSizeY(), m_input->getSizeZ());
    canvasFilter.run();
    SImageFloat* psf = dynamic_cast<SImageFloat*>(canvasFilter.getOutput());

    // normalize inputs
    float maxInput = inputFloat->getMax();
    SNormalize normInputFilter;
    normInputFilter.setMethod(m_normalization);
    normInputFilter.setInput(m_input);
    normInputFilter.run();
    SImage* normInput = normInputFilter.getOutput();

    SNormalize normPSFFilter;
    normPSFFilter.setMethod("sum");
    normPSFFilter.setInput(psf);
    normPSFFilter.run();
    SImage* normPSF = normPSFFilter.getOutput();

    // run deconvolution
    SImageFloat* outputImage = nullptr;
    if (m_method == "SV"){
        if (m_verbose){
            this->notify("use SV");
        }
        outputImage = this->runSV3d(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, m_verbose);
        if (m_calculateEnergy){
            m_energy = this->SVEnergy(normInput, normPSF, outputImage, m_weighting, m_regularization, m_delta);
        }
    }
    else if (m_method == "HSV"){
        if (m_verbose){
            this->notify("use HSV");
        }
        outputImage = this->runHSV3d(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, m_verbose);
        if (m_calculateEnergy){
            m_energy = this->HSVEnergy(normInput, normPSF, outputImage, m_weighting, m_regularization, m_delta);
        }
    }
    else{
        this->notify("ERROR: Method must be SV or HSV");
    }

    SImageMath::multiply(outputImage, maxInput/ outputImage->getMax());
    m_output = outputImage;
}


void SDeconv3d::runStack(){

    if (m_verbose){
        this->notify("Run stack mode");
    }

    // Check stack coefficients
    if (m_stackCoefficients.size() == 0){
        this->notify("WARNING: Stack coefficients empty - use mean");
        m_stackCoefficients.resize(m_input->getSizeZ());
        for (unsigned int z = 0 ; z < m_input->getSizeZ() ; z++){
            m_stackCoefficients[z] = 1/float(m_input->getSizeZ());
        }
    }

    if (m_stackCoefficients.size() != m_input->getSizeZ()){
        this->notify("Error: stack coefficients size (" + std::to_string(m_stackCoefficients.size())+") different of stack size ("+std::to_string(m_input->getSizeZ()));
        return;
    }

    // resize PSF
    SResizeCanvas canvasFilter;
    canvasFilter.setInput(m_psf);
    canvasFilter.setSize(m_input->getSizeX(), m_input->getSizeY(), m_input->getSizeZ());
    canvasFilter.run();
    SImageFloat* psf = dynamic_cast<SImageFloat*>(canvasFilter.getOutput());

    // normalize inputs
    SImageFloat* inputFloat = dynamic_cast<SImageFloat*>(m_input);
    float maxInput = inputFloat->getMax();
    SNormalize normInputFilter;
    normInputFilter.setMethod(m_normalization);
    normInputFilter.setInput(m_input);
    normInputFilter.run();
    SImage* normInput = normInputFilter.getOutput();

    SNormalize normPSFFilter;
    normPSFFilter.setMethod(SNormalize::Sum);
    normPSFFilter.setInput(psf);
    normPSFFilter.run();
    SImage* normPSF = normPSFFilter.getOutput();

    delete psf;

    // run deconv
    SImageFloat* outputImage = nullptr;
    if (m_method == "SV"){
        if (m_verbose){
            this->notify("use SV");
        }
        outputImage = this->runSV3dStack(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, m_verbose);
    }
    else if (m_method == "HSV") {
        if (m_verbose){
            this->notify("use HSV");
        }
        outputImage = this->runHSV3dStack(dynamic_cast<SImageFloat*>(normInput), dynamic_cast<SImageFloat*>(normPSF), m_regularization, m_weighting, m_iterationsNumber, m_verbose);
    }
    else{
        this->notify("ERROR: Method must be SV or HSV");
    }

    SImageMath::multiply(outputImage, maxInput/ outputImage->getMax());
    m_output = outputImage;
}


SImageFloat* SDeconv3d::runSV3d(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool m_verbose){

#ifdef SL_USE_OPENMP
    omp_set_num_threads(omp_get_max_threads());
    int fftThreads = fftwf_init_threads();
    if (fftThreads == 0){
        std::cout << "Cannot initialize parrallel fft: error " << fftThreads << std::endl;
    }
#endif

    int img_width = image->getSizeX();
    int img_height = image->getSizeY();
    int img_depth = image->getSizeZ();
    int N = img_width*img_height*img_depth;
    int Nfft = img_width*img_height*(img_depth/2 + 1);

    // m_buffer[ z + m_sz*(y + m_sy*x)]
    // Optical transfer function and its adjoint
    SShift shiftOTFFilter;
    shiftOTFFilter.setInput(psf);
    shiftOTFFilter.setShift(int(- float(img_width) / 2.0), int(-float(img_height) / 2.0), int(-float(img_depth) / 2.0));
    shiftOTFFilter.run();
    SImageFloat* OTFCImg = dynamic_cast<SImageFloat*>(shiftOTFFilter.getOutput());

    SImageFloat* adjoint_PSF = new SImageFloat(img_width, img_height, img_depth);
    float* psfBuffer = psf->getBuffer();
    float* adjoint_PSFBuffer = adjoint_PSF->getBuffer();
    for (int x = 0 ; x < img_width ; x++){
        for (int y = 0 ; y < img_height ; y++){
            for (int z = 0 ; z < img_depth ; z++){
                adjoint_PSFBuffer[z + img_depth*(y + img_height*x)] = psfBuffer[(img_depth - 1 - z) + img_depth*((img_height - 1 - y) + img_height*(img_width - 1 - x))];
            }
        }
    }

    SShift shiftAdjPSFFilter;
    shiftAdjPSFFilter.setInput(adjoint_PSF);
    shiftAdjPSFFilter.setShift(-(int(img_width) - 1) % 2, -(int(img_height) - 1) % 2, -int((img_depth - 1)) % 2);
    shiftAdjPSFFilter.run();
    SImageFloat* adjoint_PSF_shift = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter.getOutput());

    SShift shiftAdjPSFFilter2;
    shiftAdjPSFFilter2.setInput(adjoint_PSF_shift);
    shiftAdjPSFFilter2.setShift(int(-float(img_width) / 2.0), int(-float(img_height) / 2.0), int(-float(img_depth) / 2.0));
    shiftAdjPSFFilter2.run();
    SImageFloat* adjoint_OTFCimg = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter2.getOutput());
    delete adjoint_PSF_shift;

    // copy inputs to buffer
    float* blurry_array = image->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* OTFReal = OTFCImg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* adjoint_OTFReal = adjoint_OTFCimg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* deblurred_image = (float*) malloc(sizeof(float) * N);

    int p = 0;
    // Splitting parameters
    float dual_step = SMath::max(0.01, SMath::min(0.1, m_regularization));
    float primal_step = 0.99 / (0.5 + (12 * pow(m_weighting, 2.) + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);


    float* dual_image0 = (float*) malloc(sizeof(float) * N);
    float* dual_image1 = (float*) malloc(sizeof(float) * N);
    float* dual_image2 = (float*) malloc(sizeof(float) * N);
    float* dual_image3 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* residue_image = (float*) malloc(sizeof(float) * N);

#pragma omp parallel for 
    for (int i = 0 ; i < N ; i++){
        dual_image0[i] = 0.0;
        dual_image1[i] = 0.0;
        dual_image2[i] = 0.0;
        dual_image3[i] = 0.0;
        deblurred_image[i] = blurry_array[i];
    }

    fftwf_complex* blurry_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* deblurred_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* residue_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* adjoint_OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));

    SFFT::fft3D(blurry_array, blurry_image_FT, img_width, img_height, img_depth);
    SFFT::fft3D(OTFReal, OTF, img_width, img_height, img_depth);
    SFFT::fft3D(adjoint_OTFReal, adjoint_OTF, img_width, img_height, img_depth);

    free(blurry_array);
    free(OTFReal);
    free(adjoint_OTFReal);

#pragma omp parallel for 
    for (int i = 0 ; i < Nfft ; i++){
        deblurred_image_FT[i][0] = blurry_image_FT[i][0];
        deblurred_image_FT[i][1] = blurry_image_FT[i][1];
        residue_image_FT[i][0] = blurry_image_FT[i][0];
        residue_image_FT[i][1] = blurry_image_FT[i][1];
    }


    // Deconvolution process
    float tmp, dx, dy, dz, dx_adj, dy_adj, dz_adj;
    float real_tmp, imag_tmp;
    float min_val = 0.0;
    float max_val = 1.0;
    for (unsigned int iter = 0; iter < m_iterationsNumber; iter++) {
        // Primal optimization
#pragma omp parallel for 
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = deblurred_image[i];
        }
        SFFT::fft3D(deblurred_image, deblurred_image_FT, img_width, img_height, img_depth);

#pragma omp parallel for 
        for (int i = 0 ; i < Nfft ; i++){
            residue_image_FT[i][0] = deblurred_image_FT[i][0];
            residue_image_FT[i][1] = deblurred_image_FT[i][1];
        }

        // Data term
#pragma omp parallel for 
        for (int i = 0 ; i < Nfft ; i++){
            real_tmp = OTF[i][0] * deblurred_image_FT[i][0]
                    - OTF[i][1] * deblurred_image_FT[i][1]
                    - blurry_image_FT[i][0];
            imag_tmp = OTF[i][0] * deblurred_image_FT[i][1]
                    + OTF[i][1] * deblurred_image_FT[i][0]
                    - blurry_image_FT[i][1];

            residue_image_FT[i][0] = adjoint_OTF[i][0] * real_tmp
                    - adjoint_OTF[i][1] * imag_tmp;
            residue_image_FT[i][1] = adjoint_OTF[i][0] * imag_tmp
                    + adjoint_OTF[i][1] * real_tmp;

        }

        SFFT::ifft3D(residue_image_FT, residue_image, img_width, img_height, img_depth);

        // gradient term
        int pd;
#pragma omp parallel for 
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++) {

                    p = z + (img_depth)* (y + (img_height)* x);
                    tmp = deblurred_image[p] - primal_step * residue_image[p]/float(N);

                    pd = z + (img_depth)* (y + (img_height)* (x-1));
                    if (x > 0)
                        dx_adj = dual_image0[pd] - dual_image0[p];
                    else
                        dx_adj = 0.0; //dual_image0[p];

                    pd = z + (img_depth)* ((y-1) + (img_height)* (x));
                    if (y > 0)
                        dy_adj = dual_image1[pd] - dual_image1[p];
                    else
                        dy_adj = 0.0; //dual_image1[p];

                    pd = (z-1) + (img_depth)* ((y) + (img_height)* (x));
                    if (z > 0)
                        dz_adj = m_delta*(dual_image2[pd] - dual_image2[p]);
                    else
                        dz_adj = 0.0; //dual_image2[p];

                    tmp -= (primal_weight * (dx_adj + dy_adj + dz_adj) + primal_weight_comp * dual_image3[p]);
                    deblurred_image[p] = SMath::max(min_val, SMath::min(max_val, tmp));
                }
            }
        }

        // verbose
        if(m_verbose){
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0){
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
            }
        }

        // Dual optimization
#pragma omp parallel for 
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * deblurred_image[i] - auxiliary_image[i];
        }

#pragma omp parallel for 
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++){

                    p = z + (img_depth)* (y + (img_height)* x);

                    if (x < img_width - 1) {
                        pd = z + (img_depth)* (y + (img_height)* (x+1));
                        dx = auxiliary_image[pd] - auxiliary_image[p];
                        dual_image0[p] += dual_weight * dx;
                    }
                    if (y < img_height - 1) {
                        pd = z + (img_depth)* ((y+1) + (img_height)* x);
                        dy = auxiliary_image[pd] - auxiliary_image[p];
                        dual_image1[p] += dual_weight * dy;
                    }
                    if (z < img_depth - 1) {
                        pd = (z+1) + (img_depth)* (y + (img_height)* x);
                        dz = m_delta*( auxiliary_image[pd] - auxiliary_image[p] );
                        dual_image2[p] += dual_weight * dz;
                    }

                    dual_image3[p] += dual_weight_comp * auxiliary_image[p];
                }
            }
        }

#pragma omp parallel for 
        for(int p = 0 ; p < N ; p++){
            tmp = SMath::max(1.,
                             1. / m_regularization
                             * sqrt(
                                 pow(dual_image0[p], 2.)
                                 + pow(dual_image1[p], 2.)
                                 + pow(dual_image2[p], 2.)
                                 + pow(dual_image3[p], 2.)));
            dual_image0[p] /= tmp;
            dual_image1[p] /= tmp;
            dual_image2[p] /= tmp;
            dual_image3[p] /= tmp;
        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    // copy output
    free(dual_image0);
    free(dual_image1);
    free(dual_image2);
    free(dual_image3);
    free(auxiliary_image);
    free(residue_image);

    fftwf_free(blurry_image_FT);
    fftwf_free(deblurred_image_FT);
    fftwf_free(residue_image_FT);
    fftwf_free(OTF);
    fftwf_free(adjoint_OTF);

    SImageFloat* outputImage = new SImageFloat(deblurred_image, img_width, img_height, img_depth);
    this->notifyProgress(100);
    return outputImage;
}

SImageFloat* SDeconv3d::runHSV3d(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool m_verbose){
    int img_width = image->getSizeX();
    int img_height = image->getSizeY();
    int img_depth = image->getSizeZ();
    int N = img_width*img_height*img_depth;
    int Nfft = img_width*img_height*(img_depth/2 + 1);
    float sqrt2 = sqrt(2.);

#ifdef SL_USE_OPENMP
    omp_set_num_threads(omp_get_max_threads());

    int fftThreads = fftwf_init_threads();
    if (fftThreads == 0){
        std::cout << "Cannot initialize parrallel fft: error " << fftThreads << std::endl;
    }
#endif

    // Optical transfer function and its adjoint
    SShift shiftOTFFilter;
    shiftOTFFilter.setInput(psf);
    shiftOTFFilter.setShift(int(- float(img_width) / 2.0), int(-float(img_height) / 2.0), int(-float(img_depth) / 2.0));
    shiftOTFFilter.run();
    SImageFloat* OTFCImg = dynamic_cast<SImageFloat*>(shiftOTFFilter.getOutput());

    SImageFloat* adjoint_PSF = new SImageFloat(img_width, img_height, img_depth);
    float* psfBuffer = psf->getBuffer();
    float* adjoint_PSFBuffer = adjoint_PSF->getBuffer();
    for (int x = 0 ; x < img_width ; x++){
        for (int y = 0 ; y < img_height ; y++){
            for (int z = 0 ; z < img_depth ; z++){
                adjoint_PSFBuffer[z + img_depth*(y + img_height*x)] = psfBuffer[(img_depth - 1 - z) + img_depth*((img_height - 1 - y) + img_height*(img_width - 1 - x))];
            }
        }
    }

    SShift shiftAdjPSFFilter;
    shiftAdjPSFFilter.setInput(adjoint_PSF);
    shiftAdjPSFFilter.setShift(-(int(img_width) - 1) % 2, -(int(img_height) - 1) % 2, -int((img_depth - 1)) % 2);
    shiftAdjPSFFilter.run();
    SImageFloat* adjoint_PSF_shift = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter.getOutput());

    SShift shiftAdjPSFFilter2;
    shiftAdjPSFFilter2.setInput(adjoint_PSF_shift);
    shiftAdjPSFFilter2.setShift(int(-float(img_width) / 2.0), int(-float(img_height) / 2.0), int(-float(img_depth) / 2.0));
    shiftAdjPSFFilter2.run();
    SImageFloat* adjoint_OTFCimg = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter2.getOutput());
    delete adjoint_PSF_shift;

    // Splitting parameters
    float dual_step = SMath::max(0.001, SMath::min(0.01, m_regularization));
    float primal_step = 0.99
            / (0.5
               + (144 * pow(m_weighting, 2.)
                  + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);


    // copy inputs to buffer
    float* blurry_array = image->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* OTFReal = OTFCImg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* adjoint_OTFReal = adjoint_OTFCimg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* deblurred_image = (float*) malloc(sizeof(float) * N);

    float* dual_image0 = (float*) malloc(sizeof(float) * N);
    float* dual_image1 = (float*) malloc(sizeof(float) * N);
    float* dual_image2 = (float*) malloc(sizeof(float) * N);
    float* dual_image3 = (float*) malloc(sizeof(float) * N);
    float* dual_image4 = (float*) malloc(sizeof(float) * N);
    float* dual_image5 = (float*) malloc(sizeof(float) * N);
    float* dual_image6 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* residue_image = (float*) malloc(sizeof(float) * N);

#pragma omp parallel for 
    for (int i = 0 ; i < N ; i++){
        dual_image0[i] = 0.0;
        dual_image1[i] = 0.0;
        dual_image2[i] = 0.0;
        dual_image3[i] = 0.0;
        dual_image4[i] = 0.0;
        dual_image5[i] = 0.0;
        dual_image6[i] = 0.0;
        deblurred_image[i] = blurry_array[i];
    }

    fftwf_complex* blurry_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* deblurred_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* residue_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* adjoint_OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));

    SFFT::fft3D(blurry_array, blurry_image_FT, img_width, img_height, img_depth);
    SFFT::fft3D(OTFReal, OTF, img_width, img_height, img_depth);
    SFFT::fft3D(adjoint_OTFReal, adjoint_OTF, img_width, img_height, img_depth);

    //free(blurry_array);
    free(OTFReal);
    free(adjoint_OTFReal);

#pragma omp parallel for 
    for (int i = 0 ; i < Nfft ; i++){
        deblurred_image_FT[i][0] = blurry_image_FT[i][0];
        deblurred_image_FT[i][1] = blurry_image_FT[i][1];
        residue_image_FT[i][0] = blurry_image_FT[i][0];
        residue_image_FT[i][1] = blurry_image_FT[i][1];
    }


    // Deconvolution process
    float tmp, dxx, dyy, dzz, dxy, dyz, dzx, min_val, max_val, dxx_adj,
            dyy_adj, dzz_adj, dxy_adj, dyz_adj, dzx_adj;


    min_val = 0.0;
    max_val = 1.0;

    float real_tmp, imag_tmp;
    int p;
    int pxm, pxp,pym, pyp,pzm, pzp, pxyp, pyzp, pxzp, pyzm, pxym, pxzm;

    for (unsigned int iter = 0; iter < m_iterationsNumber; iter++) {
        // Primal optimization
#pragma omp parallel for 
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = deblurred_image[i];
        }

        SFFT::fft3D(deblurred_image, deblurred_image_FT, img_width, img_height, img_depth);

#pragma omp parallel for 
        for (int i = 0 ; i < Nfft ; i++){
            residue_image_FT[i][0] = deblurred_image_FT[i][0];
            residue_image_FT[i][1] = deblurred_image_FT[i][1];
        }

        // Data term
#pragma omp parallel for 
        for (int i = 0 ; i < Nfft ; i++){
            real_tmp = OTF[i][0] * deblurred_image_FT[i][0]
                    - OTF[i][1] * deblurred_image_FT[i][1]
                    - blurry_image_FT[i][0];
            imag_tmp = OTF[i][0] * deblurred_image_FT[i][1]
                    + OTF[i][1] * deblurred_image_FT[i][0]
                    - blurry_image_FT[i][1];

            residue_image_FT[i][0] = adjoint_OTF[i][0] * real_tmp
                    - adjoint_OTF[i][1] * imag_tmp;
            residue_image_FT[i][1] = adjoint_OTF[i][0] * imag_tmp
                    + adjoint_OTF[i][1] * real_tmp;

        }

        SFFT::ifft3D(residue_image_FT, residue_image, img_width, img_height, img_depth);

        // gradient term
#pragma omp parallel for 
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++) {

                    p = z + (img_depth)* (y + (img_height)* x);
                    tmp = auxiliary_image[p] - primal_step * residue_image[p]/float(N);

                    dxx_adj = dyy_adj = dzz_adj = dxy_adj = dyz_adj = dzx_adj = 0.;

                    // Diagonal terms
                    pxm =  z + (img_depth)* (y + (img_height)* (x-1));
                    pxp =  z + (img_depth)* (y + (img_height)* (x+1));
                    if ((x > 0) && (x < img_width - 1)){
                        dxx_adj = dual_image0[pxm] - 2 * dual_image0[p] + dual_image0[pxp];
                    }

                    pym =  z + (img_depth)* ((y-1) + (img_height)* (x));
                    pyp =  z + (img_depth)* ((y+1) + (img_height)* (x));
                    if ((y > 0) && (y < img_height - 1)){
                        dyy_adj = dual_image1[pym] - 2 * dual_image1[p] + dual_image1[pyp];
                    }

                    pzm =  (z-1) + (img_depth)* ((y) + (img_height)* (x));
                    pzp =  (z+1) + (img_depth)* ((y) + (img_height)* (x));
                    if ((z > 0) && (z < img_depth - 1)){
                        dzz_adj = (m_delta*m_delta)*(dual_image2[pzm] - 2 * dual_image2[p] + dual_image2[pzp]);
                    }

                    // Other terms
                    if ((x == 0) && (y == 0)){
                        dxy_adj = dual_image3[p];
                    }

                    if ((x > 0) && (y == 0)){
                        dxy_adj = dual_image3[p] - dual_image3[pxm];
                    }

                    if ((x == 0) && (y > 0)){
                        dxy_adj = dual_image3[p] - dual_image3[pym];
                    }

                    pxym = z + (img_depth)* ((y-1) + (img_height)* (x-1));
                    if ((x > 0) && (y > 0)){
                        dxy_adj = dual_image3[p] - dual_image3[pxm] - dual_image3[pym] + dual_image3[pxym];
                    }

                    if ((y == 0) && (z == 0)){
                        dyz_adj = dual_image4[p];
                    }

                    if ((y > 0) && (z == 0)){
                        dyz_adj = dual_image4[p] - dual_image4[pym];
                    }

                    if ((y == 0) && (z > 0)){
                        dyz_adj = dual_image4[p] - dual_image4[pzm];
                    }

                    pyzm = (z-1) + (img_depth)* ((y-1) + (img_height)* x);
                    if ((y > 0) && (z > 0)){
                        dyz = m_delta*( dual_image4[p] - dual_image4[pym] - dual_image4[pzm] + dual_image4[pyzm] );
                    }


                    if ((z == 0) && (x == 0)){
                        dzx_adj = dual_image5[p];
                    }

                    if ((z > 0) && (x == 0)){
                        dzx_adj = dual_image5[p] - dual_image5[pzm];
                    }

                    if ((z == 0) && (x > 0)){
                        dzx_adj = m_delta*(dual_image5[pxm] - dual_image5[p]);
                    }

                    pxzm = (z-1) + (img_depth)* (y + (img_height)* (x-1));
                    if ((z > 0) && (x > 0)){
                        dzx_adj = dual_image5[p] - dual_image5[pzm]
                                - dual_image5[pxm]
                                + dual_image5[pxzm];
                    }


                    tmp -= (primal_weight
                            * (dxx_adj + dyy_adj + dzz_adj
                               + sqrt2 * (dxy_adj + dyz_adj + dzx_adj))
                            + primal_weight_comp * dual_image6[p]);

                    deblurred_image[p] = SMath::max(0.0, SMath::min(1.0, tmp));

                }
            }
        }

        // Stopping criterion
        if (m_verbose){
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0)
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
        }


        // Dual optimization
#pragma omp parallel for 
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * deblurred_image[i] - auxiliary_image[i];
        }

#pragma omp parallel for 
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++){

                    p = z + (img_depth)* (y + (img_height)* x);

                    pxp = z + (img_depth)* (y + (img_height)* (x+1));
                    pxm = z + (img_depth)* (y + (img_height)* (x-1));
                    if ((x > 0) && (x < img_width - 1)) {
                        dxx = auxiliary_image[pxp]
                                - 2 * auxiliary_image[p]
                                + auxiliary_image[pxm];
                        dual_image0[p] += dual_weight * dxx;
                    }

                    pyp = z + (img_depth)* ((y+1) + (img_height)* x);
                    pym = z + (img_depth)* ((y-1) + (img_height)* x);
                    if ((y > 0) && (y < img_height - 1)) {
                        dyy = auxiliary_image[pyp]
                                - 2 * auxiliary_image[p]
                                + auxiliary_image[pym];
                        dual_image1[p] += dual_weight * dyy;
                    }

                    pzp = (z+1) + (img_depth)* (y + (img_height)* x);
                    pzm = (z-1) + (img_depth)* (y + (img_height)* x);
                    if ((z > 0) && (z < img_depth - 1)) {
                        dzz = (m_delta*m_delta)*(auxiliary_image[pzp]
                                                 - 2 * auxiliary_image[p]
                                                 + auxiliary_image[pzm]);
                        dual_image2[p] += dual_weight * dzz;
                    }

                    pxyp = z + (img_depth)* ((y+1) + (img_height)* (x+1));
                    if ((x < img_width - 1) && (y < img_height - 1)) {
                        dxy = auxiliary_image[pxyp]
                                - auxiliary_image[pxp]
                                - auxiliary_image[pyp]
                                + auxiliary_image[p];
                        dual_image3[p] += sqrt2 * dual_weight * dxy;
                    }

                    pyzp = (z+1) + (img_depth)* ((y+1) + (img_height)* (x));
                    if ((y < img_height - 1) && (z < img_depth - 1)) {
                        dyz = m_delta*(auxiliary_image[pyzp]
                                       - auxiliary_image[pyp]
                                       - auxiliary_image[pzp]
                                       + auxiliary_image[p]);
                        dual_image4[p] += sqrt2 * dual_weight * dyz;
                    }

                    pxzp = (z+1) + (img_depth)* ((y) + (img_height)* (x+1));
                    if ((z < img_depth - 1) && (x < img_width - 1)) {
                        dzx = m_delta*(auxiliary_image[pxzp]
                                       - auxiliary_image[pxp]
                                       - auxiliary_image[pzp]
                                       + auxiliary_image[p]);
                        dual_image5[p] += sqrt2 * dual_weight * dzx;
                    }

                    dual_image6[p] += dual_weight_comp * auxiliary_image[p];

                }
            }
        }

#pragma omp parallel for 
        for (int p = 0 ; p < N ; p++){
            float tmp = SMath::max(1.,
                                   1. / m_regularization
                                   * sqrt(
                                       pow(dual_image0[p], 2.)
                                       + pow(dual_image1[p], 2.)
                                       + pow(dual_image2[p], 2.)
                                       + pow(dual_image3[p], 2.)
                                       + pow(dual_image4[p], 2.)
                                       + pow(dual_image5[p], 2.)
                                       + pow(dual_image6[p],
                                             2.)));
            dual_image0[p] /= tmp;
            dual_image1[p] /= tmp;
            dual_image2[p] /= tmp;
            dual_image3[p] /= tmp;
            dual_image4[p] /= tmp;
            dual_image5[p] /= tmp;
            dual_image6[p] /= tmp;
        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    // copy output
    free(dual_image0);
    free(dual_image1);
    free(dual_image2);
    free(dual_image3);
    free(dual_image4);
    free(dual_image5);
    free(dual_image6);
    free(auxiliary_image);
    free(residue_image);

    fftwf_free(blurry_image_FT);
    fftwf_free(deblurred_image_FT);
    fftwf_free(residue_image_FT);

    SImageFloat* outputImage = new SImageFloat(deblurred_image, img_width, img_height, img_depth);
    this->notifyProgress(100);
    return outputImage;
}

std::vector<float> SDeconv3d::HSVEnergy(SImage* blurryImage, SImage* PSF, SImage* debluredImage, float weighting, float reg, float delta){

    // deblured_image_buffer[ (z) + sz*((y) + sy*(x))]
    //std::cout << "Calculate energy begin" << std::endl;
    float hessian = 0.0;
    float dxx, dyy, dzz;
    float dxy;
    float dzx;
    float dyz;
    float Er = 0.0;
    //std::cout << "Calculate energy get deblured image" << std::endl;
    float* deblured_image_buffer = dynamic_cast<SImageFloat*>(debluredImage)->getBuffer();
    //std::cout << "Calculate energy getten deblured image" << std::endl;
    unsigned int sx = debluredImage->getSizeX();
    unsigned int sy = debluredImage->getSizeY();
    unsigned int sz = debluredImage->getSizeZ();
    //std::cout << "Calculate energy Er" << std::endl;
    for (unsigned int x = 0 ; x < sx ; x++){
        for (unsigned int y = 0 ; y < sy ; y++){
            for (unsigned int z = 0 ; z < sz ; z++){

                dxx = 0; dyy = 0; dzz = 0; dxy = 0; dzx = 0; dyz = 0;
                if ((x > 0) && (x < sx - 1))
                    dxx = deblured_image_buffer[ (z) + sz*((y) + sy*(x-1))] - 2 * deblured_image_buffer[ (z) + sz*((y) + sy*(x))] + deblured_image_buffer[ (z) + sz*((y) + sy*(x+1))];
                if ((y > 0) && (y < sy - 1))
                    dyy = deblured_image_buffer[ (z) + sz*((y-1) + sy*(x))] - 2*deblured_image_buffer[ (z) + sz*((y) + sy*(x))] + deblured_image_buffer[ (z) + sz*((y+1) + sy*(x))];
                if ((z > 0) && (z < sz - 1))
                    dzz = deblured_image_buffer[ (z-1) + sz*((y) + sy*(x))] - 2*deblured_image_buffer[ (z) + sz*((y) + sy*(x))] + deblured_image_buffer[ (z+1) + sz*((y) + sy*(x))];


                if ((x == 0) && (y == 0))
                    dxy = deblured_image_buffer[ (z) + sz*((y) + sy*(x))];
                if ((x > 0) && (y == 0))
                    dxy = deblured_image_buffer[ (z) + sz*((y) + sy*(x))] - deblured_image_buffer[ (z) + sz*((y) + sy*(x-1))];
                if ((x == 0) && (y > 0))
                    dxy = deblured_image_buffer[ (z) + sz*((y) + sy*(x))] - deblured_image_buffer[ (z) + sz*((y-1) + sy*(x))];
                if ((x > 0) && (y > 0))
                    dxy = deblured_image_buffer[ (z) + sz*((y) + sy*(x))] - deblured_image_buffer[ (z) + sz*((y) + sy*(x-1))] - deblured_image_buffer[ (z) + sz*((y-1) + sy*(x))] + deblured_image_buffer[ (z) + sz*((y-1) + sy*(x-1))];

                if ((y == 0) && (z == 0))
                    dyz = deblured_image_buffer[ (z) + sz*((y) + sy*(x))];
                if ((y > 0) && (z == 0))
                    dyz = deblured_image_buffer[ (z) + sz*((y) + sy*(x))] - deblured_image_buffer[ (z) + sz*((y-1) + sy*(x))];
                if ((y == 0) && (z > 0))
                    dyz = deblured_image_buffer[ (z) + sz*((y) + sy*(x))] - deblured_image_buffer[ (z-1) + sz*((y) + sy*(x))];
                if ((y > 0) && (z > 0))
                    dyz = delta*(deblured_image_buffer[ (z) + sz*((y) + sy*(x))] - deblured_image_buffer[ (z) + sz*((y-1) + sy*(x))] - deblured_image_buffer[ (z-1) + sz*((y) + sy*(x))] + deblured_image_buffer[ (z-1) + sz*((y-1) + sy*(x))]);

                if ((z == 0) && (x == 0))
                    dzx = deblured_image_buffer[ (z) + sz*((y) + sy*(x))];
                if ((z > 0) && (x == 0))
                    dzx = deblured_image_buffer[ (z) + sz*((y) + sy*(x))] - deblured_image_buffer[ (z-1) + sz*((y) + sy*(x))];
                if ((z == 0) && (x > 0))
                    dzx = deblured_image_buffer[ (z) + sz*((y) + sy*(x-1))] - deblured_image_buffer[ (z) + sz*((y) + sy*(x))];
                if ((z > 0) && (x > 0))
                    dzx = delta*(deblured_image_buffer[ (z) + sz*((y) + sy*(x))] - deblured_image_buffer[ (z-1) + sz*((y) + sy*(x))] - deblured_image_buffer[ (z) + sz*((y) + sy*(x-1))] + deblured_image_buffer[ (z-1) + sz*((y) + sy*(x-1))]);

                hessian = dxx*dxx + dyy*dyy + dzz*dzz + dxy*dxy + dyz*dyz + dzx*dzx;
                Er += sqrt(pow(weighting,2)*hessian + pow(1-weighting,2)*pow(deblured_image_buffer[ (z) + sz*((y) + sy*(x))],2));
            }
        }
    }

    // Calculate Ed
    //std::cout << "Calculate energy Ed" << std::endl;
    SFFTConvolutionFilter convFilter;
    convFilter.setImage1(debluredImage);
    convFilter.setImage2(PSF);
    convFilter.run();
    SImageFloat* conv_deblured = dynamic_cast<SImageFloat*>(convFilter.getOutput());
    //std::cout << "Calculate energy conv_deblured done" << std::endl;
    int sx_ = conv_deblured->getSizeX();
    int sy_ = conv_deblured->getSizeY();
    int sz_ = conv_deblured->getSizeZ();
    //std::cout << "Calculate energy conv_deblured sizex="<< sx_ << ", " << sy_ << ", " << sz_ << std::endl;
    float Ed = 0;
    float* blurry_image_buffer = dynamic_cast<SImageFloat*>(blurryImage)->getBuffer();
    //std::cout << "Calculate energy blurry sizex="<< dynamic_cast<SImageFloat*>(blurryImage)->getSizeX() << ", " << dynamic_cast<SImageFloat*>(blurryImage)->getSizeY() << ", " << dynamic_cast<SImageFloat*>(blurryImage)->getSizeZ() << std::endl;
    //std::cout << "buffer blurry 0 = " << blurry_image_buffer[0] << std::endl;
    float* conv_deblured_buffer = dynamic_cast<SImageFloat*>(conv_deblured)->getBuffer();
    
    //std::cout << "bufffer conv_deblured_buffer 0 = " << conv_deblured_buffer[0] << std::endl;
#pragma omp parallel for
    for (unsigned int i = 0 ; i < sx*sy*sz ; i++){
        Ed += pow(blurry_image_buffer[i] - conv_deblured_buffer[i],2);
    }
    //std::cout << "Calculate energy Ed done" << std::endl;

    delete conv_deblured;

    this->notify("HSVEnergy: Er=" + SImg::float2string(Er));
    this->notify("HSVEnergy: Ed=" + SImg::float2string(Ed));
    this->notify("HSVEnergy: E=" + SImg::float2string(Ed + reg*Er));
    std::vector<float> out;
    out.resize(3);
    out[0] = Ed + reg*Er;
    out[1] = Ed;
    out[2] = Er;

    //std::cout << "Calculate energy ends" << std::endl;
    return out;
}

std::vector<float> SDeconv3d::SVEnergy(SImage* blurryImage, SImage* PSF, SImage* debluredImage, float weighting, float reg, float delta){

    // deblured_image_buffer[ (z) + sz*((y) + sy*(x))]
    float sv = 0.0;
    float dxx, dyy, dzz;
    float Er = 0.0;
    float* deblured_image_buffer = dynamic_cast<SImageFloat*>(debluredImage)->getBuffer();
    unsigned int sx = debluredImage->getSizeX();
    unsigned int sy = debluredImage->getSizeY();
    unsigned int sz = debluredImage->getSizeZ();
    int p, pd;
    for (unsigned int x = 0 ; x < sx ; x++){
        for (unsigned int y = 0 ; y < sy ; y++){
            for (unsigned int z = 0 ; z < sz ; z++){

                p = z + (sz)* (y + (sy)* x);

                pd = z + (sz)* (y + (sy)* (x-1));
                if (x > 0)
                    dxx = deblured_image_buffer[pd] - deblured_image_buffer[p];
                else
                    dxx = deblured_image_buffer[p];

                pd = z + (sz)* ((y-1) + (sy)* (x));
                if (y > 0)
                    dyy = deblured_image_buffer[pd] - deblured_image_buffer[p];
                else
                    dyy = deblured_image_buffer[p];

                pd = (z-1) + (sz)* ((y) + (sy)* (x));
                if (z > 0)
                    dzz = delta*(deblured_image_buffer[pd] - deblured_image_buffer[p]);
                else
                    dzz = deblured_image_buffer[p];

                sv = dxx*dxx + dyy*dyy + dzz*dzz;
                Er += sqrt(pow(weighting,2)*sv + pow(1-weighting,2)*pow(deblured_image_buffer[ (z) + sz*((y) + sy*(x))],2));
            }
        }
    }

    // Calculate Ed
    SFFTConvolutionFilter convFilter;
    convFilter.setImage1(debluredImage);
    convFilter.setImage2(PSF);
    convFilter.run();
    SImageFloat* conv_deblured = dynamic_cast<SImageFloat*>(convFilter.getOutput());

    float Ed = 0;
    float* blurry_image_buffer = dynamic_cast<SImageFloat*>(blurryImage)->getBuffer();
    float* conv_deblured_buffer = dynamic_cast<SImageFloat*>(conv_deblured)->getBuffer();
#pragma omp parallel for
    for (unsigned int i = 0 ; i < sx*sy*sz ; i++){
        Ed += pow(blurry_image_buffer[i] - conv_deblured_buffer[i],2);
    }

    delete conv_deblured;

    this->notify("HSVEnergy: Er=" + SImg::float2string(Er));
    this->notify("HSVEnergy: Ed=" + SImg::float2string(Ed));
    this->notify("HSVEnergy: E=" + SImg::float2string(Ed + reg*Er));
    std::vector<float> out;
    out.resize(3);
    out[0] = Ed + reg*Er;
    out[1] = Ed;
    out[2] = Er;
    return out;
}

SImageFloat* SDeconv3d::runSV3dStack(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose){

#ifdef SL_USE_OPENMP
    omp_set_num_threads(omp_get_max_threads());
    int fftThreads = fftwf_init_threads();
    if (fftThreads == 0){
        std::cout << "Cannot initialize parrallel fft: error " << fftThreads << std::endl;
    }
#endif

    int img_width = image->getSizeX();
    int img_height = image->getSizeY();
    int img_depth = image->getSizeZ();
    int img_detector = image->getSizeT();
    int N = img_width*img_height*img_depth;
    int Nfft = img_width*img_height*(img_depth/2 + 1);

    // m_buffer[ z + m_sz*(y + m_sy*x)]
    // Optical transfer function and its adjoint
    SShift shiftOTFFilter;
    shiftOTFFilter.setInput(psf);
    shiftOTFFilter.setShift(int(- float(img_width) / 2.0), int(-float(img_height) / 2.0), int(-float(img_depth) / 2.0));
    shiftOTFFilter.run();
    SImageFloat* OTFCImg = dynamic_cast<SImageFloat*>(shiftOTFFilter.getOutput());

    SImageFloat* adjoint_PSF = new SImageFloat(img_width, img_height, img_depth);
    float* psfBuffer = psf->getBuffer();
    float* adjoint_PSFBuffer = adjoint_PSF->getBuffer();
    for (int x = 0 ; x < img_width ; x++){
        for (int y = 0 ; y < img_height ; y++){
            for (int z = 0 ; z < img_depth ; z++){
                adjoint_PSFBuffer[z + img_depth*(y + img_height*x)] = psfBuffer[(img_depth - 1 - z) + img_depth*((img_height - 1 - y) + img_height*(img_width - 1 - x))];
            }
        }
    }

    SShift shiftAdjPSFFilter;
    shiftAdjPSFFilter.setInput(adjoint_PSF);
    shiftAdjPSFFilter.setShift(-(int(img_width) - 1) % 2, -(int(img_height) - 1) % 2, -int((img_depth - 1)) % 2);
    shiftAdjPSFFilter.run();
    SImageFloat* adjoint_PSF_shift = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter.getOutput());

    SShift shiftAdjPSFFilter2;
    shiftAdjPSFFilter2.setInput(adjoint_PSF_shift);
    shiftAdjPSFFilter2.setShift(int(-float(img_width) / 2.0), int(-float(img_height) / 2.0), int(-float(img_depth) / 2.0));
    shiftAdjPSFFilter2.run();
    SImageFloat* adjoint_OTFCimg = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter2.getOutput());
    delete adjoint_PSF_shift;

    // copy inputs to buffer
    float* blurry_array = image->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* OTFReal = OTFCImg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* adjoint_OTFReal = adjoint_OTFCimg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* deblurred_image = (float*) malloc(sizeof(float) * N);

    int p = 0;
    // Splitting parameters
    float dual_step = SMath::max(0.01, SMath::min(0.1, m_regularization));
    float primal_step = 0.99 / (0.5 + (12 * pow(m_weighting, 2.) + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);


    float* dual_image0 = (float*) malloc(sizeof(float) * N);
    float* dual_image1 = (float*) malloc(sizeof(float) * N);
    float* dual_image2 = (float*) malloc(sizeof(float) * N);
    float* dual_image3 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* residue_image = (float*) malloc(sizeof(float) * N);

#pragma omp parallel for
    for (int i = 0 ; i < N ; i++){
        dual_image0[i] = 0.0;
        dual_image1[i] = 0.0;
        dual_image2[i] = 0.0;
        dual_image3[i] = 0.0;
        deblurred_image[i] = blurry_array[i];
    }


    fftwf_complex* deblurred_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* residue_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* adjoint_OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));

    SFFT::fft3D(OTFReal, OTF, img_width, img_height, img_depth);
    SFFT::fft3D(adjoint_OTFReal, adjoint_OTF, img_width, img_height, img_depth);

    for (int c = 0 ; c < img_detector ; c++){
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++){
                    deblurred_image[z + img_depth*(y + img_height*x)] = m_stackCoefficients[c]*blurry_array[c + img_detector*(z + img_depth*(y + img_height*x))];
                }
            }
        }
    }

    std::vector<fftwf_complex*> blurry_image_FT;
    for (int c = 0 ; c < img_detector ; c++){
        blurry_image_FT.push_back((fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft)));
    }

    for (int c = 0 ; c < img_detector ; c++){
        SFFT::fft3D(image->getFrame(c)->getBuffer(), blurry_image_FT[c], img_width, img_height, img_depth);
    }


#pragma omp parallel for
    for (int i = 0 ; i < Nfft ; i++){
        residue_image_FT[i][0] = 0.0;
        residue_image_FT[i][1] = 0.0;
    }

    free(blurry_array);
    free(OTFReal);
    free(adjoint_OTFReal);


    // Deconvolution process
    float tmp, dx, dy, dz, dx_adj, dy_adj, dz_adj;
    float real_tmp, imag_tmp, real_residu, imag_residu;
    float min_val = 0.0;
    float max_val = 1.0;
    for (unsigned int iter = 0; iter < m_iterationsNumber; iter++) {
        // Primal optimization
#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = deblurred_image[i];
        }
        SFFT::fft3D(deblurred_image, deblurred_image_FT, img_width, img_height, img_depth);

#pragma omp parallel for
        for (int i = 0 ; i < Nfft ; i++){
            residue_image_FT[i][0] = deblurred_image_FT[i][0];
            residue_image_FT[i][1] = deblurred_image_FT[i][1];
        }

        // Data term
        for(int c = 0 ; c < img_detector ; c++){
#pragma omp parallel for
            for (int i = 0 ; i < Nfft ; i++){
                real_tmp = OTF[i][0] * deblurred_image_FT[i][0]
                        - OTF[i][1] * deblurred_image_FT[i][1]
                        - blurry_image_FT[c][i][0];
                imag_tmp = OTF[i][0] * deblurred_image_FT[i][1]
                        + OTF[i][1] * deblurred_image_FT[i][0]
                        - blurry_image_FT[c][i][1];

                real_residu = adjoint_OTF[i][0] * real_tmp
                        - adjoint_OTF[i][1] *imag_tmp;

                imag_residu = adjoint_OTF[i][0] * imag_tmp
                        + adjoint_OTF[i][1] * real_tmp;

                residue_image_FT[i][0] += m_stackCoefficients[c]*real_residu;
                residue_image_FT[i][0] += m_stackCoefficients[c]*imag_residu;
            }
        }

        SFFT::ifft3D(residue_image_FT, residue_image, img_width, img_height, img_depth);

        // gradient term
        int pd;
#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++) {

                    p = z + (img_depth)* (y + (img_height)* x);
                    tmp = deblurred_image[p] - primal_step * residue_image[p]/float(N);

                    pd = z + (img_depth)* (y + (img_height)* (x-1));
                    if (x > 0)
                        dx_adj = dual_image0[pd] - dual_image0[p];
                    else
                        dx_adj = dual_image0[p];

                    pd = z + (img_depth)* ((y-1) + (img_height)* (x));
                    if (y > 0)
                        dy_adj = dual_image1[pd] - dual_image1[p];
                    else
                        dy_adj = dual_image1[p];

                    pd = (z-1) + (img_depth)* ((y) + (img_height)* (x));
                    if (z > 0)
                        dz_adj = m_delta*(dual_image2[pd] - dual_image2[p]);
                    else
                        dz_adj = dual_image2[p];

                    tmp -= (primal_weight * (dx_adj + dy_adj + dz_adj) + primal_weight_comp * dual_image3[p]);
                    deblurred_image[p] = SMath::max(min_val, SMath::min(max_val, tmp));
                }
            }
        }

        // verbose
        if(verbose){
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0){
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
            }
        }

        // Dual optimization
#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * deblurred_image[i] - auxiliary_image[i];
        }

#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++){

                    p = z + (img_depth)* (y + (img_height)* x);

                    if (x < img_width - 1) {
                        pd = z + (img_depth)* (y + (img_height)* (x+1));
                        dx = auxiliary_image[pd] - auxiliary_image[p];
                        dual_image0[p] += dual_weight * dx;
                    }
                    if (y < img_height - 1) {
                        pd = z + (img_depth)* ((y+1) + (img_height)* x);
                        dy = auxiliary_image[pd] - auxiliary_image[p];
                        dual_image1[p] += dual_weight * dy;
                    }
                    if (z < img_depth - 1) {
                        pd = (z+1) + (img_depth)* (y + (img_height)* x);
                        dz = m_delta*( auxiliary_image[pd] - auxiliary_image[p] );
                        dual_image2[p] += dual_weight * dz;
                    }

                    dual_image3[p] += dual_weight_comp * auxiliary_image[p];
                }
            }
        }

#pragma omp parallel for
        for(int p = 0 ; p < N ; p++){
            tmp = SMath::max(1.,
                             1. / m_regularization
                             * sqrt(
                                 pow(dual_image0[p], 2.)
                                 + pow(dual_image1[p], 2.)
                                 + pow(dual_image2[p], 2.)
                                 + pow(dual_image3[p], 2.)));
            dual_image0[p] /= tmp;
            dual_image1[p] /= tmp;
            dual_image2[p] /= tmp;
            dual_image3[p] /= tmp;
        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    // copy output
    free(dual_image0);
    free(dual_image1);
    free(dual_image2);
    free(dual_image3);
    free(auxiliary_image);
    free(residue_image);

    for (int c = 0 ; c < img_detector ; c++){
        fftwf_free(blurry_image_FT[c]);
    }
    fftwf_free(deblurred_image_FT);
    fftwf_free(residue_image_FT);
    fftwf_free(OTF);
    fftwf_free(adjoint_OTF);

    SImageFloat* outputImage = new SImageFloat(deblurred_image, img_width, img_height, img_depth);
    this->notifyProgress(100);
    return outputImage;
}

SImageFloat* SDeconv3d::runHSV3dStack(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose){

    int img_width = image->getSizeX();
    int img_height = image->getSizeY();
    int img_depth = image->getSizeZ();
    int img_detector = image->getSizeT();
    int N = img_width*img_height*img_depth;
    int Nfft = img_width*img_height*(img_depth/2 + 1);
    float sqrt2 = sqrt(2.);

#ifdef SL_USE_OPENMP
    omp_set_num_threads(omp_get_max_threads());

    int fftThreads = fftwf_init_threads();
    if (fftThreads == 0){
        std::cout << "Cannot initialize parrallel fft: error " << fftThreads << std::endl;
    }
#endif

    // Optical transfer function and its adjoint
    SShift shiftOTFFilter;
    shiftOTFFilter.setInput(psf);
    shiftOTFFilter.setShift(int(- float(img_width) / 2.0), int(-float(img_height) / 2.0), int(-float(img_depth) / 2.0));
    shiftOTFFilter.run();
    SImageFloat* OTFCImg = dynamic_cast<SImageFloat*>(shiftOTFFilter.getOutput());

    SImageFloat* adjoint_PSF = new SImageFloat(img_width, img_height, img_depth);
    float* psfBuffer = psf->getBuffer();
    float* adjoint_PSFBuffer = adjoint_PSF->getBuffer();
    for (int x = 0 ; x < img_width ; x++){
        for (int y = 0 ; y < img_height ; y++){
            for (int z = 0 ; z < img_depth ; z++){
                adjoint_PSFBuffer[z + img_depth*(y + img_height*x)] = psfBuffer[(img_depth - 1 - z) + img_depth*((img_height - 1 - y) + img_height*(img_width - 1 - x))];
            }
        }
    }

    SShift shiftAdjPSFFilter;
    shiftAdjPSFFilter.setInput(adjoint_PSF);
    shiftAdjPSFFilter.setShift(-(int(img_width) - 1) % 2, -(int(img_height) - 1) % 2, -int((img_depth - 1)) % 2);
    shiftAdjPSFFilter.run();
    SImageFloat* adjoint_PSF_shift = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter.getOutput());

    SShift shiftAdjPSFFilter2;
    shiftAdjPSFFilter2.setInput(adjoint_PSF_shift);
    shiftAdjPSFFilter2.setShift(int(-float(img_width) / 2.0), int(-float(img_height) / 2.0), int(-float(img_depth) / 2.0));
    shiftAdjPSFFilter2.run();
    SImageFloat* adjoint_OTFCimg = dynamic_cast<SImageFloat*>(shiftAdjPSFFilter2.getOutput());
    delete adjoint_PSF_shift;

    // Splitting parameters
    float dual_step = SMath::max(0.001, SMath::min(0.01, m_regularization));
    float primal_step = 0.99
            / (0.5
               + (144 * pow(m_weighting, 2.)
                  + pow(1 - m_weighting, 2.)) * dual_step);
    float primal_weight = primal_step * m_weighting;
    float primal_weight_comp = primal_step * (1 - m_weighting);
    float dual_weight = dual_step * m_weighting;
    float dual_weight_comp = dual_step * (1 - m_weighting);


    // copy inputs to buffer
    float* blurry_array = image->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* OTFReal = OTFCImg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* adjoint_OTFReal = adjoint_OTFCimg->getBuffer(); //(float*) malloc(sizeof(float) * unsigned(N));
    float* deblurred_image = (float*) malloc(sizeof(float) * N);

    float* dual_image0 = (float*) malloc(sizeof(float) * N);
    float* dual_image1 = (float*) malloc(sizeof(float) * N);
    float* dual_image2 = (float*) malloc(sizeof(float) * N);
    float* dual_image3 = (float*) malloc(sizeof(float) * N);
    float* dual_image4 = (float*) malloc(sizeof(float) * N);
    float* dual_image5 = (float*) malloc(sizeof(float) * N);
    float* dual_image6 = (float*) malloc(sizeof(float) * N);
    float* auxiliary_image = (float*) malloc(sizeof(float) * N);
    float* residue_image = (float*) malloc(sizeof(float) * N);

#pragma omp parallel for
    for (int i = 0 ; i < N ; i++){
        dual_image0[i] = 0.0;
        dual_image1[i] = 0.0;
        dual_image2[i] = 0.0;
        dual_image3[i] = 0.0;
        dual_image4[i] = 0.0;
        dual_image5[i] = 0.0;
        dual_image6[i] = 0.0;
        deblurred_image[i] = 0.0;
    }

    for (int c = 0 ; c < img_detector ; c++){
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++){
                    deblurred_image[z + img_depth*(y + img_height*x)] = m_stackCoefficients[c]*blurry_array[c + img_detector*(z + img_depth*(y + img_height*x))];
                }
            }
        }
    }

    std::vector<fftwf_complex*> blurry_image_FT;
    for (int c = 0 ; c < img_detector ; c++){
        blurry_image_FT.push_back((fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft)));
    }
    for (int c = 0 ; c < img_detector ; c++){
        SFFT::fft3D(image->getFrame(c)->getBuffer(), blurry_image_FT[c], img_width, img_height, img_depth);
    }

    fftwf_complex* deblurred_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* residue_image_FT = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));
    fftwf_complex* adjoint_OTF = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * unsigned(Nfft));

    SFFT::fft3D(OTFReal, OTF, img_width, img_height, img_depth);
    SFFT::fft3D(adjoint_OTFReal, adjoint_OTF, img_width, img_height, img_depth);
    SFFT::fft3D(deblurred_image, deblurred_image_FT, img_width, img_height, img_depth);

    free(blurry_array);
    free(OTFReal);
    free(adjoint_OTFReal);

#pragma omp parallel for
    for (int i = 0 ; i < Nfft ; i++){
        residue_image_FT[i][0] = 0.0;
        residue_image_FT[i][1] = 0.0;
    }

    // Deconvolution process
    float tmp, dxx, dyy, dzz, dxy, dyz, dzx, min_val, max_val, dxx_adj,
            dyy_adj, dzz_adj, dxy_adj, dyz_adj, dzx_adj;


    min_val = 0.0;
    max_val = 1.0;

    float real_tmp, imag_tmp, real_residu, imag_residu;
    int p;
    int pxm, pxp,pym, pyp,pzm, pzp, pxyp, pyzp, pxzp, pyzm, pxym, pxzm;

    for (unsigned int iter = 0; iter < m_iterationsNumber; iter++) {
        // Primal optimization
#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = deblurred_image[i];
        }

        SFFT::fft3D(deblurred_image, deblurred_image_FT, img_width, img_height, img_depth);

#pragma omp parallel for
        for (int i = 0 ; i < Nfft ; i++){
            residue_image_FT[i][0] = 0.0;
            residue_image_FT[i][1] = 0.0;
        }

        // Data term
        for (int c = 0 ; c < img_detector ; c++){
#pragma omp parallel for
            for (int i = 0 ; i < Nfft ; i++){
                real_tmp = OTF[i][0] * deblurred_image_FT[i][0]
                        - OTF[i][1] * deblurred_image_FT[i][1]
                        - blurry_image_FT[c][i][0];
                imag_tmp = OTF[i][0] * deblurred_image_FT[i][1]
                        + OTF[i][1] * deblurred_image_FT[i][0]
                        - blurry_image_FT[c][i][1];

                real_residu = adjoint_OTF[i][0] * real_tmp
                        -adjoint_OTF[i][1] * imag_tmp;
                imag_residu = adjoint_OTF[i][0] * imag_tmp
                        + adjoint_OTF[i][1] * real_tmp;

                residue_image_FT[i][0] += m_stackCoefficients[c]*real_residu;
                residue_image_FT[i][1] += m_stackCoefficients[c]*imag_residu;
            }
        }

        SFFT::ifft3D(residue_image_FT, residue_image, img_width, img_height, img_depth);

        // gradient term
#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++) {

                    p = z + (img_depth)* (y + (img_height)* x);
                    tmp = auxiliary_image[p] - primal_step * residue_image[p]/float(N);

                    dxx_adj = dyy_adj = dzz_adj = dxy_adj = dyz_adj = dzx_adj = 0.;

                    // Diagonal terms
                    pxm =  z + (img_depth)* (y + (img_height)* (x-1));
                    pxp =  z + (img_depth)* (y + (img_height)* (x+1));
                    if ((x > 0) && (x < img_width - 1)){
                        dxx_adj = dual_image0[pxm] - 2 * dual_image0[p] + dual_image0[pxp];
                    }

                    pym =  z + (img_depth)* ((y-1) + (img_height)* (x));
                    pyp =  z + (img_depth)* ((y+1) + (img_height)* (x));
                    if ((y > 0) && (y < img_height - 1)){
                        dyy_adj = dual_image1[pym] - 2 * dual_image1[p] + dual_image1[pyp];
                    }

                    pzm =  (z-1) + (img_depth)* ((y) + (img_height)* (x));
                    pzp =  (z+1) + (img_depth)* ((y) + (img_height)* (x));
                    if ((z > 0) && (z < img_depth - 1)){
                        dzz_adj = (m_delta*m_delta)*(dual_image2[pzm] - 2 * dual_image2[p] + dual_image2[pzp]);
                    }

                    // Other terms
                    if ((x == 0) && (y == 0)){
                        dxy_adj = dual_image3[p];
                    }

                    if ((x > 0) && (y == 0)){
                        dxy_adj = dual_image3[p] - dual_image3[pxm];
                    }

                    if ((x == 0) && (y > 0)){
                        dxy_adj = dual_image3[p] - dual_image3[pym];
                    }

                    pxym = z + (img_depth)* ((y-1) + (img_height)* (x-1));
                    if ((x > 0) && (y > 0)){
                        dxy_adj = dual_image3[p] - dual_image3[pxm] - dual_image3[pym] + dual_image3[pxym];
                    }

                    if ((y == 0) && (z == 0)){
                        dyz_adj = dual_image4[p];
                    }

                    if ((y > 0) && (z == 0)){
                        dyz_adj = dual_image4[p] - dual_image4[pym];
                    }

                    if ((y == 0) && (z > 0)){
                        dyz_adj = dual_image4[p] - dual_image4[pzm];
                    }

                    pyzm = (z-1) + (img_depth)* ((y-1) + (img_height)* x);
                    if ((y > 0) && (z > 0)){
                        dyz = m_delta*( dual_image4[p] - dual_image4[pym] - dual_image4[pzm] + dual_image4[pyzm] );
                    }


                    if ((z == 0) && (x == 0)){
                        dzx_adj = dual_image5[p];
                    }

                    if ((z > 0) && (x == 0)){
                        dzx_adj = dual_image5[p] - dual_image5[pzm];
                    }

                    if ((z == 0) && (x > 0)){
                        dzx_adj = m_delta*(dual_image5[pxm] - dual_image5[p]);
                    }

                    pxzm = (z-1) + (img_depth)* (y + (img_height)* (x-1));
                    if ((z > 0) && (x > 0)){
                        dzx_adj = dual_image5[p] - dual_image5[pzm]
                                - dual_image5[pxm]
                                + dual_image5[pxzm];
                    }


                    tmp -= (primal_weight
                            * (dxx_adj + dyy_adj + dzz_adj
                               + sqrt2 * (dxy_adj + dyz_adj + dzx_adj))
                            + primal_weight_comp * dual_image6[p]);

                    deblurred_image[p] = SMath::max(0.0, SMath::min(1.0, tmp));

                }
            }
        }

        // Stopping criterion
        if (verbose){
            if (iter % int(SMath::max(1, m_iterationsNumber / 10)) == 0)
                this->notifyProgress(100*(float(iter)/float(m_iterationsNumber)));
        }


        // Dual optimization
#pragma omp parallel for
        for (int i = 0 ; i < N ; i++){
            auxiliary_image[i] = 2 * deblurred_image[i] - auxiliary_image[i];
        }

#pragma omp parallel for
        for (int x = 0 ; x < img_width ; x++){
            for (int y = 0 ; y < img_height ; y++){
                for (int z = 0 ; z < img_depth ; z++){

                    p = z + (img_depth)* (y + (img_height)* x);

                    pxp = z + (img_depth)* (y + (img_height)* (x+1));
                    pxm = z + (img_depth)* (y + (img_height)* (x-1));
                    if ((x > 0) && (x < img_width - 1)) {
                        dxx = auxiliary_image[pxp]
                                - 2 * auxiliary_image[p]
                                + auxiliary_image[pxm];
                        dual_image0[p] += dual_weight * dxx;
                    }

                    pyp = z + (img_depth)* ((y+1) + (img_height)* x);
                    pym = z + (img_depth)* ((y-1) + (img_height)* x);
                    if ((y > 0) && (y < img_height - 1)) {
                        dyy = auxiliary_image[pyp]
                                - 2 * auxiliary_image[p]
                                + auxiliary_image[pym];
                        dual_image1[p] += dual_weight * dyy;
                    }

                    pzp = (z+1) + (img_depth)* (y + (img_height)* x);
                    pzm = (z-1) + (img_depth)* (y + (img_height)* x);
                    if ((z > 0) && (z < img_depth - 1)) {
                        dzz = (m_delta*m_delta)*(auxiliary_image[pzp]
                                                 - 2 * auxiliary_image[p]
                                                 + auxiliary_image[pzm]);
                        dual_image2[p] += dual_weight * dzz;
                    }

                    pxyp = z + (img_depth)* ((y+1) + (img_height)* (x+1));
                    if ((x < img_width - 1) && (y < img_height - 1)) {
                        dxy = auxiliary_image[pxyp]
                                - auxiliary_image[pxp]
                                - auxiliary_image[pyp]
                                + auxiliary_image[p];
                        dual_image3[p] += sqrt2 * dual_weight * dxy;
                    }

                    pyzp = (z+1) + (img_depth)* ((y+1) + (img_height)* (x));
                    if ((y < img_height - 1) && (z < img_depth - 1)) {
                        dyz = m_delta*(auxiliary_image[pyzp]
                                       - auxiliary_image[pyp]
                                       - auxiliary_image[pzp]
                                       + auxiliary_image[p]);
                        dual_image4[p] += sqrt2 * dual_weight * dyz;
                    }

                    pxzp = (z+1) + (img_depth)* ((y) + (img_height)* (x+1));
                    if ((z < img_depth - 1) && (x < img_width - 1)) {
                        dzx = m_delta*(auxiliary_image[pxzp]
                                       - auxiliary_image[pxp]
                                       - auxiliary_image[pzp]
                                       + auxiliary_image[p]);
                        dual_image5[p] += sqrt2 * dual_weight * dzx;
                    }

                    dual_image6[p] += dual_weight_comp * auxiliary_image[p];

                }
            }
        }

#pragma omp parallel for
        for (int p = 0 ; p < N ; p++){
            float tmp = SMath::max(1.,
                                   1. / m_regularization
                                   * sqrt(
                                       pow(dual_image0[p], 2.)
                                       + pow(dual_image1[p], 2.)
                                       + pow(dual_image2[p], 2.)
                                       + pow(dual_image3[p], 2.)
                                       + pow(dual_image4[p], 2.)
                                       + pow(dual_image5[p], 2.)
                                       + pow(dual_image6[p],
                                             2.)));
            dual_image0[p] /= tmp;
            dual_image1[p] /= tmp;
            dual_image2[p] /= tmp;
            dual_image3[p] /= tmp;
            dual_image4[p] /= tmp;
            dual_image5[p] /= tmp;
            dual_image6[p] /= tmp;
        }

    } // endfor (int iter = 0; iter < nb_iters_max; iter++)

    // copy output
    free(dual_image0);
    free(dual_image1);
    free(dual_image2);
    free(dual_image3);
    free(dual_image4);
    free(dual_image5);
    free(dual_image6);
    free(auxiliary_image);
    free(residue_image);

    for (int c = 0 ; c < img_detector ; c++){
        fftwf_free(blurry_image_FT[c]);
    }
    fftwf_free(deblurred_image_FT);
    fftwf_free(residue_image_FT);

    SImageFloat* outputImage = new SImageFloat(deblurred_image, img_width, img_height, img_depth);
    this->notifyProgress(100);
    return outputImage;

}
