/// \file SDeconv2d.h
/// \brief SDeconv2d class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2018

#pragma once

#include <string>
#include "spitfiredeconvExport.h"

#include "simage/SImageFloat.h"
#include "simage/SImageFilter.h"
#include "sdata/SArray.h"
#include <vector>

/// \class SDeconv2d
/// \brief class that contains the implementation of SPARTION 2D deconvolution
class SPITFIREDECONV_EXPORT SDeconv2d : public SImageFilter{

public:
    SDeconv2d();

public:
    void setMethod(std::string method);
    void setSigmaPSF(float sigma);
    void setRegularization(float regularization);
    void setWeighting(float weighting);
    void setIterationsNumber(unsigned int iterationsNumber);
    void setVerbose(bool verbose);
    void setUseStack(bool useStack);
    void setStackCoefficients(std::vector<float> coefficients);
    void setIntensityNormalization(std::string normalization); // max, sum, L2, 8bits, 12bits, 16bits
    void setCalculateFinalEnergy(bool calculateEnergy);

public:
    void run();

public:
    std::vector<float> getEnergy();
    SArray* getEnergyArray();

protected:
    void runOnce();
    void runSlices();
    void runStack();

protected:
    SImageFloat* runSV2d(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose);
    SImageFloat* runHSV2d(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose);
    SImageFloat* runSV2dStack(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose);
    SImageFloat* runHSV2dStack(SImageFloat* image, SImageFloat* psf, float m_regularization, float m_weighting, unsigned int m_iterationsNumber, bool verbose);

protected:
    std::vector<float> HSVEnergy(SImage* blurryImage, SImage* PSF, SImage* debluredImage, float weighting, float reg);
    std::vector<float> SVEnergy(SImage* blurryImage, SImage* PSF, SImage* debluredImage, float weighting, float reg);

protected:
    float m_sigma;
    float m_regularization;
    float m_weighting;
    unsigned int m_iterationsNumber;
    bool m_verbose;
    std::string m_method;
    bool m_useStack;
    std::vector<float> m_stackCoefficients;
    std::string m_normalization;
    bool m_calculateEnergy;

protected:
    float m_maxInput;
    std::vector<float> m_energy;

};
