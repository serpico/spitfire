/// \file SGaussianPSF.h
/// \brief SGaussianPSF class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2020

#pragma once

#include <string>

#include "spitfiredeconvExport.h"
#include "simage/SImage.h"

/// \class SGaussianPSF
/// \brief class that contains the implementation of SPARTION 2D deconvolution
class SPITFIREDECONV_EXPORT SGaussianPSF{

public:
    SGaussianPSF();

public:
    void setImageSize(unsigned int sx, unsigned int sy, unsigned int sz = 1);
    void setSigma(float sigmaX, float sigmaY, float sigmaZ = 1.0);
    void setSigmaX(float sigma);
    void setSigmaY(float sigma);
    void setSigmaZ(float sigma);

public:
    SImage* getPSF();
    SImage* getPSF2D();
    SImage* getPSF3D();

protected:
    float m_sigmaX;
    float m_sigmaY;
    float m_sigmaZ;
    unsigned int m_sx;
    unsigned int m_sy;
    unsigned int m_sz;
};
