/// \file SAiryscanWeights.h
/// \brief SAiryscanWeights class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2019

#pragma once

#include <string>
#include "spitfireairyscanExport.h"

#include <vector>

/// \class SAiryscanWeights
/// \brief Define deconvolution weights for airyscan multi image deconvolution
class SPITFIREAIRYSCAN_EXPORT SAiryscanWeights{

public:
    SAiryscanWeights();

public:
    std::vector<float> mean();
    std::vector<float> distanceToCenter(float sigma = 3.0414);
    std::vector<float> indexDistance(float sigma = 3.0414);
    std::vector<float> indexDistanceInv(float sigma = 3.0414);
    std::vector<float> invertedDistanceToCenter(float sigma = 3.0414);
    std::vector<float> stepFunction(int stepIndex, float stepCoefficient = 1);

    void plotWeights(std::vector<float> weights, std::string title);

public:
    std::vector<float> distancesToCenter(float d = 1.0);

protected:
    float norm(float a, float b);
    std::vector<float> sumToOne(std::vector<float> a);

protected:
    float maxDistance;
};
