/// \file SAiryscanWeights.cpp
/// \brief SAiryscanWeights class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2019

#include "SAiryscanWeights.h"
#include <math.h>
#include <iostream>

SAiryscanWeights::SAiryscanWeights(){

}

std::vector<float> SAiryscanWeights::mean(){

    std::vector<float> weights(32,1);
    return this->sumToOne(weights);

}

std::vector<float> SAiryscanWeights::distanceToCenter(float sigma){

    std::vector<float> weights = this->distancesToCenter();
    for (int i = 0 ; i < 32 ; i++){
        weights[i] = exp( -  weights[i] / sigma  );
    }
    return this->sumToOne(weights);

}

std::vector<float> SAiryscanWeights::indexDistance(float){
    std::vector<float> weights(32,0);
    for (int i = 0 ; i < 32 ; i++){
        weights[i] = i;
    }
    return this->sumToOne(weights);
}

std::vector<float> SAiryscanWeights::indexDistanceInv(float){
    std::vector<float> weights(32,0);
    for (int i = 0 ; i < 32 ; i++){
        weights[i] = 31-i;
    }
    return this->sumToOne(weights);
}

std::vector<float> SAiryscanWeights::stepFunction(int stepIndex, float stepCoefficient){

    if (stepIndex < 0 || stepIndex > 31){
        std::cout << "stepIndex must be in [0,31]" << std::endl;
    }

    std::vector<float> weights(32,0);
    for (int i = 0 ; i < stepIndex ; i++){
        weights[i] = 1.0*stepCoefficient;
    }
    for (int i = stepIndex ; i < 32 ; i++){
        weights[i] = 1;
    }
    return this->sumToOne(weights);

}

std::vector<float> SAiryscanWeights::invertedDistanceToCenter(float sigma){
    std::vector<float> weights = this->distanceToCenter(sigma);
    std::vector<float> weightsInv(32,0);
    for (int i = 0 ; i < 32 ; i++){
        weightsInv[i] = weights[31-i];
    }
    return weightsInv;
}

void SAiryscanWeights::plotWeights(std::vector<float> weights, std::string title){

    std::cout << title << ":" << std::endl;
    for (unsigned int i = 0 ; i < weights.size() ; i++){
        std::cout << weights[i] << std::endl;
    }
    std::cout << std::endl;
}

std::vector<float> SAiryscanWeights::distancesToCenter(float d){

    std::vector<float> dist(32,0);
    dist[0] = norm(0, 0);
    dist[1] = norm(d, 0.5*d);
    dist[2] = norm(d, -0.5*d);
    dist[3] = norm(0, -d);
    dist[4] = norm(-d, -0.5*d);
    dist[5] = norm(-d, 0.5*d);
    dist[6] = norm(0, d);
    dist[7] = norm(d, 1.5*d);
    dist[8] = norm(2*d, d);
    dist[9] = norm(2*d, 0);
    dist[10] = norm(2*d, -d);
    dist[11] = norm(d, -1.5*d);
    dist[12] = norm( 0, -2*d);
    dist[13] = norm(-d, -1.5*d);
    dist[14] = norm(-2*d, -d);
    dist[15] = norm(-2*d, 0);
    dist[16] = norm(-2*d, d);
    dist[17] = norm(-d, 1.5*d);
    dist[18] = norm(0, 2*d);
    dist[19] = norm(d, 2.5*d);
    dist[20] = norm(2*d, 2*d);
    dist[21] = norm(3*d, 0.5*d);
    dist[22] = norm(3*d, -0.5*d);
    dist[23] = norm(2*d, -2*d);
    dist[24] = norm(d, -2.5*d);
    dist[25] = norm(-d, -2.5*d);
    dist[26] = norm(-2*d, -2*d);
    dist[27] = norm(-3*d, -0.5*d);
    dist[28] = norm(-3*d, 0.5*d);
    dist[29] = norm(-2*d, 2*d);
    dist[30] = norm(-1*d, 2.5*d);
    dist[31] = norm(0, 3*d);
    return dist;
}

float SAiryscanWeights::norm(float a, float b){
    return sqrt( a*a + b*b );
}

std::vector<float> SAiryscanWeights::sumToOne(std::vector<float> a){
    float sum = 0;
    for (unsigned int i = 0 ; i < a.size() ; i++){
        sum += a[i];
    }
    std::vector<float> out(a.size(),0);
    for (unsigned int i = 0 ; i < a.size() ; i++){
        out[i] = a[i]/sum;
    }
    return out;
}
