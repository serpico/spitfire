#include <score>
#include <scli>
#include <simageio>
#include <spitfiredenoise>
#include "svdeconvTestConfig.h"

#include <iostream>
#include "math.h"

int main(int argc, char *argv[])
{
    std::cout << "input image = " << SAMPLE2D << std::endl;
    std::cout << "ref image = " << SAMPLE2DDENOISED << std::endl;
    std::cout << "result opt image = " << SAMPLE2DDENOISEDOPT << std::endl;


    SImageFloat* inputImage = dynamic_cast<SImageFloat*>(SImageReader::read(SAMPLE2D));
    SDenoise2d process;
    process.addObserver(new SObserverConsole());
    process.setInput( inputImage );
    process.setMethod("HSV");
    process.setRegularization(2);
    process.setWeighting(0.6);
    process.setIterationsNumber(200);
    process.setVerbose(true);
    SImg::tic();
    process.run();
    SImg::toc();
    SImageFloat* outputImage = dynamic_cast<SImageFloat*>(process.getOutput());
    SImageReader::write(outputImage, SAMPLE2DDENOISEDOPT);

    // calculate error with the reference image
    SImageFloat* resultImage = dynamic_cast<SImageFloat*>(SImageReader::read(SAMPLE2DDENOISED));
    float *b1 = outputImage->getBuffer();
    float *b2 = resultImage->getBuffer();
    unsigned int sx = inputImage->getSizeX();
    unsigned int sy = inputImage->getSizeY();
    unsigned int p = 0;
    double error = 0.0;
    for (unsigned int x = 10 ; x < sx-10 ; ++x )
    {
        for (unsigned int y = 10 ; y < sy-10 ; ++y )
        {
            p = sy*x+y;
            error += pow(b1[p]-b2[p],2);
        }
    }
    //error /= float(outputImage->getBufferSize());
    std::cout << "error =" << error << std::endl;
    if (error > 10)
    {
        return 1;
    }
    return 0;

}
