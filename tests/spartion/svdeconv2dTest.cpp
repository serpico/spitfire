#include <simageio>
#include <spitfiredeconv>
#include "svdeconvTestConfig.h"

#include <iostream>
#include "math.h"

int main(int argc, char *argv[])
{

    return 0;

    std::cout << "input image = " << SAMPLE2D << std::endl;
    std::cout << "result image = " << SAMPLE2DDECONV2D << std::endl;


    SImageFloat* inputImage = dynamic_cast<SImageFloat*>(SImageReader::read(SAMPLE2D));
    SDeconv2d process;
    process.setInput( inputImage );
    process.setMethod("SV");
    process.setSigmaPSF(2);
    process.setRegularization(9);
    process.setWeighting(0.5);
    process.setIterationsNumber(200);
    process.setVerbose(true);
    process.run();
    SImageFloat* outputImage = dynamic_cast<SImageFloat*>(process.getOutput());

    // calculate error with the reference image
    SImageFloat* resultImage = dynamic_cast<SImageFloat*>(SImageReader::read(SAMPLE2DDECONV2D));
    float *b1 = outputImage->getBuffer();
    float *b2 = resultImage->getBuffer();
    double error = 0.0;
    for (int i = 0  ; i < outputImage->getBufferSize() ; i++){
        error += pow(b1[i]-b2[i],2);
    }
    error /= float(outputImage->getBufferSize());
    if (error > 10)
    {
        return 1;
    }
    return 0;

}
