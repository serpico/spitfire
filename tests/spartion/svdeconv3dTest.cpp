#include <simageio>
#include <spitfiredeconv>
#include "svdeconvTestConfig.h"

#include <iostream>
#include "math.h"

int main(int argc, char *argv[])
{

    std::cout << "input image = " << SAMPLE3D << std::endl;
    std::cout << "psf image = " << PSF3D << std::endl;
    std::cout << "result image = " << SAMPLE3DDECONV3D << std::endl;

    SImageFloat* inputImage = dynamic_cast<SImageFloat*>(SImageReader::read(SAMPLE3D));
    //inputImage.save("input.tif");
    SImageFloat* psfImage = dynamic_cast<SImageFloat*>(SImageReader::read(PSF3D));
    //psfImage.save("psf.tif");
    SDeconv3d process;
    process.setInput( inputImage );
    process.setPSF(psfImage);
    process.setMethod("SV");
    process.setRegularization(30);
    process.setWeighting(0.5);
    process.setIterationsNumber(200);
    process.setVerbose(true);
    process.run();
    SImageFloat* outputImage = dynamic_cast<SImageFloat*>(process.getOutput());

    // calculate error with the reference image
    // calculate error with the reference image
    SImageFloat* resultImage = dynamic_cast<SImageFloat*>(SImageReader::read(SAMPLE3DDECONV3D));
    float *b1 = outputImage->getBuffer();
    float *b2 = resultImage->getBuffer();
    double error = 0.0;
    for (int i = 0  ; i < outputImage->getBufferSize() ; i++){
        error += pow(b1[i]-b2[i],2);
    }
    error /= float(outputImage->getBufferSize());
    if (error > 10)
    {
        return 1;
    }
    return 0;

}
